import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter as Router } from 'react-router-redux'
import routes from './routes'

if (process.env.NODE_ENV !== 'development') {
  let Raven = require('raven-js')
  Raven.config(
    'https://0e53d5fd5c9747a7ad8ef0cdf16cd501@sentry.io/215305'
  ).install()
}

class App extends React.Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Router history={this.props.history}>{routes}</Router>
      </Provider>
    )
  }
}

export default App
