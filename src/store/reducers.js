import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as notifications } from 'react-notification-system-redux'
import {
  persistStore,
  persistCombineReducers,
  purgeStoredState
} from 'redux-persist'
import storage from 'redux-persist/es/storage'
import user from './reducers/user'
import candidateOptions from './reducers/candidateOptions'
import candidates from './reducers/candidates'
import assignments from './reducers/assignments'
import stats from './reducers/stats'
import customers from './reducers/customers'
import ads from './reducers/ads'
import assignmentBoard from './reducers/assignmentBoard'

const config = {
  key: 'root',
  storage
}

const appReducer = {
  notifications,
  routing: routerReducer,
  user: user,
  candidateOptions: candidateOptions,
  candidates: candidates,
  assignments: assignments,
  stats: stats,
  customers: customers,
  ads: ads,
  assignmentBoard: assignmentBoard
}

const reducer = persistCombineReducers(config, appReducer)
// purgeStoredState(config)

export const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    // state = undefined
    purgeStoredState(config)
    state = {
      routing: state.routing
    }
  }
  if (action.type === 'UPDATE') {
    window.location.reload()
  }
  return reducer(state, action)
}

export default rootReducer
