const EMPTY_STATE = {
  loaded: false,
  token: null,
  loggingIn: false,
  profile: null,
  updatingProfile: false
}
const INITIAL_STATE = window.__PRELOADED_STATE__ || EMPTY_STATE

export default function user(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        loggingIn: true
      }
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        loggingIn: false,
        ...action
      }
    case 'LOGIN_FAIL':
      return {
        ...state,
        loggingIn: false,
        user: null,
        loginError: action.error
      }
    case 'LOGOUT':
      console.log('logout')
      return {
        ...state,
        loggingOut: true,
        token: null,
        profile: null
      }
    case 'LOGOUT_SUCCESS':
      return {
        ...state,
        loggingOut: false,
        profile: null
      }
    case 'LOGOUT_FAIL':
      return {
        ...state,
        loggingOut: false,
        logoutError: action.error
      }

    case 'GET_PROFILE':
      let mProfile = Object.assign({}, action.profile, {
        offices: [...action.offices]
      })
      return {
        ...state,
        profile: mProfile
      }

    case 'UPDATE':
      return {
        ...state,
        token: null
      }

    case 'SET_TOKEN':
      return {
        ...state,
        token: action.token
      }
    case 'UPDATING_PROFILE_START':
      return {
        ...state,
        updatingProfile: true
      }

    case 'UPDATING_PROFILE_SUCCESS':
      return {
        ...state,
        profile: action.profile,
        updatingProfile: false
      }
    default:
      return state
  }
}
