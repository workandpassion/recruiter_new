export default function stats(state = {}, action) {
  if (action.error) {
    return {
      result: state.result,
      error: action.error
    }
  }
  switch (action.type) {
    case 'GET_LAGER_STATS':
      return {
        lagerstats: action.stats
      }
    case 'GET_WAP_STATS':
      return {
        wapstats: action.stats
      }
    case 'GET_BEHANDLADE_STATS':
      return {
        behandladestats: action.stats
      }
    default:
      return state
  }
}
