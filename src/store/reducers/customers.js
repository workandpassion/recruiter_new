export default function customers(state = {}, action) {
  if (action.error) {
    return {
      result: state.result,
      error: action.error
    }
  }
  switch (action.type) {
    case 'GET_ALL_CUSTOMERS':
      return {
        ...state,
        fetchingCustomers: action.fetchingCustomers,
        allCustomers: action.allCustomers
          ? action.allCustomers
          : state.allCustomers
      }

    case 'SET_CUSTOMER':
      return {
        ...state,
        ...action
      }

    case 'GET_CUSTOMER':
      return {
        ...state,
        ...action
      }

    case 'CREATE_CUSTOMER':
      return {
        ...state,
        ...action
      }

    case 'UPDATE_CUSTOMER':
      return {
        ...state,
        ...action
      }

    default:
      return state
  }
}
