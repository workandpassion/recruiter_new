function _hasNoRating(item) {
  return !item.recruiter_info || !item.recruiter_info.rating
}

function _isMPC(item) {
  return item.recruiter_info && item.recruiter_info.rating === 5
}

export default function candidates(state = {}, action) {
  // console.log(action.type)
  if (action.error) {
    return {
      result: state.result,
      error: action.error
    }
  }

  switch (action.type) {
    case 'GET_ALL_CANDIDATES':
      let mCandidates = []
      console.log(action)
      action.allCandidates.map(candidate => {
        return mCandidates.push(setCandidateProgress(candidate))
      })

      return {
        ...state,
        allCandidates: mCandidates,
        receivedAt: Date.now()
      }

    case 'UPDATE_IN_ALL_CANDIDATES':
      console.log(state)
      let index = state.allCandidates.findIndex(
        allCandidates => allCandidates.user === action.id
      )
      if (index === -1) return state
      return {
        ...state,
        allCandidates: [
          ...state.allCandidates.slice(0, index),
          Object.assign({}, state.allCandidates[index], {
            recruiter_info: {
              ...action.rating
            }
          }),
          ...state.allCandidates.slice(index + 1)
        ]
      }

    case 'GET_CANDIDATES_WITHOUT_RATING':
      let obehandladeCandidates = state.allCandidates.filter(_hasNoRating)
      return {
        allCandidates: obehandladeCandidates
      }

    case 'GET_CANDIDATES_WITH_MPC':
      let mpcCandidates = state.allCandidates.filter(_isMPC)
      return {
        allCandidates: mpcCandidates
      }

    case 'TABLE_STATE':
      return {
        ...state,
        ...action
      }

    case 'UPDATE_SEARCH_TERMS':
      return {
        ...state,
        ...action
      }

    case 'GET_CANDIDATE':
      let mCandidate = Object.assign({}, action.candidateInfo, {
        jobApplications: action.jobApplications,
        assignments: action.assignments
      })
      return {
        ...state,
        selectedCandidate: mCandidate
      }

    case 'SET_CANDIDATE':
      return {
        ...state,
        ...action
      }

    case 'SET_RATING':
      return {
        ...state,
        selectedCandidate: {
          ...state.selectedCandidate,
          recruiter_info: {
            ...action
          }
        }
      }

    case 'GET_RECOMMENDATIONS':
      return {
        ...state,
        recommendations: action.recommendations
      }

    case 'GET_FOLDERS':
      return {
        ...state,
        folders: action.folders
      }

    case 'CREATE_FOLDER':
      let mFolders = Object.assign([], state.folders)
      mFolders.push(action.newFolder)

      mFolders.sort(
        (a, b) => (a.name !== b.name ? (a.name < b.name ? -1 : 1) : 0)
      )

      return {
        ...state,
        folders: mFolders
      }

    case 'SAVE_CANDIDATE_START':
      return {
        ...state,
        savingCandidate: true
      }

    case 'SAVE_CANDIDATE_SUCCESS':
      return {
        ...state,
        savingCandidate: false
      }

    default:
      return state
  }
}

function setCandidateProgress(candidate) {
  // console.log(candidate)
  let count = 0
  candidate.employments.length > 0 && count++
  candidate.educations.length > 0 && count++
  candidate.languages.length > 0 && count++
  candidate.locations.length > 0 && count++
  candidate.motivations.length > 0 && count++
  candidate.occupations.length > 0 && count++
  candidate.personalities.length > 0 && count++
  candidate.questions.length > 0 && count++
  candidate.skills.length > 0 && count++
  candidate.videos.length > 0 && count++
  candidate.assessment_result && count++
  candidate.personal_info && count++

  let mCandidate = Object.assign({}, candidate, { progress: count / 12 * 100 })
  return mCandidate
}
