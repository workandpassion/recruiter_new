import _ from 'lodash'

export default function assignments(state = {}, action) {
  if (action.error) {
    return {
      result: state.result,
      error: action.error
    }
  }
  switch (action.type) {
    case 'FETCHING_ASSIGNMENT':
      return {
        ...state,
        fetchingAssignment: action.fetchingAssignment
      }

    case 'GET_ASSIGNMENTS':
      return {
        ...state,
        allAssignments: action.allAssignments,
        myAssignments: action.myAssignments
      }

    case 'SET_ASSIGNMENT':
      return {
        ...state,
        ...action
      }

    case 'GET_ASSIGNMENT':
      return {
        ...state,
        selectedAssignment: {
          ...state.selectedAssignment,
          ...action.selectedAssignment,
          matchedCandidates: state.selectedAssignment
            ? state.selectedAssignment.matchedCandidates
            : [],
          receivedMatchedCandidates: state.selectedAssignment
            ? state.selectedAssignment.receivedMatchedCandidates
            : undefined
        },
        unnestedSelectedAssignment: action.unnestedSelectedAssignment,
        fetchingAssignment: action.fetchingAssignment
      }
    case 'REMOVE_CANDIDATE_FROM_MATCHES':
      return {
        ...state,
        selectedAssignment: {
          ...state.selectedAssignment,
          matchedCandidates: action.matchedCandidates
        }
      }

    case 'FETCHING_MATCHED_CANDIDATES':
      return {
        ...state,
        selectedAssignment: {
          ...state.selectedAssignment,
          fetchingMatchedCandidates: true
        }
      }

    case 'GET_MATCHED_CANDIDATES':
      return {
        ...state,
        selectedAssignment: {
          ...state.selectedAssignment,
          fetchingMatchedCandidates: action.fetchingMatchedCandidates,
          matchedCandidates: action.matchedCandidates,
          receivedMatchedCandidates: action.receivedAt
            ? action.receivedAt
            : state.selectedAssignment.receivedMatchedCandidates
        }
      }

    case 'GET_ASSIGNMENT_STATE_VALUES':
      return {
        ...state,
        ...action
      }

    case 'GET_PROCESS_STATE_VALUES':
      return {
        ...state,
        ...action
      }

    case 'ADD_ASSIGNMENT_STATE':
      return {
        ...state,
        selectedAssignment: {
          ...state.selectedAssignment,
          creatingAssignmentState: action.creatingAssignmentState
        }
      }

    case 'UPDATE_AD':
      return Object.assign({}, state, {
        selectedAssignment: Object.assign({}, state.selectedAssignment, {
          ad: action.ad
        })
      })

    default:
      return state
  }
}
