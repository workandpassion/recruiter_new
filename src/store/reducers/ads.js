export default function ads(state = {}, action) {
  if (action.error) {
    return {
      result: state.result,
      error: action.error
    }
  }

  switch (action.type) {
    case 'GET_ALL_ADS':
      return {
        allAds: action.allAds
      }

    case 'UPDATE_AD':
      console.log(state)

    default:
      return state
  }
}
