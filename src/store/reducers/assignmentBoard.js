import _ from 'lodash'

Array.prototype.move = function(old_index, new_index) {
  if (new_index >= this.length) {
    var k = new_index - this.length
    while (k-- + 1) {
      this.push(undefined)
    }
  }
  this.splice(new_index, 0, this.splice(old_index, 1)[0])
  return this // for testing purposes
}

export default function assignmentBoard(state = {}, action) {
  let { process, nextIndex, fromIndex, indexInList, stateExists } = action

  switch (action.type) {
    case 'SETTING_UP_BOARD':
      return Object.assign({}, state, {
        ...action
      })

    case 'SETUP_LISTS':
      return Object.assign({}, state, {
        ...action
      })

    case 'MOVING_CARD':
      return Object.assign({}, state, {
        ...action
      })

    case 'MOVE_CARD':
      let mLists = [...state.lists]

      // move element to new place

      mLists[nextIndex].items.splice(0, 0, mLists[fromIndex].items[indexInList])
      let tempState = {
        comment: '',
        completed_at: null,
        date: null,
        end_date: null,
        scheduled_at: null,
        value: {
          color: '',
          icon: '',
          id: mLists[nextIndex].id,
          name: mLists[nextIndex].name
        }
      }
      let newItem = Object.assign({}, mLists[nextIndex].items[0], {
        currentList: mLists[nextIndex].id,
        states: _.concat(mLists[nextIndex].items[0].states, tempState)
      })

      if (stateExists) {
        let existingIndex = _.findIndex(newItem.states, {
          id: stateExists.id
        })

        newItem.states.move(existingIndex, newItem.states.length - 1)
      }
      mLists[nextIndex].items[0] = newItem

      // delete element from old place
      mLists[fromIndex].items.splice(indexInList, 1)

      return Object.assign({}, state, {
        ...state,
        lists: mLists
      })

    case 'UPDATE_CARD':
      let uLists = [...state.lists]
      let yIndex = _.findIndex(uLists[nextIndex].items, {
        id: action.process.id
      })
      uLists[nextIndex].items[yIndex] = action.process
      return Object.assign({}, state, {
        ...state,
        lists: uLists,
        movingProcess: null
      })

    case 'CHECK_CARD':
      let lists = [...state.lists]
      let currentListIndex = _.findIndex(lists, {
        id: action.process.currentList
      })

      if (currentListIndex === -1) {
        currentListIndex = 0
      }

      let currentItemIndex = _.findIndex(lists[currentListIndex].items, {
        id: action.process.id
      })
      lists[currentListIndex].items[currentItemIndex].checked = !lists[
        currentListIndex
      ].items[currentItemIndex].checked
      return Object.assign({}, state, {
        lists: lists
      })

    default:
      return state
  }
}
