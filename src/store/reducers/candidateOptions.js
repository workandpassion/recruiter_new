export default function candidateOptions(state = {}, action) {
  if (action.error) {
    return {
      ...action,
      error: action.error
    };
  }
  switch (action.type) {
    case 'GET_ALL_OPTIONS':
      return {
        ...state,
        ...action
      };

    default:
      return state;
  }
}
