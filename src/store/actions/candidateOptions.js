import axios from 'axios'
import { apiClient } from '../axios.config'

let options = []

export function getAllOptions() {
  options = []
  return (dispatch, getState) => {
    return axios
      .all([
        getLanguages(),
        getMunicipalities(),
        getSkills(),
        getOccupations(),
        getDrivingLicenses()
      ])
      .then(
        axios.spread(function(
          languages,
          municipalities,
          skills,
          occupations,
          drivinglicenses
        ) {
          // Both requests are now complete
          languages.data.map((lang, index) => {
            options.push(
              Object.assign({}, lang, {
                label: lang.name,
                value: lang.name,
                type: 'language'
              })
            )
          })

          municipalities.data.map((location, index) => {
            options.push(
              Object.assign({}, location, {
                label: location.name,
                value: location.name,
                type: 'location_parent'
              })
            )
            location.municipalities.map(city => {
              options.push(
                Object.assign({}, city, {
                  label: city.name,
                  value: city.name,
                  type: 'location'
                })
              )
            })
          })

          skills.data.map((skillcat, index) => {
            skillcat.skills.map(skill => {
              options.push(
                Object.assign({}, skill, {
                  label: skill.name,
                  value: skill.name,
                  type: 'skill'
                })
              )
            })
          })

          occupations.data.map((occupation, index) => {
            options.push(
              Object.assign({}, occupation, {
                label: occupation.name,
                value: occupation.name,
                type: 'occupation_parent'
              })
            )
            occupation.occupations.map(occ => {
              options.push(
                Object.assign({}, occ, {
                  label: occ.name,
                  value: occ.name,
                  type: 'occupation'
                })
              )
            })
          })

          drivinglicenses.data.map((license, index) => {
            options.push(
              Object.assign({}, license, {
                label: 'Körkort ' + license.name,
                value: license.name,
                type: 'drivingLicense'
              })
            )
          })

          return dispatch({
            type: 'GET_ALL_OPTIONS',
            options: options,
            receivedAt: Date.now()
          })
        })
      )
  }
}

function getLanguages() {
  return apiClient.get('languages/')
}

function getSkills() {
  return apiClient.get('skills/')
}

function getMunicipalities() {
  return apiClient.get('locations/')
}

function getOccupations() {
  return apiClient.get('occupations/')
}

function getDrivingLicenses() {
  return apiClient.get('driving-licenses/')
}
