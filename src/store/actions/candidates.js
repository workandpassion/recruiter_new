import axios from 'axios'
import { apiClient } from '../axios.config'

export function getCandidates(filterTerms, searchTerms) {
  let params = new URLSearchParams()
  if (searchTerms) {
    for (let i = 0; i < searchTerms.length; i++) {
      // Object.assign(params, { search: searchTerms[i].value })
      params.append('search', searchTerms[i].value)
    }
  }
  if (filterTerms) {
    console.log(filterTerms)
    for (let i = 0; i < filterTerms.length; i++) {
      switch (filterTerms[i].type) {
        case 'occupation':
          params.append('occupation_filter', filterTerms[i].id)
          break
        case 'occupation_parent':
          params.append('occupation_filter', filterTerms[i].id)
          break
        case 'language':
          params.append('language_filter', filterTerms[i].id)
          break
        case 'location':
          params.append('location_filter', filterTerms[i].id)
          break
        case 'location_parent':
          params.append('location_filter', filterTerms[i].id)
          break
        case 'drivingLicense':
          params.append('driving_license_filter', filterTerms[i].id)
          break
        case 'skill':
          params.append('skill_filter', filterTerms[i].id)
          break
        default:
          return params
      }
    }
  }
  return (dispatch, getState) => {
    return (
      apiClient
        .get('recruiter/profiles/', {
          params: params
        })
        // .then(() => {
        //   console.log('got all candidates from server')
        //   // dispatch({
        //   //   type: 'GET_ALL_CANDIDATES',
        //   //   allCandidates: result.data,
        //   //   receivedAt: Date.now()
        //   // })
        //   // console.log('deleted')
        // })
        .catch(function(error) {
          console.log(error)
        })
    )
  }
}

export function getCandidate(candidateId) {
  return (dispatch, getState) => {
    return axios
      .all([
        getCandidateInfo(candidateId),
        getJobApplications(candidateId),
        getAssignments(candidateId)
      ])
      .then(
        axios.spread(function(candidateInfo, jobApplications, assignments) {
          dispatch({
            type: 'GET_CANDIDATE',
            candidateInfo: candidateInfo.data,
            jobApplications: jobApplications.data,
            assignments: assignments.data,
            receivedAt: Date.now()
          })
        })
      )
  }

  function getCandidateInfo(candidateId) {
    return apiClient.get('recruiter/profiles/' + candidateId)
  }

  function getJobApplications(candidateId) {
    return apiClient.get(
      'recruiter/profiles/' + candidateId + '/job-applications/'
    )
  }

  function getAssignments(candidateId) {
    return apiClient.get('recruiter/assignments', {
      params: {
        nested: true,
        user: candidateId
      }
    })
  }
}

export function setCandidate(user) {
  return (dispatch, getState) => {
    const currentState = getState()
    console.log(currentState)

    return new Promise((resolve, reject) => {
      dispatch({
        type: 'SET_CANDIDATE',
        selectedCandidate: user,
        receivedAt: Date.now()
      })

      resolve()
    })
  }
}

export function setCandidateRating(ratingData, candidateId) {
  return (dispatch, getState) => {
    return apiClient
      .post('recruiter/profiles/' + candidateId + '/info/', ratingData)
      .then(result => {
        dispatch({
          type: 'SET_RATING',
          ...result.data
        })
        dispatch({
          type: 'UPDATE_IN_ALL_CANDIDATES',
          id: candidateId,
          rating: ratingData
        })
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}

export function getCandidatesWithoutRating() {
  return (dispatch, getState) => {
    dispatch({
      type: 'GET_CANDIDATES_WITHOUT_RATING'
    })
  }
}

export function getCandidatesWithMPC() {
  return (dispatch, getState) => {
    dispatch({
      type: 'GET_CANDIDATES_WITH_MPC'
    })
  }
}

export function saveTableState(mState) {
  return (dispatch, getState) => {
    return dispatch({
      type: 'TABLE_STATE',
      tableState: mState
    })
  }
}

export function updateSearchTerms(terms, advanced, advancedIsOpen) {
  return (dispatch, getState) => {
    return dispatch({
      type: 'UPDATE_SEARCH_TERMS',
      terms: terms,
      advanced: advanced,
      advancedIsOpen: advancedIsOpen
    })
  }
}

export function getRecommendations() {
  return dispatch => {
    return apiClient
      .get('recruiter/recommendations/', {
        params: {
          nested: true
        }
      })
      .then(result => {
        return dispatch({
          type: 'GET_RECOMMENDATIONS',
          recommendations: result.data
        })
      })
  }
}

export function sendRecommendation(user, to_recruiter) {
  return dispatch => {
    dispatch({
      type: 'SEND_RECOMMENDATION_START'
    })
    return apiClient
      .post('recruiter/recommendations/', {
        ...user,
        ...to_recruiter
      })
      .then(result => {})
  }
}

export function getFolders() {
  return dispatch => {
    return apiClient.get('recruiter/labels/').then(result => {
      let folders = result.data
      let params = new URLSearchParams()
      let funcArray = []

      let i,
        j,
        temparray,
        chunk = 20

      for (let x = 0; x < folders.length; x++) {
        let folderUsers = folders[x].users
        for (i = 0, j = folderUsers.length; i < j; i += chunk) {
          temparray = folderUsers.slice(i, i + chunk)

          for (let y = 0; y < temparray.length; y++) {
            let mUserId = temparray[y]
            params.append('id', mUserId)
            if (y === temparray.length - 1) {
              funcArray.push(
                apiClient.get('recruiter/profiles/', {
                  params: params
                })
              )
              params = new URLSearchParams()
            }
          }
        }
      }

      return axios.all(funcArray).then(results => {
        console.log(results)
        let usersIndex = 0
        for (let i = 0; i < folders.length; i++) {
          if (folders[i].users.length > 0) {
            folders[i] = Object.assign({}, folders[i], {
              users: results[usersIndex].data
            })
            usersIndex++
          }
        }

        return dispatch({
          type: 'GET_FOLDERS',
          folders: folders
        })
      })
    })
  }
}

export function createFolder(folder) {
  return dispatch => {
    return apiClient.post('recruiter/labels/', folder).then(result => {
      return dispatch({
        type: 'CREATE_FOLDER',
        newFolder: result.data
      })
    })
  }
}

export function saveCandidateToFolder(folder) {
  return dispatch => {
    dispatch({
      type: 'SAVE_CANDIDATE_START'
    })

    return apiClient
      .put('recruiter/labels/' + folder.id, folder)
      .then(result => {
        return dispatch({
          type: 'SAVE_CANDIDATE_SUCCESS',
          newFolder: result.data
        })
      })
      .catch(error => {
        return dispatch({
          type: 'SAVE_CANDIDATE_FAIL',
          error: error
        })
      })
  }
}
