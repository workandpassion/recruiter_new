import { apiClient } from '../axios.config'
import axios from 'axios'
import _ from 'lodash'
import { getAssignment } from './assignments'

export function getAllAds() {
  return dispatch => {
    return apiClient
      .get('recruiter/ads/', {
        params: {
          nested: true
        }
      })
      .then(result => {
        return dispatch({
          type: 'GET_ALL_ADS',
          allAds: result.data
        })
      })
  }
}

export function deleteAd(ad) {
  return dispatch => {
    return apiClient.delete('recruiter/ads/' + ad.id).then(result => {
      return dispatch({
        type: 'DELETE_AD'
      })
    })
  }
}

export function getAd(adId) {
  return dispatch => {
    return apiClient.get('recruiter/ads/' + adId).then(result => {
      return dispatch({
        type: 'GET_AD',
        ad: result.data
      })
    })
  }
}

export function createAdPartner(adId, adPartner) {
  return dispatch => {
    dispatch({
      type: 'CREATE_AD_PARTNER',
      creatingAdPartner: true
    })
    return apiClient
      .post('recruiter/ads/' + adId + '/partners/', adPartner)
      .then(result => {
        dispatch({
          type: 'CREATE_AD_PARTNER',
          creatingAdPartner: false
        })
        // dispatch(getAllAds())
      })
  }
}

export function updatePartner(adId, adPartner) {
  return dispatch => {
    dispatch({
      type: 'UPDATE_AD_PARTNER',
      updatingAdPartner: true
    })
    return apiClient
      .put('recruiter/ads/' + adId + '/partners/' + adPartner.id, adPartner)
      .then(result => {
        dispatch({
          type: 'UPDATE_AD_PARTNER',
          updatingAdPartner: false
        })
        // dispatch(getAllAds())
      })
  }
}
