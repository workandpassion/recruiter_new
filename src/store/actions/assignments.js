import { apiClient } from '../axios.config'
import axios from 'axios'
import _ from 'lodash'
import { updateCard } from '../actions/assignmentBoard'

export function getAssignments() {
  return (dispatch, getState) => {
    console.log(getState())
    return axios.all([getAllAssignments(), getMyAssignments()]).then(
      axios.spread(function(allAssignments, myAssignments) {
        let mAssignments = []
        let mAssignment
        allAssignments.data.map(assignment => {
          if (
            _.find(assignment.recruiters, { id: getState().user.profile.id })
          ) {
            mAssignment = Object.assign({ mine: true }, assignment)
          } else {
            mAssignment = Object.assign({ mine: false }, assignment)
          }
          return mAssignments.push(mAssignment)
        })

        return dispatch({
          type: 'GET_ASSIGNMENTS',
          allAssignments: mAssignments,
          myAssignments: myAssignments.data
        })
      })
    )
  }
}

function getAllAssignments() {
  return apiClient.get('recruiter/assignments', {
    params: {
      nested: true
    }
  })
}

function getMyAssignments() {
  return apiClient.get('recruiter/me/assignments', {
    params: {
      nested: true
    }
  })
}

export function setAssignment(ass) {
  return dispatch => {
    return new Promise(resolve => {
      dispatch({
        type: 'SET_ASSIGNMENT',
        selectedAssignment: Object.assign({}, ass, {
          matchedCandidates: []
        }),
        receivedAt: Date.now(),
        fetchingAssignment: true
      })
      resolve()
    })
  }
}

export function getAssignment(id) {
  return dispatch => {
    dispatch({
      type: 'FETCHING_ASSIGNMENT',
      fetchingAssignment: true,
      selectedAssignment: undefined,
      unnestedSelectedAssignment: undefined
    })
    return axios.all([getNestedAssignment(id), getUnnestedAssignment(id)]).then(
      axios.spread(function(nested, unnested) {
        return dispatch({
          type: 'GET_ASSIGNMENT',
          selectedAssignment: nested.data,
          unnestedSelectedAssignment: unnested.data,
          fetchingAssignment: false
        })
      })
    )
  }

  function getNestedAssignment(id) {
    return apiClient.get('recruiter/assignments/' + id, {
      params: {
        nested: true
      }
    })
  }

  function getUnnestedAssignment(id) {
    return apiClient.get('recruiter/assignments/' + id, {
      params: {
        nested: false
      }
    })
  }
}

export function addAssignment(assignment) {
  return dispatch => {
    return apiClient
      .post('recruiter/assignments/', assignment)
      .then(result => {
        dispatch({
          type: 'ADD_ASSIGNMENT',
          mAssignment: result.data
        })
        dispatch(getAssignment(result.data.id))
      })
      .then(() => {
        return dispatch(getAssignments())
      })
  }
}

export function updateAssignment(assignment) {
  return dispatch => {
    return apiClient
      .put('recruiter/assignments/' + assignment.id, assignment)
      .then(result => {
        dispatch({
          type: 'UPDATE_ASSIGNMENT',
          mAssignment: result.data
        })
      })
      .then(() => {
        dispatch(getAssignment(assignment.id))
      })
  }
}

export function deleteAssignment(assignment) {
  return dispatch => {
    return apiClient
      .delete('recruiter/assignments/' + assignment.id)
      .then(() => {
        return dispatch({
          type: 'DELETE_ASSIGNMENT'
        })
      })
  }
}

export function createAssignmentState(assignmentId, state) {
  return dispatch => {
    dispatch({
      type: 'ADD_ASSIGNMENT_STATE',
      creatingAssignmentState: true
    })
    return apiClient
      .post('recruiter/assignments/' + assignmentId + '/states/', state)
      .then(result => {
        dispatch({
          type: 'ADD_ASSIGNMENT_STATE',
          creatingAssignmentState: false
        })
      })
      .then(() => {
        return dispatch(getAssignment(assignmentId))
      })
  }
}

export function updateAssignmentState(assignmentId, state) {
  return dispatch => {
    dispatch({
      type: 'UPDATE_ASSIGNMENT_STATE',
      creatingAssignmentState: true
    })
    return apiClient
      .put(
        'recruiter/assignments/' + assignmentId + '/states/' + state.id,
        state
      )
      .then(result => {
        dispatch({
          type: 'UPDATE_ASSIGNMENT_STATE',
          creatingAssignmentState: false
        })
      })
      .then(() => {
        return dispatch(getAssignment(assignmentId))
      })
  }
}

export function updateAssignmentStates(assignmentId, state) {
  return dispatch => {
    return apiClient
      .delete('recruiter/assignments/' + assignmentId + '/states/' + state.id)
      .then(() => {
        return apiClient.post(
          'recruiter/assignments/' + assignmentId + '/states/',
          Object.assign({}, state, {
            value: state.value.id
          })
        )
      })
      .then(() => {
        return dispatch(getAssignment(assignmentId))
      })
  }
}

export function removeCandidateFromMatchedCandidates(candidate) {
  return (dispatch, getState) => {
    let mCandidates = Object.assign(
      [],
      getState().assignments.selectedAssignment.matchedCandidates
    )
    _.remove(mCandidates, { user_id: candidate.user })
    return new Promise(resolve => {
      dispatch({
        type: 'REMOVE_CANDIDATE_FROM_MATCHES',
        matchedCandidates: mCandidates
      })
      resolve()
    })
  }
}

export function getMatchingCandidates(assignmentId) {
  function setCandidateProgress(candidate) {
    // console.log(candidate)
    let count = 0
    candidate.employments.length > 0 && count++
    candidate.educations.length > 0 && count++
    candidate.languages.length > 0 && count++
    candidate.locations.length > 0 && count++
    candidate.motivations.length > 0 && count++
    candidate.occupations.length > 0 && count++
    candidate.personalities.length > 0 && count++
    candidate.questions.length > 0 && count++
    candidate.skills.length > 0 && count++
    candidate.videos.length > 0 && count++
    candidate.assessment_result && count++
    candidate.personal_info && count++

    let mCandidate = Object.assign({}, candidate, {
      progress: count / 12 * 100
    })
    return mCandidate
  }

  return (dispatch, getState) => {
    dispatch({
      type: 'FETCHING_MATCHED_CANDIDATES'
    })

    return apiClient
      .get('recruiter/assignments/' + assignmentId + '/matching-users/')
      .then(result => {
        let mCandidates = result.data.users

        let filteredCandidates = mCandidates.filter(function(obj) {
          return !_.find(
            getState().assignments.selectedAssignment.recruitment_processes,
            { user: { id: obj.user_id } }
          )
        })

        let params = new URLSearchParams()
        let funcArray = []

        let i,
          j,
          temparray,
          chunk = 20
        for (i = 0, j = filteredCandidates.length; i < j; i += chunk) {
          temparray = filteredCandidates.slice(i, i + chunk)
          // do whatever
          // console.log(temparray)

          for (let x = 0; x < temparray.length; x++) {
            if (!temparray[x].user) {
              let mUserId = temparray[x].user_id
              params.append('id', mUserId)
              if (x === temparray.length - 1) {
                funcArray.push(
                  apiClient.get('recruiter/profiles/', {
                    params: params
                  })
                )
                params = new URLSearchParams()
              }
            }
          }
        }

        console.log(funcArray)
        return axios.all(funcArray).then(results => {
          let result = []
          let temp = results.map(r => r.data)

          for (let i = 0; i < temp.length; i++) {
            result = [...result, ...temp[i]]
          }

          let resultData = result.sort(function(a, b) {
            if (a.new === b.new) return b.score - a.score
            else if (a.new) return -1
            else return 1
          })

          resultData.map((user, index) => {
            let uIndex = _.findIndex(filteredCandidates, {
              user_id: user.user
            })

            filteredCandidates[uIndex] = Object.assign(
              {},
              setCandidateProgress(user),
              {
                ...filteredCandidates[uIndex]
              }
            )
          })

          filteredCandidates.sort(function(a, b) {
            if (a.new !== b.new) return a.new > b.new ? -1 : 1
            if (a.score !== b.score) return a.score > b.score ? -1 : 1
            return a.progress > b.progress ? -1 : 1
          })

          return dispatch({
            type: 'GET_MATCHED_CANDIDATES',
            fetchingMatchedCandidates: false,
            matchedCandidates: filteredCandidates
          })
        })
      })
  }
}

export function createCandidateProcess(assignmentId, process, state) {
  return (dispatch, getState) => {
    let allAssignments = getState().assignments.allAssignments
    // let mAssignment = _.find(allAssignments, {
    //   id: assignmentId
    // })

    let mAssignment = getState().assignments.selectedAssignment
    let candidateExists = _.find(mAssignment.recruitment_processes, {
      user: { id: process.user }
    })

    if (!candidateExists) {
      return apiClient
        .post(
          'recruiter/assignments/' + assignmentId + '/recruitment-processes/',
          process
        )
        .then(result => {
          if (state) {
            return dispatch(
              createCandidateProcessState(assignmentId, result.data.id, state)
            ).then(result => {
              return dispatch(getAssignment(assignmentId))
            })
          }
        })
    } else {
      // return dispatch(getAssignment(assignmentId, true))
    }
  }
}

export function createCandidateProcessState(assignmentId, processId, state) {
  return dispatch => {
    return apiClient.post(
      'recruiter/assignments/' +
        assignmentId +
        '/recruitment-processes/' +
        processId +
        '/states/',
      state
    )
  }
}

export function updateCandidateProcessStates(assignmentId, processId, state) {
  return dispatch => {
    return apiClient
      .delete(
        'recruiter/assignments/' +
          assignmentId +
          '/recruitment-processes/' +
          processId +
          '/states/' +
          state.id
      )
      .then(() => {
        return apiClient.post(
          'recruiter/assignments/' +
            assignmentId +
            '/recruitment-processes/' +
            processId +
            '/states/',
          Object.assign({}, state, {
            value: state.value.id
          })
        )
      })
  }
}

export function updateCandidateProcessState(assignmentId, process, state) {
  return (dispatch, getState) => {
    return apiClient
      .put(
        'recruiter/assignments/' +
          assignmentId +
          '/recruitment-processes/' +
          process.id +
          '/states/' +
          state.id,
        Object.assign({}, state, {
          value: state.value.id ? state.value.id : state.value
        })
      )
      .then(result => {
        console.log(result.data)
        let nextIndex = _.findIndex(getState().assignmentBoard.lists, function(
          list
        ) {
          return _.find(list.items, { id: process.id })
        })

        let mProcess = Object.assign({}, process)
        mProcess.states[mProcess.states.length - 1] = Object.assign(
          {},
          result.data,
          {
            value: {
              id: result.data.value,
              name: mProcess.states[mProcess.states.length - 1].value.name
            }
          }
        )

        dispatch(updateCard(mProcess, nextIndex))
      })
  }
}

export function getProcessStateValues() {
  return (dispatch, getState) => {
    if (
      getState().assignments.processStateValues &&
      getState().assignments.processStateValues.length > 0
    ) {
      return new Promise(resolve => {
        dispatch({
          type: 'GET_PROCESS_STATE_VALUES',
          processStateValues: getState().assignments.processStateValues
        })
        resolve()
      })
    } else {
      return apiClient
        .get('/recruiter/recruitment-processes-state-values')
        .then(result => {
          return dispatch({
            type: 'GET_PROCESS_STATE_VALUES',
            processStateValues: result.data
          })
        })
    }
  }
}

export function getAssignmentStateValues() {
  return (dispatch, getState) => {
    if (
      getState().assignments.recruitmentStateValues &&
      getState().assignments.recruitmentStateValues.length > 0
    ) {
      dispatch({
        type: 'GET_ASSIGNMENT_STATE_VALUES',
        assignmentStateValues: getState().assignments.recruitmentStateValues
      })
    }
    return apiClient.get('recruiter/assignments-state-values').then(result => {
      return dispatch({
        type: 'GET_ASSIGNMENT_STATE_VALUES',
        assignmentStateValues: result.data
      })
    })
  }
}

export function createAd(ad, logo, coverImage) {
  return (dispatch, getState) => {
    return apiClient.post('recruiter/ads/', ad).then(result => {
      if (logo) {
        let fd = new FormData()
        fd.append('file', logo)
        apiClient.post('recruiter/ads/' + result.data.id + '/logo', fd)
      }

      if (coverImage) {
        let fd = new FormData()
        fd.append('file', coverImage)
        apiClient.post('recruiter/ads/' + result.data.id + '/cover-image', fd)
      }

      let assignmentId = getState().assignments.selectedAssignment.id
      return dispatch({
        type: 'CREATE_AD',
        ad: result.data,
        aId: assignmentId
      })
    })
  }
}

export function updateAd(ad, logo, coverImage) {
  return (dispatch, getState) => {
    return apiClient.put('recruiter/ads/' + ad.id, ad).then(result => {
      if (logo) {
        let fd = new FormData()
        fd.append('file', logo)
        apiClient.post('recruiter/ads/' + ad.id + '/logo', fd)
      }

      if (coverImage) {
        let fd = new FormData()
        fd.append('file', coverImage)
        apiClient.post('recruiter/ads/' + ad.id + '/cover-image', fd)
      }

      let assignmentId = getState().assignments.selectedAssignment.id
      return dispatch({
        type: 'UPDATE_AD',
        ad: result.data,
        aId: assignmentId
      })
    })
  }
}

export function deleteAd(ad) {
  return dispatch => {
    return apiClient.delete('recruiter/ads/' + ad.id).then(result => {
      return dispatch({
        type: 'DELETE_AD'
      })
    })
  }
}
