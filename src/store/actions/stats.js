import moment from 'moment'
import { apiClient } from '../axios.config'
import axios from 'axios'

export function getAllLagerStats(occupations) {
  return (dispatch, getState) => {
    let promises = []
    let stats = []

    for (let i = 0; i < occupations.length; i++) {
      let promise = new Promise((resolve, reject) => {
        let mFunction = dispatch(getLagerStats(occupations[i]))

        return mFunction
          .then(result => {
            // console.log(result)
            let newStat = {
              name: occupations[i].name,
              id: occupations[i].id,
              totalCount: result.total,
              weekCount: result.week
            }
            stats.push(newStat)
            resolve('Valid')
          })
          .catch(err => {
            reject(err)
          })
      })
      promises.push(promise)
    }

    return Promise.all(promises)
      .then(result => {
        stats.sort(function(a, b) {
          if (a.name < b.name) return -1
          if (a.name > b.name) return 1
          return 0
        })
        return dispatch({
          type: 'GET_LAGER_STATS',
          stats
        })
      })
      .catch(function(result) {
        // if any image fails to load, then() is skipped and catch is called
        console.log(result) // returns array of images that failed to load
      })
  }
}

function getLagerStats(occupation) {
  return () => {
    return axios
      .all([getTotalLagerStats(occupation), getWeekLagerStats(occupation)])
      .then(
        axios.spread(function(total, week) {
          // Both requests are now complete
          return { total: total.data.count, week: week.data.count }
        })
      )
  }
}

function getTotalLagerStats(occupation) {
  return apiClient.get('recruiter/profiles/count/', {
    params: {
      start_date: '2017-01-01',
      occupation_filter: occupation.id
    }
  })
}

function getWeekLagerStats(occupation) {
  let dateFrom = moment()
    .subtract(7, 'd')
    .format('YYYY-MM-DD')
  return apiClient.get('recruiter/profiles/count/', {
    params: {
      start_date: dateFrom,
      occupation_filter: occupation.id
    }
  })
}

// =======================

export function getWapcardStats(dateFrom, dateTo) {
  return (dispatch, getState) => {
    let promises = []
    let stats = []
    let totalStats = []
    let dayCount = moment(dateTo).diff(dateFrom, 'days')
    let lastWapStat = 0
    let lastBehandladeStat = 0

    return axios
      .all([
        dispatch(
          getTotalWapcardsBeforeDate(moment(dateFrom).format('YYYY-MM-DD'))
        ),
        dispatch(
          getTotalBehandladeBeforeDate(moment(dateFrom).format('YYYY-MM-DD'))
        )
      ])
      .then(
        axios.spread(function(
          totalWapcardsBeforeDate,
          totalBehandladeBeforeDate
        ) {
          lastWapStat = totalWapcardsBeforeDate.data.count
          lastBehandladeStat = totalBehandladeBeforeDate.data.count

          for (let i = 0; i < dayCount; i++) {
            let thisDate = moment(dateFrom)
              .add(i, 'd')
              .format('YYYY-MM-DD')
            let thisEndDate = moment(thisDate)
              .add(1, 'd')
              .format('YYYY-MM-DD')
            // console.log(thisDate + ' - ' + thisEndDate)
            let promise = new Promise((resolve, reject) => {
              // let mFunction = dispatch(getWapStatsOnDate(thisDate, thisEndDate))
              let mFunction = axios
                .all([
                  dispatch(getWapStatsOnDate(thisDate, thisEndDate)),
                  dispatch(getBehandladeOnDate(thisDate, thisEndDate))
                ])
                .then(
                  axios.spread(function(wapcardsPerDay, behandlade) {
                    // Both requests are now complete
                    return {
                      total: wapcardsPerDay.data.count,
                      behandlade: behandlade.data.count
                    }
                  })
                )
              return mFunction
                .then(result => {
                  let newStat = {
                    date: thisDate,
                    count_per_day: result.total,
                    behandlade_per_day: result.behandlade
                  }

                  totalStats.push(lastWapStat)
                  stats.push(newStat)
                  resolve('Valid')
                })
                .catch(err => {
                  reject(err)
                })
            })
            promises.push(promise)
          }

          return Promise.all(promises)
            .then(result => {
              stats.sort(function(a, b) {
                if (a.date < b.date) return -1
                if (a.date > b.date) return 1
                return 0
              })
              // totalStats.sort(function (a, b) {
              //   return a - b
              // })

              for (let i = 0; i < stats.length; i++) {
                console.log(stats[i].date)
                lastWapStat += stats[i].count_per_day
                lastBehandladeStat += stats[i].behandlade_per_day
                stats[i] = Object.assign(
                  {
                    totalCount: lastWapStat,
                    totalBehandlade: lastBehandladeStat
                  },
                  stats[i]
                )
              }

              return dispatch({
                type: 'GET_WAP_STATS',
                stats
              })
            })
            .catch(function(result) {
              // if any image fails to load, then() is skipped and catch is called
              console.log(result) // returns array of images that failed to load
            })
        })
      )
  }
}

function getWapStatsOnDate(dateFrom, dateTo) {
  return () => {
    return apiClient
      .get('recruiter/profiles/count/', {
        params: {
          start_date: dateFrom,
          end_date: dateTo
        }
      })
      .then(result => {
        // console.log(result)
        return result
      })
  }
}

function getBehandladeOnDate(dateFrom, dateTo) {
  return () => {
    return apiClient
      .get('recruiter/profiles/count/', {
        params: {
          rating_min: 1,
          start_date: dateFrom,
          end_date: dateTo
        }
      })
      .then(result => {
        // console.log(result)
        return result
      })
  }
}

function getTotalWapcardsBeforeDate(dateFrom) {
  return () => {
    return apiClient
      .get('recruiter/profiles/count/', {
        params: {
          start_date: '2017-01-01',
          end_date: dateFrom
        }
      })
      .then(result => {
        console.log('total before date')
        console.log(result)
        return result
      })
  }
}

function getTotalBehandladeBeforeDate(dateFrom) {
  return () => {
    return apiClient
      .get('recruiter/profiles/count/', {
        params: {
          rating_min: 1,
          start_date: '2017-01-01',
          end_date: dateFrom
        }
      })
      .then(result => {
        // console.log(result)
        return result
      })
  }
}
