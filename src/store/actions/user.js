import { apiClient } from '../axios.config'
import axios from 'axios'
import Cookies from 'universal-cookie'
const cookies = new Cookies()

export function login(creds) {
  let token = null
  return (dispatch, getState) => {
    dispatch({
      type: 'LOGIN'
    })
    return apiClient
      .post('token/', creds)
      .then(result => {
        token = result.data.token
        apiClient.defaults.headers = {
          Authorization: 'Token ' + result.data.token
        }
      })
      .then(() => {
        axios.all([_getProfile(), _getProfileOfficeAndAssignments()]).then(
          axios.spread(function(profile, officeAndAssignment) {
            let officeFuncArray = []

            for (let i = 0; i < officeAndAssignment.data.offices.length; i++) {
              officeFuncArray.push(
                _getNestedOffice(officeAndAssignment.data.offices[i].id)
              )
            }

            axios.all(officeFuncArray).then(results => {
              let mOffices = []
              results.map(result => {
                mOffices.push(result.data)
              })

              dispatch({
                type: 'GET_PROFILE',
                profile: profile.data,
                offices: mOffices,
                picture: 123
              })
              dispatch({
                type: 'LOGIN_SUCCESS',
                token: token,
                receivedAt: Date.now()
              })
            })
          })
        )
      })
  }
}

export function updateProfile(profile) {
  return dispatch => {
    dispatch({
      type: 'UPDATING_PROFILE_START'
    })

    return apiClient.post('/me', profile).then(result => {
      dispatch({
        type: 'UPDATING_PROFILE_SUCCESS',
        profile: result.data
      })
    })
  }
}

export function logout() {
  return {
    type: 'LOGOUT'
  }
}

export function update() {
  return (dispatch, getState) => {
    cookies.set('update', getState().user.token, { path: '/', maxAge: 60 })
    return dispatch({
      type: 'UPDATE'
    })
  }
}

export function setTokenAfterUpdate(token) {
  return dispatch => {
    return dispatch({
      type: 'SET_TOKEN',
      token: token
    })
  }
}

function _getProfile() {
  return apiClient.get('me/')
}

function _getProfileOfficeAndAssignments() {
  return apiClient.get('recruiter/me/')
}

function _getNestedOffice(officeId) {
  return apiClient.get('recruiter/offices/' + officeId, {
    params: {
      nested: 'true'
    }
  })
}

export function getAppSettings() {
  return dispatch => {
    return apiClient.get('recruiter/settings/')
  }
}
