import _ from 'lodash'
import moment from 'moment'
import {
  createCandidateProcessState,
  updateCandidateProcessStates,
  getAssignment
} from './assignments'

function movingCard(process) {
  return {
    type: 'MOVING_CARD',
    movingProcess: process.id
  }
}

export function moveCard(process, moveTo) {
  // moveTo = processStateValues

  return (dispatch, getState) => {
    // let currentList = process.states[process.states.length - 1].value.id
    let { currentList } = process
    let inList = _.find(moveTo, { id: currentList })

    if (!inList) {
      dispatch(movingCard(process))
      let nextIndex = _.findIndex(getState().assignmentBoard.lists, {
        id: moveTo[0].id
      })
      let fromIndex = _.findIndex(getState().assignmentBoard.lists, {
        id: currentList
      })

      if (fromIndex === -1) {
        fromIndex = 0
      }

      let tempState = getState()
      let mItems = getState().assignmentBoard.lists[fromIndex].items
      let indexInList = _.findIndex(mItems, {
        id: process.id
      })
      console.log(fromIndex + ' => ' + nextIndex)
      console.log('Index in list: ' + indexInList)
      //TODO: Create new state on server
      let stateExists = _.find(process.states, {
        value: { id: moveTo[0].id }
      })

      let assignmentId = getState().assignments.selectedAssignment.id

      if (stateExists) {
        dispatch(
          updateCandidateProcessStates(assignmentId, process.id, stateExists)
        ).then(result => {
          dispatch(getAssignment(assignmentId, true)).then(result => {
            dispatch(updateCard(process, nextIndex))
          })
        })
      } else {
        dispatch(
          createCandidateProcessState(assignmentId, process.id, {
            value: moveTo[0].id,
            start_date: moment().format('YYYY-MM-DD')
          })
        ).then(result => {
          dispatch(getAssignment(assignmentId, true)).then(result => {
            dispatch(updateCard(process, nextIndex))
          })
        })
      }

      return dispatch({
        type: 'MOVE_CARD',
        fromIndex: fromIndex,
        nextIndex: nextIndex,
        indexInList: indexInList,
        process: process,
        stateExists: stateExists
      })
    }

    return
  }
}

export function checkCard(process) {
  return dispatch => {
    return dispatch({
      type: 'CHECK_CARD',
      process: process
    })
  }
}

export function updateCard(process, nextIndex) {
  return (dispatch, getState) => {
    let { recruitment_processes } = getState().assignments.selectedAssignment
    let mProcess = _.find(recruitment_processes, { id: process.id })
    let mCurrentList = mProcess.states[mProcess.states.length - 1].value.id

    let newProcess = Object.assign({}, process, {
      states: mProcess.states,
      currentList: mCurrentList
    })

    return dispatch({
      type: 'UPDATE_CARD',
      movingProcess: null,
      process: newProcess,
      nextIndex: nextIndex
    })
  }
}

export function setupBoard() {
  return (dispatch, getState) => {
    dispatch(settingUpBoard())

    dispatch(setupLists())
  }
}

function settingUpBoard() {
  return {
    type: 'SETTING_UP_BOARD',
    lists: undefined,
    settingUpBoard: true
  }
}

function setupLists() {
  return (dispatch, getState) => {
    let { processStateValues } = getState().assignments
    let processes = getState().assignments.selectedAssignment
      .recruitment_processes
    let mValuesList = Object.assign([], processStateValues)
    let lists = []

    for (let i = 0; i < mValuesList.length; i++) {
      let processStateValue = mValuesList[i]

      if (processStateValue.icon !== '') {
        let shouldBeGrouped = _.remove(mValuesList, function(value) {
          return value.icon === processStateValue.icon
        })

        lists.push({
          id: shouldBeGrouped[0].id,
          name: shouldBeGrouped[0].icon,
          processStateValues: shouldBeGrouped,
          items: getProcesses(_.map(shouldBeGrouped, 'id'), processes)
        })

        i--
      } else {
        lists.push({
          id: processStateValue.id,
          name: processStateValue.name,
          processStateValues: [processStateValue],
          items: getProcesses([processStateValue.id], processes)
        })
      }
    }

    return dispatch({
      type: 'SETUP_LISTS',
      lists: lists,
      settingUpBoard: false
    })
  }
}

function getProcesses(stateValueIds, processes) {
  let mProcesses = []

  processes &&
    processes.map((process, index) => {
      if (process.states.length > 0) {
        if (
          _.find(stateValueIds, function(o) {
            return o === process.states[process.states.length - 1].value.id
          })
        ) {
          mProcesses.push(
            Object.assign({}, process, {
              currentList: process.states[process.states.length - 1].value.id,
              checked: false
            })
          )
        }
      }
    })

  return mProcesses
}
