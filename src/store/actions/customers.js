import { apiClient } from '../axios.config'
import axios from 'axios'
import _ from 'lodash'

export function getAllCustomers() {
  return (dispatch, getState) => {
    dispatch({
      type: 'GET_ALL_CUSTOMERS',
      fetchingCustomers: true
    })
    return apiClient
      .get('recruiter/customers', {
        params: {
          nested: true
        }
      })
      .then(result => {
        return dispatch({
          type: 'GET_ALL_CUSTOMERS',
          fetchingCustomers: false,
          allCustomers: result.data
        })
      })
  }
}

export function setCustomer(customer) {
  return dispatch => {
    return dispatch({
      type: 'SET_CUSTOMER',
      selectedCustomer: customer,
      receivedAt: Date.now()
    })
  }
}

export function getCustomer(id) {
  return (dispatch, getState) => {
    return axios.all([getCustomerData(id), getCustomerAssignments(id)]).then(
      axios.spread(function(customerData, customerAssignments) {
        let mAssignment
        let mAssignments = []

        customerAssignments.data.length > 0 &&
          customerAssignments.data.map(assignment => {
            if (
              _.find(assignment.recruiters, { id: getState().user.profile.id })
            ) {
              mAssignment = Object.assign({ mine: true }, assignment)
            } else {
              mAssignment = Object.assign({ mine: false }, assignment)
            }
            return mAssignments.push(mAssignment)
          })

        return dispatch({
          type: 'GET_CUSTOMER',
          selectedCustomer: {
            ...customerData.data,
            assignments: mAssignments
          },
          fetchedCustomer: true
        })
      })
    )
  }
}

function getCustomerData(id) {
  return apiClient.get('recruiter/customers/' + id, {
    params: {
      nested: true
    }
  })
}

function getCustomerAssignments(id) {
  return apiClient.get('recruiter/assignments/', {
    params: {
      nested: true,
      customer: id
    }
  })
}

export function createCustomer(customer) {
  return dispatch => {
    dispatch({
      type: 'CREATE_CUSTOMER',
      creatingCustomer: true
    })
    return apiClient
      .post('recruiter/customers', customer)
      .then(() => {
        dispatch({
          type: 'CREATE_CUSTOMER',
          creatingCustomer: false
        })
        return dispatch(getAllCustomers())
      })
      .catch(error => {
        dispatch({
          type: 'CREATE_CUSTOMER',
          creatingCustomer: false
        })
      })
  }
}

export function updateCustomer(customer) {
  return dispatch => {
    dispatch({
      type: 'UPDATE_CUSTOMER',
      creatingCustomer: true
    })
    return apiClient
      .put('recruiter/customers/' + customer.id, customer)
      .then(() => {
        dispatch({
          type: 'UPDATE_CUSTOMER',
          creatingCustomer: false
        })
        return dispatch(getCustomer(customer.id))
      })
  }
}

export function deleteCustomer(customerId) {
  return dispatch => {
    dispatch({
      type: 'DELETE_CUSTOMER',
      deletingCustomer: true
    })

    return apiClient.delete('recruiter/customers/' + customerId).then(() => {
      dispatch({
        type: 'DELETE_CUSTOMER',
        deletingCustomer: false
      })
      return dispatch(getAllCustomers())
    })
  }
}

export function createContactPerson(customer, contactperson) {
  return dispatch => {
    return apiClient
      .post('recruiter/customers/' + customer.id + '/contacts', contactperson)
      .then(() => {
        dispatch({
          type: 'CREATE_CONTACT_PERSON'
        })
        return dispatch(getAllCustomers())
      })
  }
}

export function deleteContactPerson(customer, contactperson) {
  return dispatch => {
    return apiClient
      .delete(
        'recruiter/customers/' + customer.id + '/contacts/' + contactperson.id
      )
      .then(() => {
        dispatch({
          type: 'DELETE_CONTACT'
        })
      })
  }
}
