import axios from 'axios'
// import getStore from '../store/createStore'

let instance = axios.create()
// instance.defaults.baseURL = 'https://api.wapcard.se/api/v1/'
if (process.env.NODE_ENV === 'development') {
  instance.defaults.baseURL = 'https://api.wapcard.se/api/v1/'
  // instance.defaults.baseURL = 'https://wapstage.leinfors.com/api/v1/'
  // instance.defaults.baseURL = 'https://assignments.workandpassion.bid/api/v1/'
  // instance.defaults.baseURL = 'https://profile.workandpassion.bid/api/v1/'
} else {
  // instance.defaults.baseURL = 'https://api.wapcard.se/api/v1/'
  // instance.defaults.baseURL = 'https://wapstage.leinfors.com/api/v1/'
  instance.defaults.baseURL = 'https://assignments.workandpassion.bid/api/v1/'
}

// instance.defaults.timeout = 60000
// let store = getStore()

instance.interceptors.request.use(
  config => {
    if (config.url[config.url.length - 1] !== '/') {
      config.url += '/'
    }
    return config
  },
  function(error) {
    // Do something with request error
    // return Promise.reject(error);
    console.log('error in axios config')
  }
)

export const apiClient = instance

export default function setListener(store) {
  if (store) {
    store.subscribe(listener)

    function listener() {
      let token = store.getState().user.token
      if (token) {
        instance.defaults.headers = {
          Authorization: 'Token ' + token,
          'Content-Type': 'application/json'
        }
      }
    }
  }
}
function getBaseUrl() {
  return instance.defaults.baseURL
}
