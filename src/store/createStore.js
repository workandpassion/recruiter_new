import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './reducers'
import { persistStore } from 'redux-persist'
import setListener from './axios.config'

export const history = createHistory()
let mStore = undefined

function savePreloadedState({ getState }) {
  return next => action => {
    const returnValue = next(action)
    // just point the __PRELOADED_STATE__ at the state after this action.
    window.__PRELOADED_STATE__ = getState()

    // we're not modifying the state, just spying on it.
    return returnValue
  }
}

export default function configureStore() {
  const enhancers = []
  const middleware = [thunk, routerMiddleware(history)]

  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.devToolsExtension

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
  )

  const store = createStore(rootReducer, composedEnhancers)
  mStore = store
  setListener(store)

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  const persistor = persistStore(store)
  return { persistor, store, history }
}

// export function getStore() {
//   return mStore
// }
