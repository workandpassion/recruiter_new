import React from 'react'
import { connect } from 'react-redux'
import { Card, CardBody, Container, Row, Col } from 'reactstrap'
import SubHeader from '../../layouts/Header/SubHeader'
import { AvForm, AvGroup, AvField } from 'availity-reactstrap-validation'
import LoadingButton from '../../components/LoadingButton/LoadingButton'
import { updateProfile } from '../../store/actions/user'

class RecruiterProfile extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      profile: props.profile
    }

    this.onValidSubmit = this.onValidSubmit.bind(this)
  }

  onValidSubmit(event, values) {
    let { dispatch } = this.props
    let profile = {
      ...this.props.profile,
      ...values
    }

    dispatch(updateProfile(profile))
  }

  render() {
    const { updatingProfile, profile } = this.props

    return (
      <div>
        <SubHeader />
        <Container className="mt-5">
          <AvForm model={profile} onValidSubmit={this.onValidSubmit}>
            <Row>
              <Col xs={12}>
                <Card>
                  <CardBody>
                    <Row>
                      <AvGroup className="col-12 col-md-6">
                        <AvField
                          type="text"
                          name="first_name"
                          label="Förnamn"
                          required
                        />
                      </AvGroup>
                      <AvGroup className="col-12 col-md-6">
                        <AvField
                          type="text"
                          name="last_name"
                          label="Efternamn"
                          required
                        />
                      </AvGroup>
                      <AvGroup className="col-12 col-md-6">
                        <AvField
                          type="email"
                          name="email"
                          label="Epost"
                          required
                        />
                      </AvGroup>
                      <AvGroup className="col-12 col-md-6">
                        <AvField
                          type="text"
                          name="mobile_phone_number"
                          label="Mobiltelefon"
                        />
                      </AvGroup>
                      <AvGroup className="col-12 col-md-6">
                        <AvField
                          type="text"
                          name="phone_number"
                          label="Telefon"
                        />
                      </AvGroup>
                    </Row>
                    <Row>
                      <Col>
                        <LoadingButton type="submit" loading={updatingProfile}>
                          Uppdatera
                        </LoadingButton>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </AvForm>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  profile: state.user.profile,
  updatingProfile: state.user.updatingProfile
})

export default connect(mapStateToProps)(RecruiterProfile)
