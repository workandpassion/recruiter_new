import React from 'react'
import ReactTable from 'react-table'
import { withRouter } from 'react-router-dom'

class CustomersTable extends React.Component {
  constructor(props) {
    super(props)

    this.clickHandler = this.clickHandler.bind(this)
  }

  clickHandler(id) {
    this.props.history.push(`/customers/${id}`)
  }
  render() {
    return (
      <ReactTable
        data={this.props.customers}
        className="-highlight"
        filterable
        previousText="Föregående"
        nextText="Nästa"
        loadingText="Hämtar kunder..."
        noDataText="Inga kunder"
        pageText="Sida"
        ofText="av"
        rowsText="rader"
        loading={this.props.loading}
        getTdProps={(state, rowInfo, column, instance) => {
          return {
            onClick: (e, handleOriginal) => {
              console.log('A Td Element was clicked!')
              console.log('it produced this event:', e)
              console.log('It was in this column:', column)
              console.log('It was in this row:', rowInfo)
              console.log('It was in this table instance:', instance)
              if (column.id && rowInfo) {
                this.clickHandler(rowInfo.row._original.id)
              } else if (rowInfo) {
                handleOriginal()
              }
            }
          }
        }}
        columns={[
          {
            Header: 'Kund',
            accessor: 'name'
          },
          {
            Header: 'Org.nr',
            accessor: 'organization_number'
          }
        ]}
        // SubComponent={row => {
        //   return <div style={{ padding: '20px' }} />
        // }}
      />
    )
  }
}

export default withRouter(CustomersTable)
