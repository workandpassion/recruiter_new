import React from 'react'
import ReactTable from 'react-table'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  deleteContactPerson,
  getCustomer
} from '../../../store/actions/customers'

class ContactsTable extends React.Component {
  constructor(props) {
    super(props)

    this.deleteContact = this.deleteContact.bind(this)
  }

  getPageSize(contactsLength) {
    if (contactsLength < 6) {
      return 5
    } else if (contactsLength > 5 && contactsLength < 11) {
      return 10
    } else if (contactsLength > 10 && contactsLength < 21) {
      return 20
    } else if (contactsLength > 20) {
      return 25
    }
  }

  deleteContact(contactperson) {
    let { dispatch } = this.props
    if (
      window.confirm(
        'Är du säker på att du vill ta bort den här kontaktpersonen?'
      ) === true
    ) {
      this.props
        .dispatch(
          deleteContactPerson(
            this.props.customers.selectedCustomer,
            contactperson
          )
        )
        .then(() => {
          dispatch(getCustomer(this.props.customers.selectedCustomer.id))
        })
    }
  }

  render() {
    return (
      <ReactTable
        data={this.props.contacts}
        pageSize={this.getPageSize(this.props.contacts.length)}
        className="-highlight"
        filterable
        previousText="Föregående"
        nextText="Nästa"
        loadingText="Hämtar kontaktpersoner..."
        noDataText="Inga kontaktpersoner"
        pageText="Sida"
        ofText="av"
        rowsText="rader"
        getTdProps={(state, rowInfo, column, instance) => {
          return {
            onClick: (e, handleOriginal) => {
              if (column.className === 'delete') {
                this.deleteContact(rowInfo.row._original)
              }
              if (column.id && rowInfo) {
              } else if (rowInfo) {
                handleOriginal()
              }
            }
          }
        }}
        columns={[
          {
            Header: 'Namn',
            accessor: 'name'
          },
          {
            Header: 'Titel',
            accessor: 'title'
          },
          {
            Header: 'Telefon',
            accessor: 'phone'
          },
          {
            Header: 'Epost',
            accessor: 'email'
          },
          {
            Header: '',
            filterable: false,
            className: 'delete',
            maxWidth: 30,
            Cell: row => <i className="fa fa-minus-circle fg-red tableDelete" />
          }
        ]}
      />
    )
  }
}

ContactsTable.propTypes = {
  contacts: PropTypes.array.isRequired
}

export default connect(state => state)(ContactsTable)
