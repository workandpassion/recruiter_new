import React from 'react'
import { connect } from 'react-redux'
import { createContactPerson } from '../../../store/actions/customers'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import {
  Row,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button
} from 'reactstrap'

class ContactPersonForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      modal: props.show,
      contact_person: this.props.contact_person
        ? this.props.contact_person
        : { name: ' ' }
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValidSubmit = this.handleValidSubmit.bind(this)
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        contact_person: {
          ...this.state.contact_person,
          [name]: value
        }
      })
    }
  }
  handleValidSubmit(event, values) {
    console.log(event)
    console.log(values)
    let { dispatch } = this.props
    dispatch(
      createContactPerson(this.props.customer, this.state.contact_person)
    ).then(() => {
      this.props.parentCallback()
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.contact_person &&
      prevProps.contact_person.name !== this.props.contact_person.name
    ) {
      this.setState({
        contact_person: this.props.contact_person
      })
    }
  }

  render() {
    return (
      <Modal
        isOpen={this.props.show}
        toggle={this.props.parentCallback}
        className="modal-lg"
      >
        {this.state.contact_person && (
          <AvForm
            onValidSubmit={this.handleValidSubmit}
            model={this.state.contact_person}
          >
            <ModalHeader>Ny kontaktperson</ModalHeader>
            <ModalBody>
              <Row>
                <Col xs={12} lg={6}>
                  <AvGroup>
                    <AvField
                      name="name"
                      label="Namn"
                      required
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>

                <Col xs={12} lg={6}>
                  <AvGroup>
                    <AvField
                      name="title"
                      label="Titel"
                      required
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
              </Row>
              <Row>
                <Col xs={12} lg={6}>
                  <AvGroup>
                    <AvField
                      name="email"
                      label="Epost"
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
                <Col xs={12} lg={6}>
                  <AvGroup>
                    <AvField
                      name="phone"
                      label="Telefon"
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type="submit">
                Skapa kontaktperson
              </Button>{' '}
              <Button color="secondary" onClick={this.props.parentCallback}>
                Avbryt
              </Button>
            </ModalFooter>
          </AvForm>
        )}
      </Modal>
    )
  }
}

export default connect(state => state)(ContactPersonForm)
