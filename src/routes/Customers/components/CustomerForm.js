import React from 'react'
import { connect } from 'react-redux'
import {
  createCustomer,
  updateCustomer
} from '../../../store/actions/customers'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import {
  Row,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button
} from 'reactstrap'
import _ from 'lodash'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'

class CustomerForm extends React.Component {
  constructor(props) {
    super(props)

    let mEditMode = _.findIndex(props.customers.allCustomers, {
      id: props.customer ? props.customer.id : '1'
    })

    this.state = {
      modal: props.show,
      customer: this.props.customer
        ? {
            ...this.props.customer,
            contacts: this.props.customer.contacts
              ? _.map(this.props.customer.contacts, 'id')
              : []
          }
        : {
            name: ' ',
            contacts: []
          },
      editMode: mEditMode > -1
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValidSubmit = this.handleValidSubmit.bind(this)
    this.toggle = this.toggle.bind(this)
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        customer: {
          ...this.state.customer,
          contacts: [],
          [name]: value
        }
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.customer &&
      prevProps.customer.name !== this.props.customer.name
    ) {
      this.setState({
        customer: this.props.customer
      })
    }
  }

  toggle() {
    this.setState({ modal: !this.state.modal })
  }

  handleValidSubmit(event, values) {
    console.log(event)
    console.log(values)
    let { dispatch } = this.props

    if (this.state.editMode) {
      dispatch(updateCustomer(this.state.customer)).then(() => {
        this.props.parentCallback()
      })
    } else {
      dispatch(createCustomer(this.state.customer)).then(() => {
        this.props.parentCallback()
      })
    }
  }

  render() {
    return (
      <Modal isOpen={this.props.show} toggle={this.props.parentCallback}>
        {this.state.customer.name && (
          <AvForm
            onValidSubmit={this.handleValidSubmit}
            model={this.state.customer}
          >
            <ModalHeader>
              {this.state.editMode ? 'Redigera kund' : 'Ny kund'}
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col xs={12}>
                  <AvGroup>
                    <AvField
                      name="name"
                      label="Namn"
                      required
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
                <Col xs={12} className="d-none">
                  <AvGroup>
                    <AvField
                      name="organization_number"
                      label="Organisationsnummer"
                      pattern="^(19|20)?[0-9]{6}[- ]?[0-9]{4}$"
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
                <Col xs={12}>
                  <AvGroup>
                    <AvField
                      type="textarea"
                      name="address"
                      label="Adress"
                      onChange={this.handleInputChange}
                      rows="3"
                    />
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <LoadingButton
                color="primary"
                type="submit"
                loading={this.props.customers.creatingCustomer}
              >
                {this.state.editMode ? 'Redigera' : 'Skapa kund'}
              </LoadingButton>{' '}
              <Button color="secondary" onClick={this.props.parentCallback}>
                Avbryt
              </Button>
            </ModalFooter>
          </AvForm>
        )}
      </Modal>
    )
  }
}

export default connect(state => state)(CustomerForm)
