import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { getAllCustomers, setCustomer } from '../../store/actions/customers'
import { Card, CardBody, Container } from 'reactstrap'
import CustomersTable from './components/CustomersTable'
import SubHeader from '../../layouts/Header/SubHeader'
import CustomerForm from './components/CustomerForm'

let clickedId
class AllCustomers extends React.Component {
  constructor(props) {
    super(props)

    document.title = 'Alla kunder | WAP recruiter'

    this.state = {
      loadsave: true,
      customerModal: false
    }

    this.addedCustomer = this.addedCustomer.bind(this)
    this.toggleCustomerModal = this.toggleCustomerModal.bind(this)
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getAllCustomers())
  }

  addedCustomer() {
    this.setState({
      customerModal: false
    })
  }

  toggleCustomerModal() {
    this.setState({
      customerModal: !this.state.customerModal
    })
  }

  render() {
    let actions = [
      {
        title: 'Ny kund',
        icon: 'fa-plus-circle',
        fn: this.toggleCustomerModal
      }
    ]

    return (
      <div>
        <SubHeader actions={actions} />
        <Container fluid className="contentContainer">
          <Card>
            <CardBody>
              <CustomersTable
                customers={this.props.customers.allCustomers}
                loading={this.props.customers.fetchingCustomers}
                clickHandler={this.setClickedCustomerInRedux}
              />
            </CardBody>
          </Card>
        </Container>
        {this.state.customerModal && (
          <CustomerForm
            show={this.state.customerModal}
            parentCallback={this.addedCustomer}
          />
        )}
      </div>
    )
  }
}

export default withRouter(connect(state => state)(AllCustomers))
