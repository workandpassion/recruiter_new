import React from 'react'
import SubHeader, { ActionButtons } from '../../layouts/Header/SubHeader'
import { Container, Card, CardBody, CardHeader, CardTitle } from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { getCustomer, deleteCustomer } from '../../store/actions/customers'
import ContactsTable from './components/ContactsTable'
import AssignmentsTable from '../Assignments/components/AssignmentsTable'
import AssignmentsForm from '../Assignments/components/AssignmentsForm'
import ContactPersonForm from './components/ContactPersonForm'
import CustomerForm from './components/CustomerForm'

class Customer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showContactModal: false,
      showAssignmentModal: false,
      showCustomerModal: false
    }

    this.toggleContactModal = this.toggleContactModal.bind(this)
    this.toggleAssignmentModal = this.toggleAssignmentModal.bind(this)
    this.toggleCustomerModal = this.toggleCustomerModal.bind(this)
    this.deleteCustomer = this.deleteCustomer.bind(this)
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getCustomer(this.props.match.params.customerId))
  }

  toggleCustomerModal() {
    this.setState({
      showCustomerModal: !this.state.showCustomerModal
    })
  }

  toggleContactModal() {
    let { dispatch } = this.props
    dispatch(getCustomer(this.props.match.params.customerId))
    this.setState({
      showContactModal: !this.state.showContactModal
    })
  }

  toggleAssignmentModal() {
    let { dispatch } = this.props
    dispatch(getCustomer(this.props.match.params.customerId))
    this.setState({
      showAssignmentModal: !this.state.showAssignmentModal
    })
  }

  deleteCustomer() {
    if (
      window.confirm('Är du säker på att du vill ta bort den här kunden?') ===
      true
    ) {
      this.props
        .dispatch(deleteCustomer(this.props.match.params.customerId))
        .then(() => {
          this.props.history.push('/customers')
        })
    }
  }

  render() {
    let assignmentActions = [
      {
        title: 'Nytt uppdrag',
        icon: 'fa-plus-circle',
        id: 'newAssignment',
        fn: this.toggleAssignmentModal
      }
    ]

    let contactActions = [
      {
        title: 'Ny kontaktperson',
        icon: 'fa-plus-circle',
        id: 'newContact',
        fn: this.toggleContactModal
      }
    ]

    let actions = [
      {
        title: 'Redigera kund',
        icon: 'fa-pencil',
        fn: this.toggleCustomerModal
      },
      {
        title: 'Radera kund',
        icon: 'fa-trash',
        fn: this.deleteCustomer
      }
    ]

    return (
      <div>
        <SubHeader actions={actions} />
        <Container fluid className="contentContainer">
          <Card>
            <CardHeader>
              <CardTitle>Uppdrag</CardTitle>
              <ActionButtons actions={assignmentActions} />
            </CardHeader>
            <CardBody>
              <AssignmentsTable
                loading={!this.props.customers.fetchedCustomer}
                showMineColumn
                assignmentsList={
                  this.props.customers.selectedCustomer
                    ? this.props.customers.selectedCustomer.assignments
                    : []
                }
              />
            </CardBody>
          </Card>

          <Card>
            <CardHeader>
              <CardTitle>Kontaktpersoner</CardTitle>
              <ActionButtons actions={contactActions} />
            </CardHeader>
            <CardBody>
              <ContactsTable
                contacts={
                  this.props.customers.selectedCustomer
                    ? this.props.customers.selectedCustomer.contacts
                    : []
                }
              />
            </CardBody>
          </Card>
        </Container>
        {this.state.showCustomerModal && (
          <CustomerForm
            show={this.state.showCustomerModal}
            parentCallback={this.toggleCustomerModal}
            customer={this.props.customers.selectedCustomer}
          />
        )}
        {this.state.showContactModal && (
          <ContactPersonForm
            show={this.state.showContactModal}
            parentCallback={this.toggleContactModal}
            customer={this.props.customers.selectedCustomer}
          />
        )}
        {this.state.showAssignmentModal && (
          <AssignmentsForm
            show={this.state.showAssignmentModal}
            toggle={this.toggleAssignmentModal}
            assignment={{
              customer: this.props.customers.selectedCustomer,
              recruiters: [this.props.user.profile]
            }}
          />
        )}
      </div>
    )
  }
}

export default withRouter(connect(state => state)(Customer))
