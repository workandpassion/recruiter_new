import React from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Card, CardBody, Container } from 'reactstrap'
import SubHeader from '../../layouts/Header/SubHeader'
import AdsTable from './components/AdsTable'
import AdsForm from './components/AdsForm'
import { getAllAds } from '../../store/actions/ads'
class Ads extends React.Component {
  constructor(props) {
    super(props)

    document.title = 'Alla annonser | WAP recruiter'

    this.state = {
      adsModal: false
    }
    this.addedAd = this.addedAd.bind(this)
    this.toggleAdModal = this.toggleAdModal.bind(this)
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getAllAds())
  }

  toggleAdModal() {
    this.setState({
      adsModal: !this.state.adsModal
    })
  }

  addedAd() {
    this.setState({
      adsModal: false
    })
  }

  render() {
    let actions = [
      // {
      //   title: 'Ny annons',
      //   icon: 'fa-plus-circle',
      //   fn: this.toggleAdModal
      // }
    ]

    return (
      <div>
        <SubHeader actions={actions} />
        <Container fluid className="contentContainer">
          <Card>
            <CardBody>
              <AdsTable
                ads={this.props.ads.allAds}
                loading={this.props.customers.fetchingCustomers}
              />
            </CardBody>
          </Card>
        </Container>
        <AdsForm show={this.state.adsModal} parentCallback={this.addedAd} />
      </div>
    )
  }
}

export default withRouter(connect(state => state)(Ads))
