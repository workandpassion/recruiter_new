import React from 'react'
import { connect } from 'react-redux'
import { createAd, updateAd } from '../../../store/actions/ads'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import {
  Row,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Label
} from 'reactstrap'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'
import Select from 'react-select'
import { apiClient } from '../../../store/axios.config'

class AdForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      modal: props.show,
      ad: this.props.ad
        ? this.props.ad
        : {
            name: ' '
          },
      countyValue: props.user.profile.offices[0].counties[0].id
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValidSubmit = this.handleValidSubmit.bind(this)
    this.toggle = this.toggle.bind(this)
    this.getCountyOptions = this.getCountyOptions.bind(this)
    this.handleCountyChange = this.handleCountyChange.bind(this)
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        ad: {
          ...this.state.ad,
          contacts: [],
          [name]: value
        }
      })
    }
  }

  handleCountyChange(value) {
    this.setState({
      countyValue: value
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.ad && prevProps.ad.name !== this.props.ad.name) {
      this.setState({
        ad: this.props.ad
      })
    }
  }

  toggle() {
    this.setState({ modal: !this.state.modal })
  }

  handleValidSubmit(event, values) {
    console.log(event)
    console.log(values)
    let { dispatch } = this.props

    if (this.props.ad) {
      dispatch(updateAd(this.state.ad)).then(() => {
        this.props.parentCallback()
      })
    } else {
      dispatch(createAd(this.state.ad)).then(() => {
        this.props.parentCallback()
      })
    }
  }

  getCountyOptions(input, callback) {
    apiClient.get('locations').then(result => {
      let mOptions = []
      result.data.map(county => {
        mOptions.push({
          value: county.id,
          label: county.name
        })
      })
      callback(null, {
        options: mOptions,
        complete: true
      })
    })
  }

  render() {
    return (
      <Modal
        isOpen={this.props.show}
        toggle={this.props.parentCallback}
        className="modal-lg"
      >
        {this.state.ad.name && (
          <AvForm onValidSubmit={this.handleValidSubmit} model={this.state.ad}>
            <ModalHeader>
              {this.props.ad ? 'Redigera annons' : 'Ny annons'}
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col xs={12} md={6}>
                  <AvGroup>
                    <AvField
                      name="title"
                      label="Titel"
                      required
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
                <Col xs={12} md={6}>
                  <AvGroup>
                    <AvField
                      name="job_title"
                      label="Jobbtitel"
                      required
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
                <Col xs={12} md={6}>
                  <AvGroup>
                    <AvField
                      name="employer_name"
                      label="Kundföretag"
                      required
                      onChange={this.handleInputChange}
                    />
                  </AvGroup>
                </Col>
                <Col xs={12} md={6}>
                  <AvGroup>
                    <Label>Län</Label>
                    <Select.Async
                      name="county"
                      loadOptions={this.getCountyOptions}
                      value={this.state.countyValue}
                      onChange={this.handleCountyChange}
                    />
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <LoadingButton
                color="primary"
                type="submit"
                loading={this.props.ads.creatingAd}
              >
                {this.props.ad ? 'Redigera' : 'Skapa annons'}
              </LoadingButton>{' '}
              <Button color="secondary" onClick={this.props.parentCallback}>
                Avbryt
              </Button>
            </ModalFooter>
          </AvForm>
        )}
      </Modal>
    )
  }
}

export default connect(state => state)(AdForm)
