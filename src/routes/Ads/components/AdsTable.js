import React from 'react'
import ReactTable from 'react-table'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  Collapse,
  Row,
  Col,
  Label,
  InputGroup,
  InputGroupButton,
  Button
} from 'reactstrap'
import {
  AvForm,
  AvField,
  AvInput,
  AvInputContainer
} from 'availity-reactstrap-validation'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'
import {
  createAdPartner,
  getAllAds,
  updatePartner,
  updateAd
} from '../../../store/actions/ads'
import { apiClient } from '../../../store/axios.config'
import _ from 'lodash'
import $ from 'jquery'

import axios from 'axios'

class AdsTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      expandedRows: {}
    }

    this.clickHandler = this.clickHandler.bind(this)
  }

  clickHandler(id) {
    this.props.history.push(`/ads/${id}`)
  }
  render() {
    return (
      <ReactTable
        expanded={this.state.expandedRows}
        data={this.props.ads.allAds}
        className="-highlight"
        filterable
        previousText="Föregående"
        nextText="Nästa"
        loadingText="Hämtar kunder..."
        noDataText="Inga kunder"
        pageText="Sida"
        ofText="av"
        rowsText="rader"
        loading={this.props.loading}
        getTdProps={(state, rowInfo, column, instance) => {
          return {
            onClick: (e, handleOriginal) => {
              console.log('A Td Element was clicked!')
              console.log('it produced this event:', e)
              console.log('It was in this column:', column)
              console.log('It was in this row:', rowInfo)
              console.log('It was in this table instance:', instance)
              if (column.expander || rowInfo) {
                let mIndex = Number(rowInfo.index)
                let mExpandedRows = Object.assign({}, this.state.expandedRows)
                if (mIndex in mExpandedRows) {
                  delete mExpandedRows[mIndex]
                } else {
                  mExpandedRows[mIndex] = true
                }

                this.setState({
                  expandedRows: mExpandedRows
                })
              }
            }
          }
        }}
        columns={[
          {
            Header: 'Titel',
            accessor: 'title'
          },
          {
            Header: 'Jobbtitel',
            accessor: 'job_title'
          },
          {
            Header: 'Företagsnamn',
            accessor: 'employer_name'
          }
        ]}
        SubComponent={row => {
          return (
            <AdsSubComponent
              ad={row.row._original}
              dispatch={this.props.dispatch}
            />
          )
        }}
      />
    )
  }
}

export default withRouter(connect(state => state)(AdsTable))

class AdsSubComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      collapse: false,
      newPartnerId: undefined,
      firstPartnerId:
        this.props.ad.ad_partners.length > 0 &&
        this.props.ad.ad_partners[0].external_id,
      loading: false
    }

    this.toggleCollapse = this.toggleCollapse.bind(this)
    this.addPartner = this.addPartner.bind(this)
    this.handleNewPartnerInput = this.handleNewPartnerInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  toggleCollapse() {
    this.setState({
      collapse: !this.state.collapse
    })
  }

  handleNewPartnerInput(e) {}

  handleSubmit(event, values) {
    let _self = this
    console.log(values)
    this.setState({
      loading: true
    })
    let { dispatch } = this.props
    let { ad } = this.props

    let adPartners = _.filter(values, function(value, key) {
      return key.startsWith('ip_ad_id_')
    })

    let mPartnerFunctions = []

    if (values.new_ip_ad_id) {
      let newPartner = {
        name: 'Intelliplan',
        partner_id: values.new_ip_ad_id
      }

      mPartnerFunctions.push(
        apiClient.post('recruiter/ads/' + ad.id + '/partners/', newPartner)
      )
    }

    adPartners.length > 0 &&
      adPartners.map((adPartner, index) => {
        if (ad.partners[index].partner_id !== adPartner && adPartner !== '') {
          mPartnerFunctions.push(
            apiClient.put(
              'recruiter/ads/' + ad.id + '/partners/' + ad.partners[index].id,
              {
                name: 'Intelliplan',
                partner_id: adPartner
              }
            )
          )
        }

        if (adPartner === '') {
          console.log('delete ad_partner')
          mPartnerFunctions.push(
            apiClient.delete(
              'recruiter/ads/' + ad.id + '/partners/' + ad.partners[index].id
            )
          )
        }
      })
    if (mPartnerFunctions.length > 0) {
      axios.all(mPartnerFunctions).then(() => {
        console.log('added all ad_partners')

        let jaid = values.ip_ad_id_0 ? values.ip_ad_id_0 : values.new_ip_ad_id
        getAdFromIp(jaid)
      })
    } else {
      getAdFromIp(values.ip_ad_id_0)
    }

    function getAdFromIp(jaid) {
      axios
        .get(
          'https://cv-maxkompetens.app.intelliplan.eu/JobAdGlobePages/Feed.aspx?pid=AA31EA47-FDA6-42F3-BD9F-E42186E5A960&version=2&JobAdId=' +
            jaid,
          {
            responseType: 'text'
          }
        )
        .then(result => {
          console.log(result)
          console.log($.parseXML(result.data))
          let xml = $.parseXML(result.data)
          let $xml = $(xml)
          console.log('Item:')
          console.log($xml.find('item'))

          if ($xml.find('item').length > 0) {
            apiClient.get('locations').then(result => {
              console.log(result.data)
              let mState = _.find(result.data, {
                name: $xml.find('intelliplan\\:state').text()
              })

              let mAd = {
                id: ad.id,
                title: $xml
                  .find('title')
                  .last()
                  .text(),
                pubdate: $xml.find('pubdate').text(),
                body: $xml.find('description').text(),
                job_type: $xml.find('intelliplan\\:type').text(),
                job_title: $xml.find('intelliplan\\:jobpositiontitle').text(),
                county: mState.id,
                county_name: $xml.find('intelliplan\\:state').text(),
                employer_name: $xml.find('intelliplan\\:company').text(),
                partners: []
              }

              dispatch(updateAd(mAd)).then(() => {
                dispatch(getAllAds()).then(() => {
                  _self.setState({
                    loading: false
                  })
                })
              })
            })
          } else {
            window.alert(
              'Hittade ingen annons med annons-id: ' + values.ip_ad_id_0
            )
            _self.setState({
              loading: false
            })
          }
        })
    }
  }

  addPartner(e) {
    console.log(e.target)
  }

  render() {
    let { ad } = this.props
    return (
      <AvForm onValidSubmit={this.handleSubmit}>
        <Row
          className="subComponent"
          style={{ padding: '20px', background: '#ffffff' }}
        >
          {ad.ad_partners.map((partner, index) => {
            return (
              <Col xs={12} md={2} key={partner.id}>
                <AvField
                  name={'ip_ad_id_' + index}
                  label={partner.name}
                  type="text"
                  pattern="^[0-9]*$"
                  maxLength="4"
                  minLength="4"
                  defaultValue={partner.external_id}
                />
              </Col>
            )
          })}
          <Col xs={12} md={2}>
            <Label>Nytt annons-ID</Label>
            <InputGroup>
              <AvInput
                name="new_ip_ad_id"
                type="text"
                pattern="^[0-9]*$"
                maxLength="4"
                minLength="4"
              />
              <InputGroupButton>
                <LoadingButton type="submit" loading={this.state.loading}>
                  Lägg till
                </LoadingButton>
              </InputGroupButton>
            </InputGroup>
          </Col>
          {ad.ad_partners.length > 0 && (
            <Col xs={12}>
              <LoadingButton loading={this.state.loading}>
                Uppdatera
              </LoadingButton>
            </Col>
          )}
          <Col xs={12}>
            <small onClick={this.toggleCollapse}>Visa annonstext</small>
          </Col>
          <Collapse isOpen={this.state.collapse}>
            <blockquote dangerouslySetInnerHTML={{ __html: ad.body }} />
          </Collapse>
        </Row>
      </AvForm>
    )
  }
}
