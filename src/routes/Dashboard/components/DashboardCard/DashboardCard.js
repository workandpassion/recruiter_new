import React from 'react'
import classnames from 'classnames'
import { DateRange } from 'react-date-range'
import defaultRanges from '../../../../components/defaultRanges/defaultRanges'
import moment from 'moment'

import { Col, Card, CardHeader, CardTitle, CardBody } from 'reactstrap'

class DashboardCard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      calVisible: false,
      minimized: false,
      expanded: false,
      showBack: true,
      startDate: moment().subtract(7, 'd'),
      endDate: moment()
    }

    this.handleSelect = this.handleSelect.bind(this)
    this.handleMinimize = this.handleMinimize.bind(this)
    this.handleExpand = this.handleExpand.bind(this)
    this.showHideDaterange = this.showHideDaterange.bind(this)
    this.createMarkup = this.createMarkup.bind(this)
  }

  componentDidUpdate(newProps, newState) {
    this.props.layout()
  }

  showHideDaterange() {
    this.setState({
      calVisible: !this.state.calVisible
    })
  }

  handleMinimize(e) {
    this.setState({
      minimized: !this.state.minimized
    })
  }

  handleExpand(e) {
    this.setState({
      expanded: !this.state.expanded
    })
  }

  handleSelect(range) {
    console.log(range)
    this.setState(
      {
        startDate: range.startDate,
        endDate: range.endDate
      },
      this.props.refreshFn(range.startDate, range.endDate)
    )
  }

  createMarkup(string) {
    return { __html: string }
  }

  render() {
    let calIconClasses = classnames(
      'fa fa-calendar',
      this.state.calVisible && 'orange'
    )

    return (
      <Col
        xs={12}
        sm={12}
        md={12}
        lg={6}
        className={classnames('grid-item', this.state.expanded && 'expanded')}
      >
        <Card>
          <CardHeader>
            {this.props.showBack && (
              <i
                className="back fa fa-arrow-circle-left"
                onClick={() => this.props.refreshFn()}
              />
            )}
            <CardTitle className="justify-content-between">
              {this.props.title}
              <div className="cardactions">
                {!this.state.minimized &&
                  this.props.showCal && (
                    <i
                      className={calIconClasses}
                      onClick={e => this.showHideDaterange(e)}
                    />
                  )}
                {!this.state.minimized &&
                  this.props.refreshFn && (
                    <i
                      className="fas fa-sync-alt"
                      onClick={() =>
                        this.props.refreshFn(
                          this.state.startDate,
                          this.state.endDate
                        )
                      }
                    />
                  )}
                <i
                  className={classnames(
                    'fa',
                    this.state.expanded ? 'fa-compress' : 'fa-expand'
                  )}
                  onClick={e => this.handleExpand(e)}
                />
                <i
                  className={classnames(
                    'fa',
                    this.state.minimized
                      ? 'fa-chevron-circle-down'
                      : 'fa-chevron-circle-up'
                  )}
                  onClick={e => this.handleMinimize(e)}
                />
              </div>
            </CardTitle>
          </CardHeader>
          {!this.state.minimized && (
            <CardBody>
              {this.props.showCal && (
                <div
                  className="daterange"
                  style={{ display: this.state.calVisible ? 'block' : 'none' }}
                >
                  <DateRange
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    onInit={this.handleSelect}
                    onChange={this.handleSelect}
                    linkedCalendars
                    ranges={defaultRanges}
                    firstDayOfWeek={1}
                    calendars={1}
                    theme={{
                      Calendar: { width: 200 },
                      PredefinedRanges: { width: 100 }
                    }}
                  />
                </div>
              )}
              {this.props.children}
            </CardBody>
          )}
        </Card>
      </Col>
    )
  }
}

export default DashboardCard
