import React from 'react'
import { connect } from 'react-redux'
import { Line, Bar } from 'react-chartjs-2'
import DashboardCard from './DashboardCard/DashboardCard'
import moment from 'moment'
import { apiClient } from '../../../store/axios.config'
import axios from 'axios'
import { getWapcardStats } from '../../../store/actions/stats'

class GraphWapcards extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      title: 'Wapcards',
      showBack: false,
      loadsave: true,
      data: [],
      labels: [],
      graphdata: {
        labels: [],
        datasets: []
      },
      totalWapcards: 0,
      plusThisTimespan: 0
    }

    this._setGraphData = this._setGraphData.bind(this)
    this._refresh = this._refresh.bind(this)
    this._checkIfBehandlad = this._checkIfBehandlad.bind(this)
  }

  _refresh(dateFrom, dateTo) {
    this._setGraphData(dateFrom, moment(dateTo).add(1, 'd'))
  }

  _checkIfBehandlad() {}

  _setGraphData(dateFrom, dateTo) {
    let startDate = moment(dateFrom).format('YYYY-MM-DD')
    let endDate = moment(dateTo).format('YYYY-MM-DD')
    let _self = this
    axios
      .all([
        apiClient.get('recruiter/profiles/count/?start_date=2017-01-01'),
        apiClient.get(
          'recruiter/profiles/count/?start_date=' +
            startDate +
            '&end_date=' +
            endDate
        )
      ])
      .then(
        axios.spread(function(totalWapcards, timespanWapcards) {
          console.log('spread')
          console.log(timespanWapcards)
          _self.setState({
            title:
              totalWapcards.data.count +
              ' wapcards (' +
              '+' +
              timespanWapcards.data.count +
              ')'
          })
        })
      )

    let { dispatch } = this.props
    this.setState({
      loadsave: true
    })
    dispatch(getWapcardStats(dateFrom, dateTo)).then(result => {
      console.log(result)
      let labels = []
      let wapcardsPerDayData = []
      let behandladeData = []
      let totalWapcardsData = []
      let totalBehandladeData = []

      result.stats.map(stat => {
        labels.push(stat.date)
        wapcardsPerDayData.push(stat.count_per_day)
        behandladeData.push(stat.behandlade_per_day)
        totalWapcardsData.push(stat.totalCount)
        totalBehandladeData.push(stat.totalBehandlade)
      })

      this.setState({
        graphdata: {
          labels: labels,
          datasets: [
            {
              type: 'line',
              label: 'Tot.wapcards',
              fill: false,
              lineTension: 0.1,
              backgroundColor: '#ff5dd7',
              borderColor: '#ff5dd7',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: '#ff5dd7',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: '#ff5dd7',
              pointHoverBorderColor: '#ff5dd7',
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: totalWapcardsData,
              borderWidth: 0,
              yAxisID: 'y-axis-2'
            },
            {
              type: 'line',
              label: 'Tot.behandlade',
              fill: false,
              lineTension: 0.1,
              backgroundColor: '#ff101b',
              borderColor: '#ff101b',
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: 'miter',
              pointBorderColor: '#ff101b',
              pointBackgroundColor: '#fff',
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: '#ff101b',
              pointHoverBorderColor: '#ff101b',
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: totalBehandladeData,
              borderWidth: 0,
              yAxisID: 'y-axis-2'
            },
            {
              stack: 'Stack 0',
              type: 'bar',
              label: 'behandlade/dag',
              backgroundColor: '#2d8454',
              data: behandladeData,
              yAxisID: 'y-axis-1'
            },
            {
              stack: 'Stack 0',
              type: 'bar',
              label: 'wapcards/dag',
              backgroundColor: '#eec03a',
              data: wapcardsPerDayData,
              yAxisID: 'y-axis-1'
            }
          ]
        },
        loadsave: false
      })
    })
  }

  render() {
    return (
      <DashboardCard
        title={this.state.title}
        refreshFn={this._refresh}
        showCal
        layout={this.props.layout}
      >
        <div className="chart-container" style={{ position: 'relative' }}>
          <Bar data={this.state.graphdata} options={options} />
        </div>
      </DashboardCard>
    )
  }
}

export default connect(state => state)(GraphWapcards)

const options = {
  responsiveAnimationDuration: 500,
  legend: {
    display: true
  },
  scales: {
    xAxes: [
      {
        stacked: true,
        ticks: {
          beginAtZero: false
        }
      }
    ],
    yAxes: [
      {
        stacked: false,
        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
        display: true,
        position: 'left',
        id: 'y-axis-1'
      },
      {
        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
        display: true,
        position: 'right',
        id: 'y-axis-2',
        // grid line settings
        gridLines: {
          drawOnChartArea: false // only want the grid lines for one axis to show up
        }
      }
    ]
  }
}
