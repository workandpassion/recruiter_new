import React from 'react'
import { connect } from 'react-redux'
import { Bar } from 'react-chartjs-2'
import DashboardCard from './DashboardCard/DashboardCard'
import { apiClient } from '../../../store/axios.config'
import { getAllLagerStats } from '../../../store/actions/stats'

class GraphLager extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      title: 'Lagerstatus',
      showBack: false,
      loadsave: true,
      data: [],
      labels: [],
      graphdata: {
        labels: [],
        datasets: []
      }
    }

    this._setGraphData = this._setGraphData.bind(this)
    this._showOccupations = this._showOccupations.bind(this)
    this._getParentIdFromLabel = this._getParentIdFromLabel.bind(this)
    this._refresh = this._refresh.bind(this)
  }

  componentDidMount() {
    this._setGraphData(lagerOccupations)
  }

  _setGraphData(occupations) {
    let { dispatch } = this.props
    this.setState({
      loadsave: true
    })
    dispatch(getAllLagerStats(occupations)).then(result => {
      console.log(result)
      let labels = []
      let totalData = []
      let weekData = []
      result.stats.map(stat => {
        labels.push(stat.name)
        totalData.push(stat.totalCount)
        weekData.push(stat.weekCount)
      })

      this.setState({
        graphdata: {
          labels: labels,
          datasets: [
            {
              label: 'Vecka',
              backgroundColor: '#eec03a',
              borderWidth: 0,
              data: weekData
            },
            {
              label: 'Totalt',
              backgroundColor: '#198f95',
              borderWidth: 0,
              data: totalData
            }
          ]
        },
        loadsave: false
      })
    })
  }

  _showOccupations(elems) {
    let thisLabel = elems[0]._model.label
    let thisId = this._getParentIdFromLabel(thisLabel)

    this.setState({
      title: 'Lagerstatus: ' + thisLabel,
      loadsave: true,
      showBack: true
    })

    apiClient.get('occupations/' + thisId).then(result => {
      this._setGraphData(result.data.occupations)
    })
  }

  _getParentIdFromLabel(label) {
    let index = lagerOccupations.findIndex(
      lagerOccupations => lagerOccupations.name === label
    )
    if (index > -1) {
      return lagerOccupations[index].id
    }
    return -1
  }

  _refresh() {
    this.setState({
      title: 'Lagerstatus',
      showBack: false
    })
    this._setGraphData(lagerOccupations)
  }

  render() {
    return (
      <DashboardCard
        title={this.state.title}
        refreshFn={this._refresh}
        showBack={this.state.showBack}
        layout={this.props.layout}
      >
        <div className="chart-container" style={{ position: 'relative' }}>
          <Bar
            data={this.state.graphdata}
            options={options}
            getElementAtEvent={elems => this._showOccupations(elems)}
          />
        </div>
      </DashboardCard>
    )
  }
}

let lagerOccupations = [
  {
    name: 'Ekonomi/Finans',
    id: '60d32b35-18f6-4a3b-a554-7fff44c3c2ea'
  },
  {
    name: 'Data/IT',
    id: '7e67733b-4ae8-4efc-8209-5a3f16152625'
  },
  {
    name: 'Marknad/Produkt',
    id: '6656568d-3457-467c-901a-31a162dd8dbf'
  },
  {
    name: 'Försäljning/Affärsutveckling',
    id: '425d5eeb-9fcc-4f75-9315-41f0a4fed0af'
  }
]

const options = {
  layout: {
    padding: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0
    }
  },
  maintainAspectRatio: true,
  responsiveAnimationDuration: 500,
  legend: {
    display: true
  },
  scales: {
    xAxes: [
      {
        stacked: true,
        ticks: {
          beginAtZero: false
        }
      }
    ],
    yAxes: [
      {
        stacked: false,
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
}

export default connect(state => state)(GraphLager)
