import React from 'react'
import Masonry from 'react-masonry-component'
import GraphWapcards from './components/GraphWapcards'
import GraphLager from './components/GraphLager'
import { connect } from 'react-redux'
import { Container } from 'reactstrap'
import SubHeader from '../../layouts/Header/SubHeader'

class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    document.title = 'Dashboard | wap recruiter'

    this.layout = this.layout.bind(this)
  }

  layout() {
    this.masonry.layout()
  }

  componentDidUpdate() {}

  render() {
    return (
      <div>
        <Container fluid className="contentContainer">
          <Masonry
            onClick={this.handleClick}
            className="row"
            ref={function(c) {
              this.masonry = this.masonry || c.masonry
            }.bind(this)}
          >
            <GraphWapcards layout={this.layout} />
            <GraphLager layout={this.layout} />
          </Masonry>
        </Container>
      </div>
    )
  }
}

export default connect(state => state)(Dashboard)
