import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Collapse,
  Button,
  Input
} from 'reactstrap'
import { AvForm, AvGroup, AvField } from 'availity-reactstrap-validation'
import SubHeader from '../../../layouts/Header/SubHeader'
import {
  createFolder,
  getFolders,
  getRecommendations
} from '../../../store/actions/candidates'
import moment from 'moment'
import classnames from 'classnames'

class MyCandidates extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      addFolder: false
    }

    props.dispatch(getRecommendations())
    props.dispatch(getFolders())

    this.toggleAddFolder = this.toggleAddFolder.bind(this)
    this.createNewFolder = this.createNewFolder.bind(this)
  }

  toggleAddFolder() {
    this.setState({
      addFolder: !this.state.addFolder
    })
  }

  createNewFolder(event, values) {
    let { dispatch } = this.props
    console.log(values)
    dispatch(createFolder(values)).then(() => {
      this.toggleAddFolder()
    })
  }

  render() {
    const { recommendations, folders } = this.props
    const { addFolder } = this.state

    return (
      <div>
        <SubHeader />
        <Container fluid className="my-5">
          <Row>
            <Col xs={12} md={5} lg={4}>
              <Card>
                <CardBody>
                  <CardTitle>Inkommande</CardTitle>
                  <Row>
                    {recommendations &&
                      recommendations.map(rec => (
                        <RecommendedCandidate
                          id={rec.id}
                          recommendation={rec}
                          folders={folders}
                        />
                      ))}
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12} md={7} lg={8} className="folder-tree">
              <Card>
                <CardBody>
                  <CardTitle>Mina sparade kandidater</CardTitle>
                  <Row>
                    {folders &&
                      folders.map(folder => <Folder folder={folder} />)}
                    <Col xs={12} className="new-folder mt-3">
                      <h5>
                        {addFolder ? (
                          <div className="d-flex flex-row">
                            <i className="folder mr-1 fas fa-folder float-left" />
                            <Col xs={12} md={6} lg={4} className="ml-0 pl-0">
                              <AvForm
                                onValidSubmit={this.createNewFolder}
                                style={{ marginTop: -5 }}
                              >
                                <AvField
                                  type="text"
                                  name="name"
                                  placeholder="Namn"
                                  required
                                />
                                <AvField
                                  type="textarea"
                                  name="description"
                                  placeholder="Beskrivning"
                                />
                                <Button
                                  color="primary"
                                  type="submit"
                                  className="ml-2"
                                >
                                  Skapa
                                </Button>
                                <Button
                                  color="secondary"
                                  className="ml-2"
                                  onClick={this.toggleAddFolder}
                                >
                                  Avbryt
                                </Button>
                              </AvForm>
                            </Col>
                          </div>
                        ) : (
                          <div>
                            <i className="folder mr-1 fas fa-folder" />
                            <span onClick={this.toggleAddFolder}>Ny mapp</span>
                          </div>
                        )}
                      </h5>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  recommendations: state.candidates.recommendations,
  folders: state.candidates.folders
})

export default withRouter(connect(mapStateToProps)(MyCandidates))

class Folder extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }

    this.openCloseFolder = this.openCloseFolder.bind(this)
  }

  openCloseFolder() {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const { folder } = this.props
    const { users } = folder
    const { open } = this.state

    return (
      <Col xs={12}>
        <h5 className="mb-0" onClick={this.openCloseFolder}>
          <i
            className={classnames(
              'folder mr-1',
              users.length === 0
                ? open ? 'far fa-folder-open' : 'far fa-folder'
                : open ? 'fas fa-folder-open' : 'fas fa-folder'
            )}
          />
          {folder.name}
        </h5>
        <Collapse isOpen={open} className="folder-candidates">
          <small className="folder-description">{folder.description}</small>
          <ul>
            {users &&
              users.map(user => <FolderUser key={user.user} user={user} />)}
          </ul>
        </Collapse>
      </Col>
    )
  }
}

class FolderUser extends React.Component {
  render() {
    const { user } = this.props
    return (
      <li className="folder-user">
        <h6>
          {user.first_name} {user.last_name}
        </h6>
      </li>
    )
  }
}

class RecommendedCandidate extends React.Component {
  constructor(props) {
    super(props)
  }

  handleSave(event, values) {
    console.log(values)
  }

  render() {
    const {
      user,
      from_recruiter,
      created_at,
      comment
    } = this.props.recommendation

    const { folders } = this.props

    return (
      <Col xs={12} className="recommended-candidate mb-2 pb-2">
        <Row>
          <Col xs={12}>
            <small>
              Från:{' '}
              <strong>
                {from_recruiter.first_name} {from_recruiter.last_name}
              </strong>{' '}
              den {moment(created_at).format('DD MMM HH:mm')}
            </small>
          </Col>
          <Col xs={12}>
            <span>
              {user.first_name} {user.last_name}
            </span>
          </Col>
          <Col xs={12}>
            <small>Kommentar</small>
            <div className="comment mb-2">{comment}</div>
          </Col>
          <Col xs={12}>
            <AvForm inline onValidSubmit={this.handleSave}>
              <AvField type="select" name="label">
                {folders.map(folder => (
                  <option key={folder.id} value={folder.id}>
                    {folder.name}
                  </option>
                ))}
              </AvField>
              <Button type="submit" className="ml-2">
                Spara
              </Button>
              <Button>Ta bort</Button>
            </AvForm>
          </Col>
        </Row>
      </Col>
    )
  }
}
