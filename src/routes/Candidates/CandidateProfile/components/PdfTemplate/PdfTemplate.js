import React from 'react'
import $ from 'jquery'
import bee_white from './bee_white.png'

import { Col, Row, Progress } from 'reactstrap'
import DotProgress from '../../../../../components/DotProgress/DotProgress'

const defaultPic = process.env.PUBLIC_URL + '/imgs/no_picture.jpg'

class PdfTemplate extends React.Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      imageIsSet: false,
      aligned: false,
      allDone: false,
      imageUrl: props.pdfOptions.hidePicture
        ? defaultPic
        : 'https://api.wapcard.se/api/v1/profiles/' +
          props.candidate.user +
          '/picture/500'
    }

    this.getAvailabilityString = this.getAvailabilityString.bind(this)
    this.onError = this.onError.bind(this)
    this.alignSections = this.alignSections.bind(this)
    this.convertImageToCanvas = this.convertImageToCanvas.bind(this)
  }

  componentDidMount() {
    this.convertImageToCanvas()
    this.alignSections($('.wapcardTemplateWrapper'))
  }

  getAvailabilityString() {
    switch (this.props.candidate.availability) {
      case 0:
        return 'Omgående'
      case 1:
        return '1 månad'
      case 2:
        return '2 månader'
      case 3:
        return '3 månader'
      case 4:
        return ' över 3 månader'
      default:
        return '-'
    }
  }

  _getStartEndDate(startDate, endDate, current) {
    let moment = require('moment')
    moment.locale('sv-SE')
    if (current) {
      return moment(startDate).format('MMM YYYY') + ' - Nuvarande anställning'
    } else {
      return (
        moment(startDate).format('MMM YYYY') +
        ' - ' +
        moment(endDate).format('MMM YYYY')
      )
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.imageUrl !== this.state.imageUrl) {
      // this.props.imageIsSet()
      this.convertImageToCanvas()
    }

    if (this.state.imageIsSet && this.state.aligned && !this.state.allDone) {
      this.setState({
        allDone: true
      })
    }

    if (this.state.allDone !== prevState.allDone) {
      this.props.templateDone()
    }
  }

  onError() {
    this.setState({
      imageUrl: defaultPic
    })
  }

  alignSections(elem) {
    let long = elem[0].scrollHeight - Math.ceil(elem.innerHeight())
    let children = elem.find('.main .wpSection').toArray()
    // if (lastChildren && lastChildren.length === children.length) {
    //   console.log('ERROR!!!!')
    //   return
    // }
    // lastChildren = children
    console.log(children)
    let removed = []
    while (long > 0 && children.length > 0) {
      let child = children.pop() // Remove last child
      $(child).detach() // ???
      removed.unshift(child)
      long = elem[0].scrollHeight - Math.ceil(elem.innerHeight())
    }

    elem.find('.main').append(removed[0])
    let removedOverflow = []
    // let overflowChildren = $(removed[0])
    //   .children()
    //   .toArray()

    let overflowChildren = $(removed[0])
      .find('.childItem')
      .toArray()
    long = elem[0].scrollHeight - Math.ceil(elem.innerHeight())

    while (long > 0 && overflowChildren.length > 0) {
      let child = overflowChildren.pop() // Remove last child
      console.log(child)
      $(child).detach() // ???
      removedOverflow.unshift(child)
      if (overflowChildren.length === 1) {
        let child = overflowChildren.pop() // Remove last child
        $(child).detach() // ???
        removedOverflow.unshift(child)
      }
      long = elem[0].scrollHeight - Math.ceil(elem.innerHeight())
    }

    if (removedOverflow.length > 0) {
      let tobeCloned = removed.shift()
      let clone = $(tobeCloned).clone()
      // let contTitle =
      //   '...fortsättning ' +
      //   $(clone[0])
      //     .find('.sectionTitle')
      //     .text()
      // let shouldBeContinue = $(clone[0])
      //   .find('.sectionTitle')
      //   .text()
      // console.log(shouldBeContinue)
      clone.empty()
      // shouldBeContinue &&
      //   clone.append(
      //     '<div class="col-12"><small class="fg-gray text-left"><em>' +
      //       contTitle +
      //       '</em></small></dla>'
      //   )
      clone.append(removedOverflow)
      removed.unshift(clone[0])
    }
    if (removed.length > 0) {
      let a4 = $(
        '<div class="wapcardTemplateWrapper row mx-0">' +
          '<div class="col-1 leftCol side">' +
          '<img src="/imgs/bee_white_all.png" class="img-fluid wapLogo page2" /></div><div class="col-11 main rightCol"></div></div>'
      )

      if (
        $(removed)
          .first()
          .hasClass('resume')
      ) {
        let desc = $('<div class="description" />')
        // $(removed)
        //   .first()
        //   .append('<div className="description" />')
        $(removed)
          .first()
          .find('.childItem')
          .appendTo(desc)

        $(removed)
          .first()
          .append(desc)

        console.log(removed)
        console.log(desc)
      }
      a4.find('.main').append(removed)
      elem.after(a4)
      this.alignSections(a4)
    } else {
      this.setState({ aligned: true })
    }
  }

  createMarkup(text) {
    // function str_replace_all(string, str_find, str_replace) {
    //   try {
    //     return string.replace(new RegExp(str_find, 'gi'), str_replace)
    //   } catch (ex) {
    //     return string
    //   }
    // }

    let mText = text.replace(/(\r\n|\r|\n){2,}/g, '$1\n')
    // mText = mText.replace(/(?:\r\n|\r|\n)/g, '<br />')

    // let to = mText
    // to = text.replace(/\n{2}/g, '</p><p>')
    // to = to.replace(/\n/g, '<br />')
    // to = '<p>' + to + '</p>'

    let to =
      '<p class="childItem col-12">' +
      mText
        .replace(/\n([ \t]*\n)+/g, '</p><p class="childItem col-12">')
        .replace('\n', '<br />') +
      '</p>'

    return { __html: to }
    // return { __html: text.replace(/(?:\r\n|\r|\n)/g, '<br />') }
  }

  convertImageToCanvas() {
    let _self = this
    let mCanvas = this.canvas
    mCanvas.width = 500
    mCanvas.height = 500
    let mContext = mCanvas.getContext('2d')
    let mImage = new Image()
    mImage.setAttribute('crossOrigin', '')
    mImage.src = this.state.imageUrl

    mImage.onload = function() {
      mContext.drawImage(mImage, 0, 0, 500, 500)
      // _self.setState({
      //   isSet: true
      // })
      _self.setState({
        imageIsSet: true
      })
    }
    mImage.onerror = function() {
      _self.onError()
    }
  }

  render() {
    let { candidate, pdfOptions } = this.props

    let userskillsList = Object.assign([], candidate.skills)
    userskillsList &&
      userskillsList.sort(function(a, b) {
        return a.experience < b.experience
      })
    userskillsList.length = 99

    let employments = Object.assign([], candidate.employments).reverse()
    let educations = Object.assign([], candidate.educations).reverse()

    return (
      <div className="parent">
        <Row className="wapcardTemplateWrapper mx-0">
          <Col xs={3} className="leftCol">
            <Row>
              <div>
                <canvas
                  style={{ width: '100%' }}
                  onError={this.onError}
                  id="mCanvas"
                  ref={canvas => {
                    this.canvas = canvas
                  }}
                />
              </div>
            </Row>

            {candidate.personalities &&
              candidate.personalities.length > 0 && (
                <Row className="wpSection mt-4">
                  <Col xs={12}>
                    <h4 className="sectionHeader">Personlighet</h4>
                    {candidate.personalities.map(personality => {
                      return (
                        <Row key={personality.id}>
                          <Col>{personality.name}</Col>
                        </Row>
                      )
                    })}
                  </Col>
                </Row>
              )}

            {candidate.motivations &&
              candidate.motivations.length > 0 && (
                <Row className="wpSection mt-4">
                  <Col xs={12}>
                    <h4 className="sectionHeader">Drivkrafter</h4>
                    {candidate.motivations.map(motivation => {
                      return (
                        <Row key={motivation.id}>
                          <Col>{motivation.name}</Col>
                        </Row>
                      )
                    })}
                  </Col>
                </Row>
              )}
            <Row className="wpSection mt-4">
              <Col xs={12}>
                <h4 className="sectionHeader">Språk</h4>
                {candidate.languages &&
                  candidate.languages.map(language => {
                    return (
                      <Row key={language.id}>
                        <Col xs={12}>{language.name}</Col>
                        <Col>
                          <Progress
                            color="warning"
                            min={0}
                            max={5}
                            value={(language.spoken + language.written) / 2}
                          />
                        </Col>
                      </Row>
                    )
                  })}
              </Col>
            </Row>
            <Row className="wpSection mt-4">
              <Col xs={12}>
                <h4 className="sectionHeader">Tillgänglighet</h4>
                {this.getAvailabilityString()}
              </Col>
            </Row>
            <Row>
              <Col
                xs={12}
                className="p-5"
                style={{ position: 'absolute', bottom: 0 }}
              >
                <img src={bee_white} className="img-fluid" alt="WAP Logo" />
              </Col>
            </Row>
          </Col>
          <Col xs={9} className="rightCol main">
            <div className="profileInfo" style={{ height: 204 }}>
              <Row className="pt-4">
                <Col xs={12}>
                  {!pdfOptions.hidePersonal && (
                    <h2 className="mb-0">
                      {candidate.first_name} {candidate.last_name}
                    </h2>
                  )}
                  {pdfOptions.hidePersonal && (
                    <div
                      style={{ height: 31, width: 300, background: '#e6e6e6' }}
                    />
                  )}
                  <h3>{candidate.title}</h3>
                </Col>
                <Col xs={12} className="mt-1 contactInfo">
                  <i className="fa fa-phone" />
                  {pdfOptions.hidePersonal ? (
                    <div
                      style={{ height: 16, width: 100, background: '#e6e6e6' }}
                    />
                  ) : (
                    <span>{candidate.mobile_phone_number}</span>
                  )}
                </Col>
                <Col xs={12} className="contactInfo">
                  <i className="fa fa-envelope" />
                  {pdfOptions.hidePersonal ? (
                    <div
                      style={{ height: 16, width: 150, background: '#e6e6e6' }}
                    />
                  ) : (
                    <span>{candidate.email}</span>
                  )}
                </Col>
                {candidate.home_page && (
                  <Col xs={12} className="contactInfo">
                    <i className="fa fa-globe" />
                    {this.props.hidePersonal ? (
                      <div
                        style={{
                          height: 16,
                          width: 130,
                          background: '#e6e6e6'
                        }}
                      />
                    ) : (
                      <span>{candidate.home_page}</span>
                    )}
                  </Col>
                )}
                <Col xs={12} className="contactInfo">
                  <i className="fa fa-map-marker" /> {candidate.city}
                </Col>
              </Row>
            </div>

            {this.props.pdfOptions.personal_info && (
              <Row className="wpSection mt-4 resume">
                <Col xs={12}>
                  <h4 className="sectionHeader">Resumé</h4>
                </Col>
                <div
                  className="description"
                  dangerouslySetInnerHTML={this.createMarkup(
                    this.props.pdfOptions.personal_info
                  )}
                />
              </Row>
            )}

            {employments &&
              employments.length > 0 && (
                <Row className="wpSection mt-4 employments">
                  <Col xs={12} className="childItem">
                    <h4 className="sectionHeader">Anställningar</h4>
                  </Col>

                  {employments.map(employment => {
                    return (
                      <Col
                        key={employment.id}
                        xs={12}
                        id={employment.title}
                        className="childItem"
                      >
                        <Row className="timeline-item">
                          <Col className="my-3">
                            <div className="mb-2 startEndDate">
                              <span>
                                {this._getStartEndDate(
                                  employment.start_date,
                                  employment.end_date,
                                  employment.current
                                )}
                              </span>
                            </div>
                            <h5>
                              {employment.title} | {employment.employer}
                            </h5>
                            <p style={{ fontSize: 12, lineHeight: 1.1 }}>
                              {employment.description}
                            </p>
                          </Col>
                        </Row>
                      </Col>
                    )
                  })}
                </Row>
              )}

            {userskillsList &&
              userskillsList.length > 0 && (
                <Row className="wpSection mt-4 skills">
                  <Col xs={12} className="childItem">
                    <h4 className="sectionHeader">Kompetenser</h4>
                  </Col>
                  {userskillsList.map((skill, index) => {
                    return (
                      <Col xs={6} key={skill.id} className="childItem">
                        <DotProgress
                          num={skill.experience}
                          title={skill.name}
                        />
                        {/* <CircularProgressbar percentage={60} /> */}
                      </Col>
                    )
                  })}
                </Row>
              )}

            {educations &&
              educations.length > 0 && (
                <Row className="wpSection mt-4 educations">
                  <Col xs={12} className="childItem">
                    <h4 className="sectionHeader">Utbildningar</h4>
                  </Col>
                  {educations.map(education => {
                    return (
                      <Col xs={12} className="childItem">
                        <Row key={education.id} className="timeline-item">
                          <Col className="my-3">
                            <div className="mb-2 startEndDate">
                              <span>
                                {this._getStartEndDate(
                                  education.start_date,
                                  education.end_date,
                                  false
                                )}
                              </span>
                            </div>
                            <h5>
                              {education.school} | {education.orientation}
                            </h5>
                            <p style={{ fontSize: 12, lineHeight: 1.1 }}>
                              {education.description}
                            </p>
                          </Col>
                        </Row>
                      </Col>
                    )
                  })}
                </Row>
              )}

            {candidate.driving_licenses &&
              candidate.driving_licenses.length > 0 && (
                <Row className="wpSection mt-4 licenses">
                  <Col xs={12} className="childItem">
                    <h4 className="sectionHeader">Körkort</h4>
                    <Row>
                      {candidate.driving_licenses.map((license, index) => {
                        return (
                          <Col xs={6} key={license.id} className="childItem">
                            {license.name}
                          </Col>
                        )
                      })}
                    </Row>
                  </Col>
                </Row>
              )}
          </Col>
        </Row>
      </div>
    )
  }
}

export default PdfTemplate
