import React from 'react'
import { connect } from 'react-redux'
import { setCandidateRating } from '../../../../../store/actions/candidates'
import classnames from 'classnames'

import {
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  FormGroup,
  Label,
  Input
} from 'reactstrap'

let mandrill = require('mandrill-api/mandrill')

class CandidateActions extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loadsave: false
    }

    this._handleStatusChange = this._handleStatusChange.bind(this)
    this._sendMailToCandidate = this._sendMailToCandidate.bind(this)
  }

  _handleStatusChange(e) {
    this.setState({ loadsave: true })
    console.log(e.target.value)
    let { dispatch, candidate } = this.props
    let mRating = {
      rating: e.target.value
    }
    let candidateHasRating =
      this.props.candidate.recruiter_info &&
      this.props.candidate.recruiter_info.rating > 0
    console.log(candidateHasRating)
    dispatch(setCandidateRating(mRating, candidate.user)).then(() => {
      this.setState({ loadsave: false })

      if (!candidateHasRating) {
        this._sendMailToCandidate()
      }
    })
  }

  _sendMailToCandidate() {
    let candidateName =
      this.props.candidate.first_name + ' ' + this.props.candidate.last_name
    let candidateEmail = this.props.candidate.email
    let templateContent = [
      {
        name: 'example name',
        content: 'example content'
      }
    ]
    let message = {
      template_name: 'Candidate behandlad',
      subject: 'Din profil har blivit behandlad',
      from_email: 'rekryterare@wapcard.se',
      from_name: 'Rekryterare Wapcard',
      to: [
        {
          email: candidateEmail,
          name: candidateName,
          type: 'to'
        }
      ],
      headers: {
        'Reply-To': 'noreply@wapcard.se'
      }
    }
    let async = false
    let mandrill_client = new mandrill.Mandrill('HqeQyBuKFGBqhUUyUTssWw')

    mandrill_client.messages.sendTemplate(
      {
        template_name: message.template_name,
        template_content: templateContent,
        message: message,
        async: async
      },
      function(result) {
        console.log(result)
      },
      function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message)
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
      }
    )
  }

  render() {
    let { candidate } = this.props
    // let { rating } = candidate.recruiter_info

    return (
      <Modal isOpen={this.props.show} toggle={this.props.toggle}>
        <ModalHeader toggle={this.props.toggle}>Ändra status</ModalHeader>
        {this.props.candidate && (
          <ModalBody>
            <FormGroup tag="fieldset" row>
              <Col sm={10} className="candidate-status">
                <FormGroup check>
                  <Label check>
                    <Input
                      hidden
                      type="radio"
                      name="radio2"
                      value="5"
                      onChange={e => this._handleStatusChange(e)}
                      defaultChecked={
                        candidate.recruiter_info &&
                        String(candidate.recruiter_info.rating) === '5'
                      }
                    />
                    <i
                      className={classnames(
                        'fa fa-star',
                        candidate.recruiter_info &&
                          String(candidate.recruiter_info.rating) === '5' &&
                          'active'
                      )}
                    />{' '}
                    MPC
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input
                      hidden
                      type="radio"
                      name="radio2"
                      value="4"
                      onChange={e => this._handleStatusChange(e)}
                      defaultChecked={
                        candidate.recruiter_info &&
                        String(candidate.recruiter_info.rating) === '4'
                      }
                    />{' '}
                    <i
                      className={classnames(
                        'fa fa-star-half-o',
                        candidate.recruiter_info &&
                          String(candidate.recruiter_info.rating) === '4' &&
                          'active'
                      )}
                    />{' '}
                    MPC - Svår
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input
                      hidden
                      type="radio"
                      name="radio2"
                      value="3"
                      onChange={e => this._handleStatusChange(e)}
                      defaultChecked={
                        candidate.recruiter_info &&
                        String(candidate.recruiter_info.rating) === '3'
                      }
                    />{' '}
                    <i
                      className={classnames(
                        'fa fa-arrow-up',
                        candidate.recruiter_info &&
                          String(candidate.recruiter_info.rating) === '3' &&
                          'active'
                      )}
                    />{' '}
                    Upcoming
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input
                      hidden
                      type="radio"
                      name="radio2"
                      value="2"
                      onChange={e => this._handleStatusChange(e)}
                      defaultChecked={
                        candidate.recruiter_info &&
                        String(candidate.recruiter_info.rating) === '2'
                      }
                    />{' '}
                    <i
                      className={classnames(
                        'fa fa-circle',
                        candidate.recruiter_info &&
                          String(candidate.recruiter_info.rating) === '2' &&
                          'active'
                      )}
                    />{' '}
                    Svår
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input
                      hidden
                      type="radio"
                      name="radio2"
                      value="1"
                      onChange={e => this._handleStatusChange(e)}
                      defaultChecked={
                        candidate.recruiter_info &&
                        String(candidate.recruiter_info.rating) === '1'
                      }
                    />{' '}
                    <i className="fa fa-circle-o" /> Ingen status (sätt
                    behandlad)
                  </Label>
                </FormGroup>
              </Col>
            </FormGroup>
          </ModalBody>
        )}
        <ModalFooter>
          <Button color="secondary" onClick={() => this.props.toggle()}>
            Stäng
          </Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default connect(state => state)(CandidateActions)
