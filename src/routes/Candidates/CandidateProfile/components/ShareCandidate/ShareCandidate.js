import React from 'react'
import { connect } from 'react-redux'
import { Modal, ModalHeader, ModalBody, Label } from 'reactstrap'
import {
  AvForm,
  AvField,
  AvInput,
  AvGroup
} from 'availity-reactstrap-validation'
import _ from 'lodash'
import { apiClient } from '../../../../../store/axios.config'
import LoadingButton from '../../../../../components/LoadingButton/LoadingButton'

class ShareCandidate extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loadsave: false,
      offices: [],
      officeRecruiters:
        props.user.profile.offices[0] &&
        props.user.profile.offices[0].recruiters.sort(
          (a, b) =>
            a.first_name !== b.first_name
              ? a.first_name < b.first_name ? -1 : 1
              : 0
        )
    }

    this.handleOfficeSelect = this.handleOfficeSelect.bind(this)
    this.handleValidSubmit = this.handleValidSubmit.bind(this)
  }

  componentDidMount() {
    const { dispatch } = this.props

    apiClient
      .get('recruiter/offices/', {
        params: {
          nested: 'true'
        }
      })
      .then(result => {
        this.setState({
          offices: result.data.sort(
            (a, b) => (a.name !== b.name ? (a.name < b.name ? -1 : 1) : 0)
          )
        })
      })
  }

  handleOfficeSelect(e) {
    const { offices } = this.state

    let selectedOffice = _.find(offices, { id: e.target.value })

    this.setState({
      officeRecruiters: selectedOffice.recruiters.sort(
        (a, b) =>
          a.first_name !== b.first_name
            ? a.first_name < b.first_name ? -1 : 1
            : 0
      )
    })
  }

  handleValidSubmit(event, values) {
    this.setState({
      loadsave: true
    })

    let data = {
      to_recruiter: values.to_recruiter,
      user: this.props.candidate.user,
      comment: values.comment
    }

    apiClient.post('recruiter/recommendations/', data).then(result => {
      this.setState({
        loadsave: false
      })

      this.props.toggle()
    })
  }

  render() {
    const { offices, officeRecruiters, loadsave } = this.state

    return (
      <Modal isOpen={this.props.show} toggle={this.props.toggle}>
        <ModalHeader toggle={this.props.toggle}>Skicka kandidat</ModalHeader>
        <ModalBody>
          <AvForm onValidSubmit={this.handleValidSubmit}>
            <AvField
              type="select"
              name="office"
              label="Kontor"
              onChange={this.handleOfficeSelect}
              defaultValue={
                this.props.user.profile.offices[0] &&
                this.props.user.profile.offices[0].id
              }
            >
              {offices.map(office => (
                <option key={office.id} value={office.id}>
                  {office.name}
                </option>
              ))}
            </AvField>
            <AvField type="select" name="to_recruiter" label="Rekryterare">
              {officeRecruiters.map(recruiter => (
                <option key={recruiter.id} value={recruiter.id}>
                  {recruiter.first_name} {recruiter.last_name}
                </option>
              ))}
            </AvField>
            <AvGroup>
              <Label>Kommentar</Label>
              <AvInput type="textarea" name="comment" label="Kommentar" />
            </AvGroup>
            <LoadingButton loading={loadsave} type="submit" className="mt-3">
              Skicka
            </LoadingButton>
          </AvForm>
        </ModalBody>
      </Modal>
    )
  }
}

export default connect(state => state)(ShareCandidate)
