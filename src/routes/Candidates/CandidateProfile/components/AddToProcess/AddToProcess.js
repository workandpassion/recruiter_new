import React from 'react'
import { connect } from 'react-redux'
import {
  getAssignments,
  createCandidateProcess,
  getProcessStateValues
} from '../../../../../store/actions/assignments'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import moment from 'moment'
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Label
} from 'reactstrap'
import Select from 'react-select'
import LoadingButton from '../../../../../components/LoadingButton/LoadingButton'

class AddToProcess extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      selectValue: null,
      stateSelectValue: undefined,
      fetchedAssignments: false,
      fetchedStateValues: false,
      process: {
        user: this.props.candidate && this.props.candidate.user,
        comment: '',
        states: []
      },
      state: {
        recruiter: this.props.user.profile.id,
        value: undefined,
        comment: ''
      },
      addingCandidate: false
    }

    this.getOptions = this.getOptions.bind(this)
    this.handleSelectChange = this.handleSelectChange.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.addCandidateToProcess = this.addCandidateToProcess.bind(this)
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getAssignments()).then(() => {
      this.setState({
        fetchedAssignments: true
      })
    })
    dispatch(getProcessStateValues()).then(result => {
      this.setState({
        fetchedStateValues: true,
        state: {
          ...this.state.state,
          value: this.props.assignments.processStateValues[0].id,
          start_date: moment().format('YYYY-MM-DD')
        }
      })
    })
  }

  getOptions() {
    let { myAssignments } = this.props.assignments
    let mOptions = []
    myAssignments &&
      typeof myAssignments.map === 'function' &&
      myAssignments.map(assignment => {
        mOptions.push({
          label: assignment.customer.name + ' ' + assignment.subject,
          value: assignment.id
        })
      })

    return mOptions
  }

  handleSelectChange(value) {
    this.setState({ selectValue: value })
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      let value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'states') {
        this.setState({
          state: {
            ...this.state.state,
            value: value
          }
        })
      } else {
        this.setState({
          process: {
            ...this.state.process,
            [name]: value
          }
        })
      }
    }
  }

  addCandidateToProcess() {
    let { dispatch } = this.props

    this.setState({
      addingCandidate: true
    })

    dispatch(
      createCandidateProcess(
        this.state.selectValue.value,
        this.state.process,
        this.state.state
      )
    ).then(result => {
      console.log(result)
      this.props.toggle()
    })
  }

  render() {
    return (
      <Modal isOpen={this.props.show} toggle={this.props.toggle}>
        <AvForm>
          <ModalHeader toggle={this.props.toggle}>
            Lägg till i uppdrag
          </ModalHeader>
          <ModalBody>
            {this.state.fetchedAssignments && (
              <AvGroup>
                <Label>Uppdrag</Label>
                <Select
                  value={this.state.selectValue}
                  placeholder="Uppdrag"
                  options={this.getOptions()}
                  onChange={this.handleSelectChange}
                />
                <AvField
                  type="hidden"
                  name="assignment"
                  value={this.state.selectValue && this.state.selectValue.value}
                  required
                />
              </AvGroup>
            )}
            {this.state.fetchedStateValues && (
              <AvGroup>
                <AvField
                  type="select"
                  name="states"
                  label="State"
                  onChange={this.handleInputChange}
                >
                  {this.props.assignments.processStateValues &&
                    this.props.assignments.processStateValues.map(option => {
                      return (
                        <option key={option.id} value={option.id}>
                          {option.name}
                        </option>
                      )
                    })}
                </AvField>
              </AvGroup>
            )}
            <AvGroup>
              <AvField
                name="comment"
                type="textarea"
                label="Kommentar"
                rows="4"
                onChange={this.handleInputChange}
              />
            </AvGroup>
          </ModalBody>
          <ModalFooter>
            <LoadingButton
              color="primary"
              onClick={this.addCandidateToProcess}
              disabled={!this.state.selectValue}
              loading={
                this.props.assignments.selectedAssignment &&
                this.props.assignments.selectedAssignment
                  .creatingAssignmentState
              }
            >
              Lägg till i uppdrag
            </LoadingButton>
            <Button color="secondary" onClick={this.props.toggle}>
              Stäng
            </Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    )
  }
}

export default connect(state => state)(AddToProcess)
