import React from 'react'
import ReactDOM from 'react-dom'
import { withRouter } from 'react-router-dom'
import $ from 'jquery'
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas'
import { getCandidate } from '../../../store/actions/candidates'
import { connect } from 'react-redux'
import { apiClient } from '../../../store/axios.config'
import PropTypes from 'prop-types'
import moment from 'moment'
import Masonry from 'react-masonry-component'
import Video from '../../../components/Video/Video'
import Loader from '../../../components/Loader/Loader'
import classnames from 'classnames'
import _ from 'lodash'
import axios from 'axios'

import CandidateActions from './components/CandidateActions/CandidateActions'

import PdfTemplate from './components/PdfTemplate/PdfTemplate'
import SubHeader from '../../../layouts/Header/SubHeader'

import {
  Row,
  Col,
  Progress,
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardHeader,
  Collapse,
  UncontrolledTooltip,
  Button,
  Input,
  FormGroup,
  Container,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody
} from 'reactstrap'

import AddToProcess from './components/AddToProcess/AddToProcess'
import RecruitementProcessStates from '../../../components/RecruitmentProcessStates/RecruitementProcessStates'
import ShareCandidate from './components/ShareCandidate/ShareCandidate'
import SaveCandidate from './components/SaveCandidate/SaveCandidate'

const defaultPic = process.env.PUBLIC_URL + '/imgs/no_picture.jpg'

class CandidateProfile extends React.Component {
  constructor(props, context) {
    super(props)

    let { dispatch } = this.props

    let userId = this.props.match.params.userid
      ? this.props.match.params.userid
      : this.props.userId

    this.state = {
      userId: userId,
      loadsave: true,
      imageUrl:
        'https://api.wapcard.se/api/v1/profiles/' + userId + '/picture/500',
      addressIsOpen: false,
      activeTab: '1',
      autoprint: false,
      kundprofil: false,
      preparePdf: false,
      noteCollapse: false,
      imageSet: false,
      pdfCollapse: false,
      pdfModal: false,
      pdfOptions: {
        compressPdf: true,
        hidePersonal: false,
        hidePicture: false,
        personal_info: props.candidates.selectedCandidate
          ? props.candidates.selectedCandidate.personal_info
          : undefined
      },
      statusModal: false,
      processModal: false,
      shareModal: false,
      saveModal: false
    }

    this.fetchCandidate()

    this.newNote = this.newNote.bind(this)
    this.onError = this.onError.bind(this)
    this.fetchCandidate = this.fetchCandidate.bind(this)
    this._calculateAge = this._calculateAge.bind(this)
    this._getGenderString = this._getGenderString.bind(this)
    this._getGenderIcon = this._getGenderIcon.bind(this)
    this._getAvailabilityString = this._getAvailabilityString.bind(this)
    this._renderMunicipalities = this._renderMunicipalities.bind(this)
    this._groupBy = this._groupBy.bind(this)
    this.videoIsDownloaded = this.videoIsDownloaded.bind(this)
    this.toggleTab = this.toggleTab.bind(this)
    this.createPdf = this.createPdf.bind(this)
    this.preparePdf = this.preparePdf.bind(this)
    this.layout = this.layout.bind(this)
    this.setImage = this.setImage.bind(this)
    this.togglePdfModal = this.togglePdfModal.bind(this)
    this.handlePdfOptions = this.handlePdfOptions.bind(this)
    this.toggleStatusModal = this.toggleStatusModal.bind(this)
    this.toggleProcessModal = this.toggleProcessModal.bind(this)
    this.downloadDocument = this.downloadDocument.bind(this)
    this.toggleShareModal = this.toggleShareModal.bind(this)
    this.toggleSaveModal = this.toggleSaveModal.bind(this)
  }

  toggleProcessModal(e) {
    let { dispatch } = this.props
    this.state.processModal && dispatch(getCandidate(this.state.userId))
    this.setState({
      processModal: !this.state.processModal
    })
  }

  toggleStatusModal() {
    this.setState({
      statusModal: !this.state.statusModal
    })
  }

  togglePdfModal() {
    this.setState({
      pdfModal: !this.state.pdfModal
    })
  }

  handlePdfOptions(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      this.setState({
        pdfOptions: {
          ...this.state.pdfOptions,
          [name]: value
        }
      })
    }
  }

  newNote() {
    let mNoteValue = ReactDOM.findDOMNode(this.newComment).value
    ReactDOM.findDOMNode(this.newComment).value = ''
    if (mNoteValue !== '') {
      apiClient
        .post('recruiter/profiles/' + this.state.userId + '/notes/', {
          note: mNoteValue
        })
        .then(result => {
          console.log(result)
          this.fetchCandidate()
        })
        .catch(function(error) {
          console.log(error)
        })
    }
  }

  fetchCandidate() {
    let { dispatch } = this.props
    Promise.all([dispatch(getCandidate(this.state.userId))]).then(() => {
      console.log('I did everything!')
      document.title =
        this.props.candidates.selectedCandidate.first_name +
        ' ' +
        this.props.candidates.selectedCandidate.last_name +
        ' | wap recruiter'
      this.setState({
        loadsave: false,
        pdfOptions: {
          ...this.state.pdfOptions,
          personal_info: this.props.candidates.selectedCandidate
            ? this.props.candidates.selectedCandidate.personal_info
            : undefined
        }
      })
    })
  }

  onError() {
    this.setState({
      imageUrl: defaultPic
    })
  }

  _calculateAge(birth) {
    // birthday is a date
    return moment().diff(birth, 'years')
  }
  _getGenderIcon(gendervalue) {
    switch (gendervalue) {
      case 'male':
        return <i className="fa fa-mars" />
      case 'female':
        return <i className="fa fa-venus" />
      case 'other':
        return <i className="fa fa-venus-mars" />
    }
  }

  _getGenderString(gendervalue) {
    switch (gendervalue) {
      case 'male':
        return <span>Man</span>
      case 'female':
        return <span>Kvinna</span>
      case 'other':
        return <span>Annat / Okänt</span>
    }
  }

  _getAvailabilityString(value) {
    switch (value) {
      case 0:
        return (
          <div>
            <i className="fas fa-calendar-alt fg-green" /> {'< 1'}
          </div>
        )
      case 1:
        return (
          <div>
            <i className="fas fa-calendar-alt fg-green" /> {value}
          </div>
        )
      case 2:
        return (
          <div>
            <i className="fas fa-calendar-alt fg-orange" /> {value}
          </div>
        )
      case 3:
        return (
          <div>
            <i className="fas fa-calendar-alt fg-red" /> {value}
          </div>
        )
      case 4:
        return (
          <div>
            <i className="fas fa-calendar-alt fg-red" /> > 3
          </div>
        )
    }
  }

  _groupBy(arr, property) {
    return arr.reduce(function(memo, x) {
      if (!memo[x[property]]) {
        memo[x[property]] = [x.parent_name]
      }
      memo[x[property]].push(x)
      return memo
    }, {})
  }

  _renderMunicipalities(userLocations) {
    let locList = $.extend(true, [], userLocations)
    let output = []
    let obj = this._groupBy(locList, 'parent_name')
    // console.log(Object.values(obj))
    Object.values(obj).map(location => {
      output.push(<Location key={location[0]} location={location} />)
    })

    // for (let i = 0; i < Object.keys(o).length; i++) {
    //   console.log(o[i])
    //   output.push(<Location key={o[i]} location={o[i]} />)
    // }
    if (output.length > 0) {
      return output
    } else {
      return (
        <Row>
          <Col>
            <h6>Kandidaten har inte angett några orter</h6>
          </Col>
        </Row>
      )
    }
  }

  videoIsDownloaded() {
    this.masonry1.layout()
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.imageSet !== prevState.imageSet &&
      this.state.imageSet &&
      this.state.preparePdf
    ) {
      this.createPdf()
    }
  }

  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  preparePdf() {
    this.setState({
      preparePdf: true,
      kundprofil: this.state.pdfOptions.hidePersonal
    })
  }

  createPdf() {
    this.setState({
      pdfModal: false
    })

    $('html, body').animate({ scrollTop: 0 }, 'fast')

    let _self = this
    let promises = []
    let images = new Array($('.wapcardTemplateWrapper').length)
    let { candidate } = this.props
    let $content = $('.wapcardTemplateWrapper'),
      $body = $('body'),
      a4 = [595.28, 841.89] // for a4 size paper width and height

    $body.css('overflow', 'visible')
    $content.appendTo('body')

    $content.css({ visibility: 'visible', 'max-width': 'none' })
    $body.scrollTop(0)

    function getCanvas() {
      // $content.width(a4[0] * 1.33333).css('max-width', 'none')
      // let doc = new jsPDF({
      //   unit: 'px',
      //   format: 'a4',
      // })

      let doc = new jsPDF({
        unit: 'pt',
        format: 'a4'
      })

      $.each($('.wapcardTemplateWrapper'), function(index, elem) {
        $body.scrollTop(0)
        console.log(index)
        let $this = $(this)
        $this.css('transform', 'scale(3, 3)')
        let promise = new Promise((resolve, reject) => {
          return html2canvas($(this), {
            imageTimeout: 6000,
            width: $this.width() * 3,
            height: $this.height() * 3,
            onrendered: function(canvas) {
              let img = canvas.toDataURL('image/png', 1.0)
              images[index] = img
              resolve('Valid')
              console.log('Valid')
            }
          }).catch(err => {
            reject(err)
          })
        })
        promises.push(promise)
      })

      Promise.all(promises).then(result => {
        let lastImage = undefined
        images.forEach((image, index) => {
          console.log(lastImage === image)
          if (index > 0) {
            doc.addPage()
            doc.setPage(index + 1)
          }

          doc.addImage(
            image,
            'PNG',
            0,
            0,
            a4[0],
            a4[1],
            index,
            _self.state.pdfOptions.compressPdf ? 'FAST' : 'NONE'
          )
        })

        if (_self.state.kundprofil) {
          doc.save(
            'kundprofil_' +
              _self.props.candidates.selectedCandidate.first_name.substr(0, 1) +
              '_' +
              _self.props.candidates.selectedCandidate.last_name.substr(0, 1) +
              '.pdf'
          )
        } else {
          doc.save(
            'profil_' +
              _self.props.candidates.selectedCandidate.first_name +
              '_' +
              _self.props.candidates.selectedCandidate.last_name +
              '.pdf'
          )
        }

        $body.css('overflow-x', 'hidden')
        $content.css('width', 'auto')
        $content.appendTo('.parent')
        _self.setState({
          autoprint: false,
          kundprofil: false,
          preparePdf: false,
          imageSet: false
        })
      })
    }

    getCanvas()
  }

  layout() {
    this.masonry1 && this.masonry1.layout()
    this.masonry2 && this.masonry2.layout()
  }

  setImage() {
    this.setState({
      imageSet: true
    })
  }

  downloadDocument(e, document) {
    e.preventDefault()

    let baseUrl = apiClient.defaults.baseURL

    axios({
      method: 'get',
      url: baseUrl + 'download/documents/' + document.id,
      contentType: 'application/pdf',
      responseType: 'blob',
      headers: { Authorization: 'Token ' + this.props.user.token }
    }).then(function(response) {
      let fileDownload = require('js-file-download')
      fileDownload(response.data, document.filename)
    })
  }

  getFileIcon(file) {
    switch (file.content_type) {
      case 'application/msword':
        return <i className="fa fa-file-word-o mr-1" />
      case 'application/pdf':
        return <i className="fa fa-file-pdf-o mr-1" />
    }
  }

  toggleShareModal() {
    this.setState({
      shareModal: !this.state.shareModal
    })
  }

  toggleSaveModal() {
    this.setState({
      saveModal: !this.state.saveModal
    })
  }

  render() {
    let candidate = this.props.candidates.selectedCandidate
    let mEmployments =
      candidate && Object.assign([], candidate.employments).reverse()
    let mEducations =
      candidate && Object.assign([], candidate.educations).reverse()

    let pictureClass = classnames(
      'img-fluid rounded-circle',
      candidate && !candidate.actively_searching && 'notActive'
    )

    let actions = [
      {
        title: 'Spara kandidat',
        icon: 'fa-save',
        fn: this.toggleSaveModal
      },
      {
        title: 'Skicka kandidat till rekryterare',
        icon: 'fa-share',
        fn: this.toggleShareModal
      },
      {
        title: 'Skapa PDF',
        icon: 'fa-file-pdf',
        fn: this.togglePdfModal
      },
      {
        title: 'Ändra status',
        icon: 'fa-star',
        fn: this.toggleStatusModal
      },
      {
        title: 'Lägg till i uppdrag',
        icon: 'fa-user-plus',
        fn: this.toggleProcessModal
      }
    ]

    return (
      <div>
        {this.state.preparePdf && (
          <PdfTemplate
            candidate={candidate}
            templateDone={this.setImage}
            pdfOptions={this.state.pdfOptions}
          />
        )}
        {!this.props.userId && <SubHeader actions={actions} />}
        <Container fluid className="contentContainer">
          {!this.state.loadsave && (
            <Masonry
              onClick={this.handleClick}
              className="row"
              ref={function(c) {
                this.masonry1 = this.masonry1 || c.masonry
              }.bind(this)}
            >
              <Col
                xs={12}
                sm={12}
                md={6}
                lg={6}
                xl={4}
                className="grid-item profile-side"
              >
                <Card>
                  <CardHeader className="text-center center-gradient">
                    <Row className="justify-content-center">
                      <Col xs={12} sm={10} className="py-3">
                        <img
                          src={this.state.imageUrl}
                          onError={this.onError}
                          id="profilePicture"
                          className={pictureClass}
                        />
                      </Col>
                      <Col xs={12}>
                        <h4 style={{ marginBottom: 0, marginTop: 10 }}>
                          {candidate.first_name} {candidate.last_name}
                        </h4>
                      </Col>
                      <Col xs={12}>
                        <h6
                          style={{ marginTop: 0, textTransform: 'uppercase' }}
                          className="accentColor"
                        >
                          {candidate.title}
                        </h6>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody>
                    <Row className="justify-content-center profile-details">
                      <Col className="text-center">
                        <UncontrolledTooltip placement="top" target="birthday">
                          {candidate.birthday}
                        </UncontrolledTooltip>
                        <CardSubtitle id="birthday">
                          {this._calculateAge(candidate.birthday)} år
                        </CardSubtitle>
                      </Col>
                      <Col className="text-center">
                        <CardSubtitle>
                          {this._getGenderIcon(candidate.gender)}
                        </CardSubtitle>
                      </Col>
                      <Col className="text-center">
                        <UncontrolledTooltip placement="top" target="student">
                          {candidate.student ? 'Student' : 'Ej student'}
                        </UncontrolledTooltip>
                        <CardSubtitle id="student">
                          {candidate.student ? (
                            <i className="fa fa-graduation-cap accentColor" />
                          ) : (
                            <i className="fa fa-graduation-cap disabled" />
                          )}
                        </CardSubtitle>
                      </Col>
                      <Col className="text-center">
                        <UncontrolledTooltip
                          placement="top"
                          target="actively_searching"
                        >
                          {candidate.actively_searching
                            ? 'Aktivt sökande'
                            : 'Ej aktivt sökande'}
                        </UncontrolledTooltip>
                        <CardSubtitle id="actively_searching">
                          {candidate.actively_searching ? (
                            <i className="fa fa-check accentColor" />
                          ) : (
                            <i className="fa fa-check disabled" />
                          )}
                        </CardSubtitle>
                      </Col>
                      <Col className="text-center">
                        <UncontrolledTooltip
                          placement="top"
                          target="availability"
                        >
                          {candidate.availability === 0 &&
                            'Tillgänglig snarast'}
                          {candidate.availability === 1 &&
                            'Tillgänglig om 1 månad'}
                          {candidate.availability === 2 &&
                            'Tillgänglig om 2 månader'}
                          {candidate.availability === 3 &&
                            'Tillgänglig om 3 månader'}
                          {candidate.availability === 4 &&
                            'Tillgänglig om mer än 3 månader'}
                        </UncontrolledTooltip>
                        <CardSubtitle id="availability">
                          {this._getAvailabilityString(candidate.availability)}
                        </CardSubtitle>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardBody style={{ paddingTop: 0 }}>
                    <Row>
                      <Col>
                        <CardSubtitle>
                          <i
                            className="fas fa-money-bill-alt fg-accentColor"
                            style={{ marginRight: 5 }}
                          />
                          {candidate.salary_expectations_min} -{' '}
                          {candidate.salary_expectations_max} kr
                        </CardSubtitle>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardBody>
                    <CardSubtitle>
                      <Row className="contact">
                        {candidate.mobile_phone_number && (
                          <Col xs={12}>
                            <i className="fa fa-phone phone" />
                            <a href={'tel:' + candidate.mobile_phone_number}>
                              {' '}
                              {candidate.mobile_phone_number}
                            </a>
                          </Col>
                        )}
                        {candidate.phone_number && (
                          <Col xs={12}>
                            <i className="fa fa-phone phone" />
                            <a href={'tel:' + candidate.phone_number}>
                              {' '}
                              {candidate.phone_number}
                            </a>
                          </Col>
                        )}
                        <Col xs={12}>
                          <i className="fa fa-envelope mail" />
                          <a href={'mailto:' + candidate.email}>
                            {' '}
                            {candidate.email}
                          </a>
                        </Col>
                        {candidate.linkedin_url && (
                          <Col xs={12}>
                            <i className="fa fa-linkedin linkedin" />
                            <a href={candidate.linkedin_url}>
                              {' '}
                              {candidate.linkedin_url}
                            </a>
                          </Col>
                        )}
                        {candidate.home_page && (
                          <Col xs={12}>
                            <i className="fa fa-globe" />{' '}
                            <a href={candidate.home_page}>
                              {candidate.home_page}
                            </a>
                          </Col>
                        )}
                        <Col xs={12}>
                          {candidate.care_of && (
                            <div>
                              C/o {candidate.care_of} <br />
                            </div>
                          )}
                          {candidate.address}
                          <br />
                          {candidate.zip_code} {candidate.city}
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} md={6}>
                          <small>
                            Skapad:{' '}
                            {moment(candidate.created_at).format(
                              'YYYY-MM-DD HH:mm'
                            )}
                          </small>
                        </Col>
                        <Col xs={12} md={6}>
                          <small>
                            Uppdaterad:{' '}
                            {moment(candidate.updated_at).format(
                              'YYYY-MM-DD HH:mm'
                            )}
                          </small>
                        </Col>
                      </Row>
                    </CardSubtitle>
                  </CardBody>
                </Card>
                <Card>
                  <CardBody>
                    <Row>
                      <Col>
                        <CardText>
                          <i
                            className="fa fa-quote-right"
                            style={{ marginRight: 5 }}
                          />
                          {candidate.personal_info}
                        </CardText>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={8} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-archive" /> Uppdrag
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.assignments &&
                      candidate.assignments.map(assignment => {
                        return (
                          <Assignment
                            key={assignment.id}
                            assignment={assignment}
                            userId={candidate.user}
                            layout={this.layout}
                            history={this.props.history}
                          />
                        )
                      })}
                    <Button
                      className="btn-info"
                      onClick={this.toggleProcessModal}
                    >
                      Lägg till i uppdrag
                    </Button>
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={8} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-comments" /> Kommentarer
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.recruiter_notes &&
                      typeof candidate.recruiter_notes.map === 'function' &&
                      candidate.recruiter_notes.map(note => {
                        return <RecruiterNote key={note.id} note={note} />
                      })}

                    <FormGroup style={{ marginTop: 20 }}>
                      <Input
                        type="textarea"
                        name="text"
                        rows="2"
                        id="exampleText"
                        ref={input => (this.newComment = input)}
                      />
                    </FormGroup>
                    <Button className="btn-info" onClick={e => this.newNote(e)}>
                      Lägg till
                    </Button>
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={8} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-file-alt" />Dokument
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Row>
                      {candidate.documents.length > 0 &&
                        candidate.documents.map(doc => (
                          <Col
                            xs={12}
                            md={6}
                            className="documents"
                            key={doc.id}
                          >
                            {this.getFileIcon(doc)}
                            <a
                              href={
                                apiClient.defaults.baseURL +
                                'download/documents/' +
                                doc.id
                              }
                              className="filename"
                              onClick={e => this.downloadDocument(e, doc)}
                            >
                              {doc.filename}
                            </a>
                          </Col>
                        ))}
                    </Row>
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-building" /> Anställningar
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {mEmployments && typeof mEmployments.map === 'function' ? (
                      mEmployments.map(employment => {
                        return (
                          <Employment
                            key={employment.id}
                            employment={employment}
                            layout={this.layout}
                          />
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inga anställningar</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-graduation-cap" /> Utbildningar
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {mEducations && typeof mEducations.map === 'function' ? (
                      mEducations.map(education => {
                        return (
                          <Education
                            key={education.id}
                            education={education}
                            layout={this.layout}
                          />
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inga utbildningar</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-rocket" /> Kompetenser
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.skills &&
                    typeof candidate.skills.map === 'function' ? (
                      candidate.skills.map(skill => {
                        return (
                          <Row style={{ marginBottom: 5 }} key={skill.id}>
                            <Col xs={12}>
                              <h6 style={{ marginBottom: 0 }}>{skill.name}</h6>
                              <Progress
                                color="warning"
                                value={skill.experience / 5 * 100}
                              />
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inga kompetenser</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-comments" /> Språk
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.languages &&
                    typeof candidate.languages.map === 'function' ? (
                      candidate.languages.map(language => {
                        return (
                          <Language language={language} key={language.id} />
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte angett språk</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-thermometer" /> Drivkrafter
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.motivations &&
                    typeof candidate.motivations.map === 'function' ? (
                      candidate.motivations.map(motivation => {
                        return (
                          <Row key={motivation.id}>
                            <Col xs={12}>
                              <h6 className="mItem">{motivation.name}</h6>
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte angett drivkrafter</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-user-circle" /> Personlighet
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.personalities &&
                    typeof candidate.personalities.map === 'function' ? (
                      candidate.personalities.map(personality => {
                        return (
                          <Row key={personality.id}>
                            <Col xs={12}>
                              <h6 className="mItem">{personality.name}</h6>
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte angett personlighet</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fas fa-chart-pie" /> TalentQ
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.assessment_result &&
                    typeof candidate.assessment_result.map === 'function' ? (
                      candidate.assessment_result.map((result, index) => {
                        return (
                          <Row style={{ marginBottom: 10 }} key={index}>
                            <Col xs={12}>
                              <h6 style={{ marginBottom: 0 }}>{result.name}</h6>
                              <Progress
                                color="info"
                                value={result.score / 5 * 100}
                              />
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col xs={12}>
                          <h6>Kandidaten har inte gjort testet</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-briefcase" /> Befattningar
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.occupations &&
                    candidate.occupations.length > 0 &&
                    typeof candidate.occupations.map === 'function' ? (
                      candidate.occupations.map((occupation, index) => {
                        return (
                          <Row key={occupation.id}>
                            <Col xs={12}>
                              <h6 className="mItem">
                                {index + 1}. {occupation.name}
                              </h6>
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte angett några befattningar</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-file" /> Sökta jobb
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.jobApplications &&
                    candidate.jobApplications.length > 0 &&
                    typeof candidate.jobApplications.map === 'function' ? (
                      candidate.jobApplications.map((application, index) => {
                        return (
                          <Row key={application.job_ad_id}>
                            <Col xs={12}>
                              <h6
                                className="mItem"
                                style={{ marginBottom: -5, fontWeight: 'bold' }}
                              >
                                {application.title
                                  ? application.title
                                  : 'Titel saknas'}
                              </h6>
                              <small>
                                {moment(application.created_at).format(
                                  'YYYY-MM-DD HH:MM'
                                )}
                              </small>
                              <small> | id: {application.job_ad_id}</small>
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte sökt några jobb</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fas fa-car" /> Körkort
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.driving_licenses &&
                    candidate.driving_licenses.length > 0 &&
                    typeof candidate.driving_licenses.map === 'function' ? (
                      candidate.driving_licenses.map((license, index) => {
                        return (
                          <Row key={license.id}>
                            <Col xs={12}>
                              <h6 className="mItem">{license.name}</h6>
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte angett några körkort</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-globe" /> Arbetsorter
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Row>{this._renderMunicipalities(candidate.locations)}</Row>
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fa fa-question-circle" /> Frågor
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.questions &&
                    candidate.questions.length > 0 &&
                    typeof candidate.questions.map === 'function' ? (
                      candidate.questions.map(question => {
                        return (
                          <Row style={{ marginBottom: 5 }} key={question.id}>
                            <Col xs={12}>
                              <h6 style={{ marginBottom: 0 }}>
                                <strong>{question.name}</strong>
                              </h6>
                              {question.answer}
                            </Col>
                          </Row>
                        )
                      })
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte svarat på frågorna</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col xs={12} sm={12} md={6} lg={6} xl={4} className="grid-item">
                <Card>
                  <CardHeader>
                    <CardTitle>
                      <i className="fas fa-video" /> Film
                    </CardTitle>
                  </CardHeader>
                  <CardBody>
                    {candidate.videos && candidate.videos[0] ? (
                      <Video
                        key={candidate.videos[0].id}
                        videoid={candidate.videos[0].id}
                        videoIsDownloaded={this.videoIsDownloaded}
                      />
                    ) : (
                      <Row>
                        <Col>
                          <h6>Kandidaten har inte laddat upp någon film</h6>
                        </Col>
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>
            </Masonry>
          )}
          <Modal
            isOpen={this.state.pdfModal}
            toggle={this.togglePdfModal}
            className="modal-lg"
          >
            <ModalHeader toggle={this.togglePdfModal}>Skapa PDF</ModalHeader>
            <ModalBody>
              <Row>
                <Col xs={12} lg={4}>
                  <div className="d-flex align-items-center">
                    <label className="switch">
                      <input
                        type="checkbox"
                        name="hidePersonal"
                        onChange={this.handlePdfOptions}
                      />
                      <span className="slider round" />
                    </label>
                    Dölj personlig information
                  </div>
                </Col>
                {this.state.imageUrl !== defaultPic && (
                  <Col xs={12} lg={4}>
                    <div className="d-flex align-items-center">
                      <label className="switch">
                        <input
                          type="checkbox"
                          name="hidePicture"
                          onChange={this.handlePdfOptions}
                        />
                        <span className="slider round" />
                      </label>
                      Dölj kandidatens bild
                    </div>
                  </Col>
                )}
                <Col xs={12} lg={4}>
                  <div className="d-flex align-items-center">
                    <label className="switch">
                      <input
                        type="checkbox"
                        name="compressPdf"
                        onChange={this.handlePdfOptions}
                        checked={this.state.pdfOptions.compressPdf}
                      />
                      <span className="slider round" />
                    </label>
                    Komprimera PDF
                  </div>
                </Col>
                <Col xs={12}>
                  <Input
                    type="textarea"
                    name="personal_info"
                    className="mt-3"
                    rows={8}
                    defaultValue={this.state.pdfOptions.personal_info}
                    onChange={this.handlePdfOptions}
                  />
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.preparePdf}>
                Skapa PDF
              </Button>{' '}
              <Button color="secondary" onClick={this.togglePdfModal}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
          <CandidateActions
            candidate={candidate}
            show={this.state.statusModal}
            toggle={this.toggleStatusModal}
          />
          {this.state.processModal && (
            <AddToProcess
              candidate={candidate}
              show={this.state.processModal}
              toggle={this.toggleProcessModal}
            />
          )}
          {this.state.shareModal && (
            <ShareCandidate
              show={this.state.shareModal}
              toggle={this.toggleShareModal}
              candidate={candidate}
            />
          )}
          {this.state.saveModal && (
            <SaveCandidate
              show={this.state.saveModal}
              toggle={this.toggleSaveModal}
              candidate={candidate}
            />
          )}
          <Loader active={this.state.preparePdf} />
        </Container>
      </div>
    )
  }
}

export default withRouter(connect(state => state)(CandidateProfile))

CandidateProfile.propTypes = {
  dispatch: PropTypes.func,
  candidate: PropTypes.object,
  params: PropTypes.object
}

const getStartEndDate = function(startDate, endDate, current) {
  let moment = require('moment')
  moment.locale('sv-SE')
  if (current) {
    return moment(startDate).format('MMM YYYY') + ' - Nuvarande anställning'
  } else {
    return (
      moment(startDate).format('MMM YYYY') +
      ' - ' +
      moment(endDate).format('MMM YYYY')
    )
  }
}

class Employment extends React.Component {
  static propTypes = {
    employment: PropTypes.object
  }

  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.state = {
      collapse: false,
      loading: true,
      occupation: null
    }

    this.onCollapse = this.onCollapse.bind(this)
  }

  componentDidUpdate(newProps, newState) {
    // this.props.layout()
  }

  toggle() {
    this.setState({
      collapse: !this.state.collapse
    })
  }

  onCollapse() {
    let _self = this
    setTimeout(function() {
      _self.props.layout()
    }, 250)
  }

  // _getOccupationName(occupationId) {
  //   apiClient.get('/occupations/').then(response => {
  //     response.data.map(parent => {
  //       parent.occupations.map(child => {
  //         if (child.id === occupationId) {
  //           this.setState({
  //             loading: false,
  //             occupation: child
  //           })
  //         }
  //       })
  //     })
  //   })
  // }

  componentDidMount() {
    // this._getOccupationName(this.props.employment.occupation)
  }

  render() {
    let { employment } = this.props
    let mClass = {
      className: employment.current
        ? 'employmentItem current'
        : 'employmentItem'
    }
    let chevronClass = classnames(
      'fa collapse-expander',
      this.state.collapse ? 'fa-chevron-down' : 'fa-chevron-left'
    )

    return (
      <Row {...mClass}>
        <Col onClick={this.toggle}>
          <h6 style={{ marginBottom: 0, width: '100%' }}>
            {employment.title} | {employment.employer}
          </h6>
          <span className="datetime aero">
            {getStartEndDate(
              employment.start_date,
              employment.end_date,
              employment.current
            )}
          </span>
          {employment.description &&
            employment.description.length > 0 && <i className={chevronClass} />}
          {employment.description &&
            employment.description.length > 0 && (
              <Collapse
                isOpen={this.state.collapse}
                onOpened={this.onCollapse()}
                onClosed={this.onCollapse()}
              >
                <div className="description">
                  <em>{employment.description}</em>
                </div>
              </Collapse>
            )}
        </Col>
      </Row>
    )
  }
}

class Education extends React.Component {
  static propTypes = {
    education: PropTypes.object
  }

  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.state = {
      collapse: false,
      loading: true
    }
  }

  toggle() {
    this.setState({
      collapse: !this.state.collapse
    })
  }

  onCollapse() {
    let _self = this
    setTimeout(function() {
      _self.props.layout()
    }, 250)
  }

  render() {
    let { education } = this.props
    let chevronClass = classnames(
      'fa collapse-expander',
      this.state.collapse ? 'fa-chevron-down' : 'fa-chevron-left'
    )
    return (
      <Row className="educationItem">
        <Col onClick={this.toggle}>
          <h6 style={{ marginBottom: 0, width: '100%' }}>
            {education.school} | {education.orientation}
          </h6>
          <span className="datetime aero">
            {getStartEndDate(education.start_date, education.end_date, false)} ({
              education.type
            })
          </span>
          {education.description &&
            education.description.length > 1 && <i className={chevronClass} />}
          {education.description &&
            education.description.length > 0 && (
              <Collapse
                isOpen={this.state.collapse}
                onOpened={this.onCollapse()}
                onClosed={this.onCollapse()}
              >
                <div className="description">
                  <em>{education.description}</em>
                </div>
              </Collapse>
            )}
        </Col>
      </Row>
    )
  }
}

class RecruiterNote extends React.Component {
  static propTypes = {
    note: PropTypes.object
  }

  constructor(props) {
    super(props)

    this.state = {
      recruiter: null,
      date: null,
      loading: true,
      imageUrl: defaultPic
    }

    this._fetchRecruiterInfo = this._fetchRecruiterInfo.bind(this)
    this.onError = this.onError.bind(this)
  }

  _fetchRecruiterInfo(recruiterId) {
    apiClient.get('/profiles/' + recruiterId).then(response => {
      console.log(response)
      this.setState({
        recruiter: response.data,
        loading: false,
        imageUrl:
          'https://api.wapcard.se/api/v1/profiles/' +
          recruiterId +
          '/picture/500'
      })
    })
  }

  componentDidMount() {
    let { note } = this.props
    this._fetchRecruiterInfo(note.recruiter)
  }

  onError() {
    this.setState({
      imageUrl: defaultPic
    })
  }

  createMarkup($string) {
    return { __html: $string }
  }

  render() {
    let { note } = this.props
    let { recruiter } = this.state

    return (
      <div key={note.id} className="recruiterNote">
        <Col xs={12}>
          <Row>
            <div className="profilePic">
              <img
                src={this.state.imageUrl}
                onError={this.onError}
                className="img-fluid rounded-circle"
              />
            </div>
            <div className="message">
              <div>
                {this.state.loading ? (
                  <h6 style={{ background: '#198f95', color: '#198f95' }}>
                    Lorem Ipsum
                  </h6>
                ) : (
                  <h6 className="recruiterName">
                    {recruiter.first_name} {recruiter.last_name}
                  </h6>
                )}

                {this.state.loading ? (
                  <p
                    style={{
                      width: '80%',
                      background: '#333333',
                      color: '#333333',
                      display: 'block'
                    }}
                  >
                    Lorem ipsum
                  </p>
                ) : (
                  <p
                    dangerouslySetInnerHTML={this.createMarkup(
                      note.note.replace(/(?:\r\n|\r|\n)/g, '<br />')
                    )}
                  />
                )}
                {this.state.loading ? (
                  <small
                    className="datetime"
                    style={{
                      width: '60%',
                      background: '#eeeeee',
                      color: '#eeeeee',
                      display: 'block'
                    }}
                  >
                    Lorem ipsum
                  </small>
                ) : (
                  <small className="datetime">
                    {moment(note.updated_at).format('YYYY-MM-DD HH:MM')}
                  </small>
                )}
              </div>
            </div>
          </Row>
        </Col>
      </div>
    )
  }
}

class Location extends React.Component {
  static propTypes = {
    location: PropTypes.array
  }

  constructor(props) {
    super(props)
    this._renderLocations = this._renderLocations.bind(this)
  }
  _renderLocations() {
    let { location } = this.props
    let locations = []
    for (let i = 1; i < location.length; i++) {
      locations.push(
        <Col xs={12} key={location[i].id}>
          {location[i].name}
        </Col>
      )
    }
    return locations
  }

  render() {
    let { location } = this.props

    return (
      <Col xs={12} sm={6} key={location}>
        <h6>
          <strong>{location[0]}</strong>
        </h6>
        <Row style={{ marginBottom: 10 }}>{this._renderLocations()}</Row>
      </Col>
    )
  }
}

class Language extends React.Component {
  static propTypes = {
    language: PropTypes.object
  }

  render() {
    let { language } = this.props
    return (
      <Row style={{ marginBottom: 5 }}>
        <Col xs={12}>
          <h6 style={{ marginBottom: 0 }}>{language.name}</h6>
          <Progress
            color="success"
            value={(language.spoken + language.written) / 10 * 100}
          />
        </Col>
      </Row>
    )
  }
}

class Assignment extends React.Component {
  static propTypes = {
    assignment: PropTypes.object,
    userId: PropTypes.number
  }

  constructor(props) {
    super(props)

    this.state = {
      collapse: false
    }

    this.toggle = this.toggle.bind(this)
    this.onCollapse = this.onCollapse.bind(this)
    this.gotoAssignment = this.gotoAssignment.bind(this)
  }

  toggle(e) {
    e.stopPropagation()
    this.setState({
      collapse: !this.state.collapse
    })
  }

  onCollapse() {
    let _self = this
    setTimeout(function() {
      _self.props.layout()
    }, 250)
  }

  gotoAssignment() {
    let { assignment } = this.props
    this.props.history.push(`/assignments/${assignment.id}`)
  }

  render() {
    let { assignment, userId } = this.props
    let mProcess = _.find(assignment.recruitment_processes, {
      user: { id: userId }
    })
    let chevronClass = classnames(
      'fa collapse-expander',
      this.state.collapse ? 'fa-chevron-down' : 'fa-chevron-left'
    )

    return (
      <Row className="assignmentItem">
        <Col xs={12}>
          <h6 onClick={this.toggle}>
            {assignment.subject} {assignment.customer.name}
            <i className={chevronClass} />
          </h6>

          <Collapse
            isOpen={this.state.collapse}
            onOpened={this.onCollapse()}
            onClosed={this.onCollapse()}
          >
            <RecruitementProcessStates
              noFunctionality
              states={mProcess.states}
            />
            <span style={{ cursor: 'pointer' }} onClick={this.gotoAssignment}>
              Gå till uppdrag
            </span>
          </Collapse>
        </Col>
      </Row>
    )
  }
}
