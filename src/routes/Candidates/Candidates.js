import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import $ from 'jquery'
import ReactTable from 'react-table'
import Select from 'react-select'
import PropTypes from 'prop-types'
import moment from 'moment'
import classnames from 'classnames'
import _ from 'lodash'
import { getAllOptions } from '../../store/actions/candidateOptions'
import {
  saveTableState,
  getCandidates,
  updateSearchTerms,
  setCandidate,
  getCandidate
} from '../../store/actions/candidates'

import {
  Row,
  Col,
  Form,
  FormGroup,
  Card,
  CardBody,
  Collapse,
  Button,
  Container
} from 'reactstrap'
import SubHeader from '../../layouts/Header/SubHeader'

let clickedId = -1

class Candidates extends React.Component {
  constructor(props) {
    super(props)

    document.title = 'Sök kandidater | wap recruiter'

    this.state = {
      advancedIsOpen: this.props.candidates
        ? this.props.candidates.advancedIsOpen
        : false,
      options: [],
      value:
        this.props.candidates.terms && this.props.candidates.terms.length > 0
          ? this.props.candidates.terms
          : [],
      advancedValue: this.props.candidates
        ? this.props.candidates.advanced
        : [],
      loadsave: false,
      sorted: this.props.candidates.tableState
        ? this.props.candidates.tableState.sorted
        : [],
      page: this.props.candidates.tableState
        ? this.props.candidates.tableState.page
        : 0,
      pageSize: this.props.candidates.tableState
        ? this.props.candidates.tableState.pageSize
        : 20,
      resized: this.props.candidates.tableState
        ? this.props.candidates.tableState.resized
        : [],
      filtered: this.props.candidates.tableState
        ? this.props.candidates.tableState.filtered
        : [],
      candidates:
        this.props.candidates &&
        Object.assign([], this.props.candidates.allCandidates)
    }

    this.dispatch = props.dispatch

    this.dispatch(getAllOptions())

    this.setClickedUserInRedux = this.setClickedUserInRedux.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this.handleSelectChange = this.handleSelectChange.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleAdvancedSelectChange = this.handleAdvancedSelectChange.bind(this)
    this.saveTableState = this.saveTableState.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this._hasNoRating = this._hasNoRating.bind(this)
    this._isMPC = this._isMPC.bind(this)
    this._isUpcoming = this._isUpcoming.bind(this)
    this._setStatusFilter = this._setStatusFilter.bind(this)
    this.renderCountiesShortcuts = this.renderCountiesShortcuts.bind(this)
    this.countyShortcutClick = this.countyShortcutClick.bind(this)
    this._gotoCandidate = this._gotoCandidate.bind(this)
  }

  componentWillMount() {
    this.setState({
      options: this.props.candidateOptions
        ? this.props.candidateOptions.options
        : []
    })
  }

  componentDidMount() {
    this.handleSearch()
  }

  componentDidUpdate(props, state) {
    if (
      state.value !== this.state.value ||
      state.advancedValue !== this.state.advancedValue
    ) {
      this.handleSearch()
    }
  }

  handleSelectChange(value) {
    this.setState({ value: value })
  }

  _gotoCandidate(userid) {
    let _self = this
    let { dispatch } = this.props
    dispatch(getCandidate(userid)).then(() => {
      let url = '/candidates/' + userid
      setTimeout(function() {
        _self.props.history.push(url)
      }, 250)
    })
  }

  handleAdvancedSelectChange(value) {
    let user = false
    value.map(val => {
      if (
        val.value.match(
          '^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$'
        )
      ) {
        user = true
        return this._gotoCandidate(val.value)
      }
    })

    if (!user) {
      this.setState({ advancedValue: value })
    }
  }

  handleSearch() {
    this.setState({ loadsave: true })
    let { dispatch } = this.props
    let shouldFetch = false
    if (this.state.value.length > 0) {
      shouldFetch = true
    }
    if (this.state.advancedValue && this.state.advancedValue.length > 0) {
      shouldFetch = true
    }
    if (shouldFetch) {
      dispatch(getCandidates(this.state.value, this.state.advancedValue)).then(
        result => {
          console.log(result)
          dispatch(
            updateSearchTerms(
              this.state.value,
              this.state.advancedValue,
              this.state.advancedIsOpen,
              this.state.onlyObehandlade
            )
          )
          this.setState({
            loadsave: false,
            candidates: result.data
          })
        }
      )
    } else {
      this.setState({
        loadsave: false,
        candidates: []
      })
    }
  }

  getOptions() {
    let optionsList = $.extend(true, [], this.props.candidateOptions.options)
    optionsList.sort(function(a, b) {
      return a.label > b.label ? 1 : -1
    })
    return optionsList
  }

  renderOption(option) {
    switch (option.type) {
      case 'skill':
        return (
          <span>
            <i className="fa fa-rocket fg-accentColor" /> {option.label}
          </span>
        )
      case 'language':
        return (
          <span>
            <i className="fa fa-comment fg-blue" /> {option.label}
          </span>
        )
      case 'occupation':
        return (
          <span>
            <i className="fa fa-briefcase fg-orange" /> {option.label}
          </span>
        )
      case 'occupation_parent':
        return (
          <span>
            <i className="fa fa-briefcase fg-orange" /> {option.label} *
          </span>
        )
      case 'drivingLicense':
        return (
          <span>
            <i className="fa fa-car fg-red" /> {option.label}
          </span>
        )
      case 'location':
        return (
          <span>
            <i className="fa fa-globe fg-purple" /> {option.label}
          </span>
        )
      case 'location_parent':
        return (
          <span>
            <i className="fa fa-globe fg-purple" /> {option.label} *
          </span>
        )
      default:
        return <span />
    }
  }

  renderValue(option) {
    switch (option.type) {
      case 'skill':
        return (
          <span>
            <i className="fa fa-rocket" /> {option.label}
          </span>
        )
      case 'language':
        return (
          <span>
            <i className="fa fa-comment" /> {option.label}
          </span>
        )
      case 'occupation':
        return (
          <span>
            <i className="fa fa-briefcase" /> {option.label}
          </span>
        )
      case 'occupation_parent':
        return (
          <span>
            <i className="fa fa-briefcase" /> {option.label}
          </span>
        )
      case 'drivingLicense':
        return (
          <span>
            <i className="fa fa-car" /> {option.label}
          </span>
        )
      case 'location':
        return (
          <span>
            <i className="fa fa-globe" /> {option.label}
          </span>
        )
      case 'location_parent':
        return (
          <span>
            <i className="fa fa-globe" /> {option.label}
          </span>
        )
    }
  }

  setClickedUserInRedux(user) {
    let { dispatch } = this.props
    clickedId = user.user
    dispatch(setCandidate(user))
  }

  componentWillReceiveProps(newProps, newState) {
    if (
      newProps.candidates.selectedCandidate &&
      newProps.candidates.selectedCandidate.user === clickedId
    ) {
      console.log('clicked')
      clickedId = -1
      this.saveTableState()
      this.props.history.push(
        `/candidates/${newProps.candidates.selectedCandidate.user}`
      )
    }
  }

  saveTableState() {
    let { dispatch } = this.props
    let stateToSave = {
      sorted: this.state.sorted,
      page: this.state.page,
      pageSize: this.state.pageSize,
      resized: this.state.resized,
      filtered: this.state.filtered
    }
    dispatch(saveTableState(stateToSave))
  }

  _toggleAdvanced() {
    this.setState({
      advancedIsOpen: !this.state.advancedIsOpen,
      advancedValue: this.state.advancedIsOpen && []
    })
  }

  _hasNoRating(item) {
    return !item.recruiter_info || !item.recruiter_info.rating
  }

  _isMPC(item) {
    return item.recruiter_info && item.recruiter_info.rating >= 4
  }

  _isUpcoming(item) {
    return item.recruiter_info && item.recruiter_info.rating === 3
  }

  _setStatusFilter(event, value) {
    let $mInput = $('.-filters')
      .find('.rt-th input')
      .first()
    let $this = $(event.currentTarget)

    $mInput.focus()
    if ($this.hasClass('active')) {
      $mInput.val('')
    } else {
      $mInput.val(value)
    }
    $mInput.blur()
  }

  countyShortcutClick(county) {
    let mValues = Object.assign([], this.state.value)
    if (_.find(mValues, { id: county.id })) {
      _.remove(mValues, function(v) {
        return v.id === county.id
      })
    } else {
      let mValue = _.find(this.props.candidateOptions.options, {
        id: county.id
      })
      // let valueToRemove = _.find(mValues, { type: 'location_parent' })
      _.remove(mValues, function(v) {
        return v.type === 'location_parent'
      })
      mValues.push(mValue)
    }

    this.setState({
      value: mValues
    })
  }

  renderCountiesShortcuts() {
    if (this.props.user.profile.offices[0].counties) {
      const { offices } = this.props.user.profile
      let mCounties = []

      offices.map(office => {
        office.counties.map(county => {
          mCounties.push(county)
        })
      })

      console.log(mCounties)

      return (
        <Row className="countyShortcuts mb-3">
          <Col xs={12}>
            {mCounties &&
              mCounties.map(county => {
                return (
                  <span
                    key={county.id}
                    className={classnames(
                      'countySpan',
                      _.find(this.state.value, { id: county.id }) && 'active'
                    )}
                    onClick={() => this.countyShortcutClick(county)}
                  >
                    <small>{county.name}</small>
                  </span>
                )
              })}
          </Col>
        </Row>
      )
    } else {
      return []
    }
  }

  render() {
    let { candidates } = this.state

    let obehandladeCount =
      candidates && candidates.filter(this._hasNoRating).length
    let mpcCount = candidates && candidates.filter(this._isMPC).length
    let upcomingCount = candidates && candidates.filter(this._isUpcoming).length

    // let fIndex = this.state.filtered.findIndex(
    //   filtered => filtered.id === 'recruiter_info.rating'
    // )

    let fIndex = _.findIndex(this.state.filtered, function(filtered) {
      return filtered.id === 'recruiter_info.rating'
    })

    return (
      <div>
        <SubHeader />
        <Container fluid className="contentContainer">
          <div id="stats-row" className="d-flex">
            <div
              className={classnames(
                'filter-item py-1 isMPC',
                fIndex > -1 &&
                  this.state.filtered[fIndex].value >= 4 &&
                  'active'
              )}
              onClick={e => this._setStatusFilter(e, 4)}
            >
              <h4>{mpcCount}</h4>
              MPC
              <i className="fas fa-filter filter-icon" />
            </div>

            <div
              className={classnames(
                'filter-item py-1 isUpcoming',
                fIndex > -1 &&
                  Number(this.state.filtered[fIndex].value) === 3 &&
                  'active'
              )}
              onClick={e => this._setStatusFilter(e, 3)}
            >
              <h4>{upcomingCount}</h4>
              upcoming
              <i className="fas fa-filter filter-icon" />
            </div>

            <div
              className={classnames(
                'filter-item py-1 hasNoRating',
                fIndex > -1 &&
                  Number(this.state.filtered[fIndex].value) === 1 &&
                  'active'
              )}
              onClick={e => this._setStatusFilter(e, 1)}
            >
              <h4>{obehandladeCount}</h4>
              obehandlade
              <i className="fas fa-filter filter-icon" />
            </div>

            {/*<div className='text-center add-new'>*/}
            {/*<h1><i className='fa fa-plus-circle' /></h1>*/}
            {/*</div>*/}
          </div>

          <Card>
            <CardBody className="card-body">
              {this.renderCountiesShortcuts()}
              <Row style={{ marginBottom: 20 }}>
                <Form inline>
                  <Col xs={12}>
                    <FormGroup style={{ width: '100%' }}>
                      <Select
                        multi
                        disabled={this.state.disabled}
                        value={this.state.value}
                        placeholder="Sökkriterier"
                        options={this.getOptions()}
                        onChange={this.handleSelectChange}
                        valueRenderer={this.renderValue}
                        optionRenderer={this.renderOption}
                      />
                      {/* optionRenderer={this.renderOption} */}
                      {/* valueComponent={ColoredValueBox} */}
                    </FormGroup>
                    <Collapse
                      isOpen={this.state.advancedIsOpen}
                      style={{ width: '100%', marginTop: 5 }}
                    >
                      <Row>
                        <Col xs={12} sm={12} xl={12}>
                          <FormGroup id="advancedSearch">
                            <Select.Creatable
                              multi
                              creatable
                              promptTextCreator={() => {
                                return 'Skapa sökord'
                              }}
                              options={[]}
                              value={this.state.advancedValue}
                              placeholder="Avancerad frisök"
                              onChange={this.handleAdvancedSelectChange}
                              arrowRenderer={() => {
                                return ''
                              }}
                            />
                          </FormGroup>
                        </Col>
                        <Col xs={3} sm={2} xl={1} className="d-none">
                          <Button
                            style={{ width: '100%' }}
                            onClick={() => this.handleSearch()}
                          >
                            <i className="fa fa-search" />
                          </Button>
                        </Col>
                      </Row>
                    </Collapse>
                    <small
                      className="blue"
                      style={{ cursor: 'pointer' }}
                      onClick={() => this._toggleAdvanced()}
                    >
                      {!this.state.advancedIsOpen
                        ? 'Visa avancerad frisök'
                        : 'Dölj frisök'}
                    </small>
                  </Col>
                </Form>
              </Row>
              <Row id="tableWrapper">
                <Col xs={12}>
                  <span>
                    Matchande kandidater:{' '}
                    {this.state.loadsave ? '-' : this.state.candidates.length}
                  </span>
                </Col>
                <Col xs={12}>
                  <ReactTable
                    className="-highlight"
                    data={this.state.candidates}
                    columns={[
                      {
                        Header: '',
                        accessor: 'recruiter_info.rating', // String-based value accessors!
                        maxWidth: 26,
                        filterable: true,
                        filterMethod: (filter, row) =>
                          filterStatus(filter, row),
                        Filter: ({ filter, onChange }) => (
                          <input
                            onChange={event => onChange(event.target.value)}
                            onBlur={event => onChange(event.target.value)}
                            style={{ width: '100%', opacity: 0 }}
                            value={filter ? filter.value : ''}
                          />
                        ),
                        Cell: row => {
                          switch (String(row.value)) {
                            case '5':
                              return (
                                <i className="status-fa fas fa-star fg-orange" />
                              )
                            case '4':
                              return (
                                <i className="status-fa fas fa-star-half fg-orange" />
                              )
                            case '3':
                              return (
                                <i className="status-fa fas fa-arrow-up fg-green" />
                              )
                            case '2':
                              return (
                                <i className="status-fa fas fa-circle fg-red" />
                              )
                            case '1':
                              return ''
                            default:
                              return (
                                <i className="status-fa fas fa-exclamation fg-red" />
                              )
                          }
                        }
                      },
                      {
                        Header: 'Förnamn',
                        accessor: 'first_name', // String-based value accessors!
                        filterMethod: (filter, row) =>
                          row[filter.id]
                            .toLowerCase()
                            .startsWith(filter.value.toLowerCase())
                      },
                      {
                        Header: 'Efternamn',
                        accessor: 'last_name',
                        filterMethod: (filter, row) =>
                          row[filter.id]
                            .toLowerCase()
                            .startsWith(filter.value.toLowerCase())
                      },
                      {
                        Header: 'Titel',
                        accessor: 'title',
                        filterMethod: (filter, row) =>
                          row[filter.id]
                            .toLowerCase()
                            .includes(filter.value.toLowerCase())
                      },
                      {
                        Header: 'Kön',
                        accessor: 'gender',
                        maxWidth: 100,
                        Cell: row => {
                          switch (row.value) {
                            case 'male':
                              return <span>Man</span>
                            case 'female':
                              return <span>Kvinna</span>
                            case 'other':
                              return <span>Annat / Okänt</span>
                          }
                        },
                        Filter: ({ filter, onChange }) => (
                          <select
                            onChange={event => onChange(event.target.value)}
                            style={{ width: '100%' }}
                            value={filter ? filter.value : ''}
                          >
                            <option value="" />
                            <option value="female">Kvinna</option>
                            <option value="male">Man</option>
                            <option value="other">Annat/Okänt</option>
                          </select>
                        )
                      },
                      {
                        Header: 'Stad',
                        accessor: 'city',
                        filterMethod: (filter, row) =>
                          row[filter.id]
                            .toLowerCase()
                            .startsWith(filter.value.toLowerCase())
                      },
                      {
                        id: 'birthday',
                        Header: 'Ålder',
                        accessor: c => _calculateAge(c.birthday),
                        maxWidth: 70,
                        filterMethod: (filter, row) => filterAge(filter, row)
                      },
                      {
                        Header: 'Profil',
                        accessor: 'progress',
                        filterable: false,
                        maxWidth: 100,
                        Cell: row => (
                          <div
                            style={{
                              width: '100%',
                              height: 13,
                              marginTop: 3,
                              backgroundColor: '#dadada',
                              borderRadius: '0px'
                            }}
                          >
                            <div
                              style={{
                                width: `${row.value}%`,
                                height: '100%',
                                backgroundColor: getColor(row.value),
                                borderRadius: '2px',
                                transition: 'all .2s ease-out'
                              }}
                            />
                          </div>
                        )
                      },
                      {
                        Header: '',
                        maxWidth: 120,
                        id: 'extras',
                        filterable: true,
                        Cell: row => {
                          return <span>{_renderIcons(row.row._original)}</span>
                        },
                        Filter: ({ filter, onChange }) =>
                          renderExtrasFilter(filter, onChange, this.state),
                        filterMethod: (filter, row) =>
                          filterExtras(filter, row, this.state)
                      }
                    ]}
                    filterable
                    // Texts
                    previousText="Föregående"
                    nextText="Nästa"
                    loadingText="Hämtar kandidater..."
                    noDataText="Inga matchningar"
                    pageText="Sida"
                    ofText="av"
                    rowsText="rader"
                    loading={this.state.loadsave}
                    onChange={this.tableChanged}
                    getTdProps={(state, rowInfo, column, instance) => {
                      return {
                        onClick: e => {
                          console.log('A Td Element was clicked!')
                          console.log('it produced this event:', e)
                          console.log('It was in this column:', column)
                          console.log('It was in this row:', rowInfo)
                          console.log(
                            'It was in this table instance:',
                            instance
                          )
                          if (rowInfo) {
                            this.setClickedUserInRedux(rowInfo.row._original)
                          }
                        }
                      }
                    }}
                    // Controlled Props
                    sorted={this.state.sorted}
                    page={this.state.page}
                    pageSize={this.state.pageSize}
                    resized={this.state.resized}
                    filtered={this.state.filtered}
                    // Callbacks
                    onSortedChange={sorted =>
                      this.setState({ sorted }, this.saveTableState)
                    }
                    onPageChange={page =>
                      this.setState({ page }, this.saveTableState)
                    }
                    onPageSizeChange={(pageSize, page) =>
                      this.setState({ page, pageSize }, this.saveTableState)
                    }
                    onResizedChange={resized =>
                      this.setState({ resized }, this.saveTableState)
                    }
                    onFilteredChange={filtered =>
                      this.setState({ filtered }, this.saveTableState)
                    }
                  />
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Container>
      </div>
    )
  }
}

export default withRouter(connect(state => state)(Candidates))

Candidates.propTypes = {
  search_terms: PropTypes.object,
  dispatch: PropTypes.func,
  candidateOptions: PropTypes.object,
  router: PropTypes.object,
  candidates: PropTypes.object
}

function filterStatus(filter, row) {
  if (Number(filter.value) >= 4) {
    return row[filter.id] >= 4
  }
  if (Number(filter.value) === 3) {
    return row[filter.id] === 3
  }
  if (Number(filter.value) === 1) {
    return row[filter.id] == null
  }

  // return row[filter.id] === (Number(filter.value))
}

function getColor(percentValue) {
  // value from 0 to 1
  let value = 1 - percentValue / 100
  let hue = ((1 - value) * 120).toString(10)
  return ['hsl(', hue, ',100%,50%)'].join('')
}

function renderExtrasFilter(filter, onChange, state) {
  let mExtras = _.find(state.filtered, { id: 'extras' })
    ? _.find(state.filtered, { id: 'extras' })
    : {}
  let mValues = mExtras.value ? mExtras.value : []

  function checkIfChecked(value) {
    return $.inArray(value, mValues) > -1
  }

  function change(value) {
    if ($.inArray(value, mValues) > -1) {
      console.log('removed' + value)
      _.remove(mValues, function(v) {
        return v === value
      })
    } else {
      console.log('added' + value)
      mValues.push(value)
    }
    onChange(mValues)
  }
  return (
    <fieldset id="extras">
      <label>
        <input
          type="checkbox"
          value="video"
          id="video"
          checked={checkIfChecked('video')}
          onChange={event => change(event.target.value)}
        />
        <i className="fas fa-video" />
      </label>
      <label>
        <input
          type="checkbox"
          value="test"
          id="test"
          checked={checkIfChecked('test')}
          onChange={event => change(event.target.value)}
        />
        <i className="fas fa-chart-pie" />
      </label>
      <label>
        <input
          type="checkbox"
          value="student"
          id="student"
          checked={checkIfChecked('student')}
          onChange={event => change(event.target.value)}
        />
        <i className="fa fa-graduation-cap" />
      </label>
      <label>
        <input
          type="checkbox"
          value="actively_searching"
          id="actively_searching"
          checked={checkIfChecked('actively_searching')}
          onChange={event => change(event.target.value)}
        />
        <i className="fas fa-check-circle" />
      </label>
      <label>
        <input
          type="checkbox"
          value="linkedin_url"
          id="linkedin_url"
          checked={checkIfChecked('linkedin_url')}
          onChange={event => change(event.target.value)}
        />
        <i className="fab fa-linkedin" />
      </label>
      <label>
        <input
          type="checkbox"
          value="documents"
          id="documents"
          checked={checkIfChecked('documents')}
          onChange={event => change(event.target.value)}
        />
        <i className="fas fa-file-alt" />
      </label>
    </fieldset>
  )
}
function filterExtras(filter, row, state) {
  let user = row._original
  let shouldShow = true

  if ($.inArray('video', filter.value) > -1) {
    if (user.videos.length === 0) {
      shouldShow = false
    }
  }
  if ($.inArray('test', filter.value) > -1 && shouldShow) {
    if (!user.assessment_result) {
      shouldShow = false
    }
  }
  if ($.inArray('student', filter.value) > -1 && shouldShow) {
    if (!user.student) {
      shouldShow = false
    }
  }
  if ($.inArray('actively_searching', filter.value) > -1 && shouldShow) {
    if (!user.actively_searching) {
      shouldShow = false
    }
  }
  if ($.inArray('linkedin_url', filter.value) > -1 && shouldShow) {
    if (!user.linkedin_url) {
      shouldShow = false
    }
  }
  if ($.inArray('documents', filter.value) > -1 && shouldShow) {
    if (!user.documents.length > 0) {
      shouldShow = false
    }
  }
  return shouldShow
}

function filterAge(filter, row) {
  // console.log(filter)
  // console.log(row)
  // console.log(filter.value.indexOf('-'))
  let age1 = 0
  let age2 = 0
  if (row[filter.id] !== null) {
    if (filter.value.indexOf('-') > -1) {
      age1 = filter.value.substr(0, filter.value.indexOf('-'))
      age2 = filter.value.substr(filter.value.indexOf('-') + 1)
      if (age1 === '') {
        age1 = 0
      }
      if (age2 === '') {
        age2 = 99
      }
      return row[filter.id] >= age1 && row[filter.id] <= age2
    }
    return row[filter.id] === parseInt(filter.value)
  }
}

function _renderIcons(data) {
  return (
    <div className="icons">
      {_renderVideoIcon(data.videos)}
      {_renderTestIcon(data.assessment_result)}
      {_renderStudentIcon(data.student)}
      {_renderActiveIcon(data.actively_searching)}
      {_renderLinkedinIcon(data.linkedin_url)}
      {_renderDocumentsIcon(data.documents)}
    </div>
  )
}

function _renderDocumentsIcon(documents) {
  return (
    <div className={classnames(documents.length === 0 && 'disabled')}>
      <i className="fas fa-file-alt" style={{ marginRight: 5 }} />
    </div>
  )
}

function _renderLinkedinIcon(linkedin_url) {
  return (
    <div
      className={classnames(!linkedin_url && 'disabled')}
      style={{ color: '#0077b5', marginRight: 5 }}
    >
      <i className="fab fa-linkedin" />
    </div>
  )
}

function _renderVideoIcon(videos) {
  return (
    <div className={classnames(!videos || (videos.length === 0 && 'disabled'))}>
      <i alt="wap film" className="fas fa-video" style={{ marginRight: 5 }} />
    </div>
  )
}

function _renderStudentIcon(student) {
  return (
    <div className={classnames(!student && 'disabled')}>
      <i className="fa fa-graduation-cap" style={{ marginRight: 5 }} />
    </div>
  )
}

function _renderActiveIcon(active) {
  return (
    <div className={classnames(!active && 'disabled')}>
      <i className="fas fa-check-circle" style={{ marginRight: 5 }} />
    </div>
  )
}

function _renderTestIcon(test) {
  return (
    <div className={classnames(!test || (test.length === 0 && 'disabled'))}>
      <i className="fas fa-chart-pie" style={{ marginRight: 5 }} />
    </div>
  )
}

function _calculateAge(birth) {
  // birthday is a date
  return moment().diff(birth, 'years')
}

// class ColoredValueBox extends React.Component {
//   render () {
//     let valueStyle = {
//       backgroundColor:
//           this.props.value.type === 'language' && 'rgba(0, 0, 255, 0.08)' ||
//           this.props.value.type === 'skill' && 'rgba(0, 126, 255, 0.08)' ||
//           this.props.value.type === 'location' && 'rgba(255, 0, 0, 0.08)',
//
//       border:
//           this.props.value.type === 'language' && '1px solid rgba(0, 0, 255, 0.24)' ||
//           this.props.value.type === 'skill' && '1px solid rgba(0, 126, 255, 0.24)' ||
//           this.props.value.type === 'location' && '1px solid rgba(255, 0, 0, 0.24)',
//
//       color: '#000000'
//     }
//
//     return (
//       <div className='Select-value' title={this.props.value.title} style={valueStyle}>
//         <span className='Select-value-icon' aria-hidden='true'>×</span>
//         <span className='Select-value-label' role='option' aria-selected='true'>
//           {this.props.children}
//           <span className='Select-aria-only'>&nbsp;</span>
//         </span>
//
//       </div>
//
//     )
//   }
// }
