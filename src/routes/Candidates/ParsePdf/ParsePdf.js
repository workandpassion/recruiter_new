import React from 'react'
import { PDFJS } from 'pdfjs-dist'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Input,
  FormGroup,
  Label
} from 'reactstrap'
import sha256, { Hash, HMAC } from 'fast-sha256'
import moment from 'moment'
import axios from 'axios'
import $ from 'jquery'

let CryptoJS = require('crypto-js')
const h = new HMAC('key')

class ParsePdf extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      pages: undefined
    }

    this.test = this.test.bind(this)
  }

  test() {
    let time = new Date().getTime()
    let username = 'mathias.hedstrom@it.test.maxkompetens_ats'
    let apiKey = '4166577870'
    let message = username + '|' + time + '|' + apiKey
    let secret =
      'e42789e59dc9ad8b36713d184ebbd3a702a5e1b836d6f5c728825b465630468f'

    let signature = CryptoJS.enc.Hex.stringify(
      CryptoJS.HmacSHA256(message, secret)
    )
    console.log(message)
    console.log(signature)

    let url = 'https://beanwidget.adcourier.com/distro/dashboardpost'
    const proxyurl = 'https://polar-plains-90955.herokuapp.com/'

    let data = {
      authentication: {
        api_key: apiKey,
        time: time,
        signature: signature
      },
      transaction: {
        filters: {
          custom_fields: [
            {
              name: 'MyJobId',
              value: '123456'
            }
          ]
        },
        notify_on_delivery: 'https://yoursite.com/jobs/1234/posting_complete',
        close_on_completion: 'true',
        vacancy_details:
          'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+CjxBZENvdXJpZXJBUEk+CiAgICA8TWV0aG9kPkV4cG9ydDwvTWV0aG9kPgogICAgPEFQSUtleT40MTY2NTc3ODcwPC9BUElLZXk+CiAgICA8QWNjb3VudD4KICAgICAgICA8VXNlck5hbWUvPgogICAgICAgIDxTaWduYXR1cmU+CiAgICAgICAgICAgIDxIYXNoLz4KICAgICAgICAgICAgPFRpbWUvPgogICAgICAgIDwvU2lnbmF0dXJlPgogICAgPC9BY2NvdW50PgogICAgPEFkdmVydD4KICAgICAgICA8Sm9iVGl0bGU+RGlzYnVyc2VtZW50cyBNYW5hZ2VyLCBFTUVBPC9Kb2JUaXRsZT4KICAgICAgICA8Sm9iUmVmZXJlbmNlPmFiYzEyMzwvSm9iUmVmZXJlbmNlPgogICAgICAgIDxKb2JUeXBlPlBlcm1hbmVudDwvSm9iVHlwZT4KICAgICAgICA8TG9jYXRpb25fUXVlcnk+TG9uZG9uLCBFbmdsYW5kPC9Mb2NhdGlvbl9RdWVyeT4KICAgICAgICA8SW5kdXN0cnk+SVQ8L0luZHVzdHJ5PgogICAgICAgIDxTYWxhcnlGcm9tPjIwPC9TYWxhcnlGcm9tPgogICAgICAgIDxTYWxhcnlUbz4zMDwvU2FsYXJ5VG8+CiAgICAgICAgPFNhbGFyeUN1cnJlbmN5PmdicDwvU2FsYXJ5Q3VycmVuY3k+CiAgICAgICAgPFNhbGFyeVBlcj5hbm51bTwvU2FsYXJ5UGVyPgogICAgICAgIDxTYWxhcnlCZW5lZml0cz5CZW5lZml0czwvU2FsYXJ5QmVuZWZpdHM+CiAgICAgICAgPFNraWxscz5Tb21lIFNraWxsczwvU2tpbGxzPgogICAgICAgIDxKb2JEZXNjcmlwdGlvbj5JIGFtIGxvb2tpbmcgZm9yIGEgaGlnaGx5IG1vdGl2YXRlZCBwcm9qZWN0IG1hbmFnZXIuLi48L0pvYkRlc2NyaXB0aW9uPgogICAgICAgIDxDdXN0b21GaWVsZCBuYW1lPSJNeUpvYklkIj4xMjM0NTY8L0N1c3RvbUZpZWxkPgogICAgPC9BZHZlcnQ+CjwvQWRDb3VyaWVyQVBJPg==',
        applyonline: {
          url: 'https://yoursite.com/jobs/1234/apply'
        },
        search: {
          candidate_import_url: 'https://your-candidate-import-url/'
        }
      },
      identification: {
        client_id: 'maxkompetens_ats',
        adc_username: 'mathias.hedstrom@it.test.maxkompetens_ats'
      }
    }

    axios.post(proxyurl + url, data).then(result => {
      console.log(result)
    })
  }

  componentDidMount() {
    PDFJS.workerSrc = '/scripts/pdf.worker.js'

    this.pdfToText('/imgs/cv.pdf').then(result => {
      this.setState({
        pages: result
      })
    })

    this.test()
  }

  pdfToText = function(data) {
    // PDFJS.workerSrc = process.env.PUBLIC_URL + '/scripts/pdf.worker.js'

    PDFJS.cMapUrl = 'js/vendor/pdfjs/cmaps/'
    PDFJS.cMapPacked = true

    return PDFJS.getDocument(data).then(function(pdf) {
      String.prototype.isEmpty = function() {
        return this.length === 0 || !this.trim()
      }

      let mString = ''
      let lastTransform = -1
      let lastItem = ''
      let mStringArray = []
      let pages = []
      for (let i = 0; i < pdf.numPages; i++) {
        pages.push(i)
      }
      return Promise.all(
        pages.map(function(pageNumber) {
          return pdf.getPage(pageNumber + 1).then(function(page) {
            return page.getTextContent().then(function(textContent) {
              //   return textContent.items
              //     .map(function(item) {
              //       if (item.transform[4] === lastTransform) {
              //         mString += item.str
              //       } else {
              //         mString = item.str + '<br />'
              //         return mString
              //       }
              //       lastTransform = item.transform[4]
              //     })
              //     .join(' ')

              let textItems = textContent.items
              let finalString = '<p>'

              // Concatenate the string of the item to the final string
              for (let i = 0; i < textItems.length; i++) {
                let item = textItems[i]
                // console.log(item.str.length)

                // if (item.transform[4] === lastTransform) {
                //   if (item.str.replace(/ /g, '')) {
                //     finalString += item.str + ' '
                //   }
                // } else {
                //   if (item.str.replace(/ /g, '')) {
                //     finalString += '</p>'
                //     finalString += '<p>' + item.str
                //   }
                // }

                if (item.str.isEmpty() && item.str.length > 1) {
                  finalString += item.str + '</p><p>'
                } else {
                  finalString += item.str + ' '
                }

                lastItem = item.str
                lastTransform = item.transform[4]
              }

              return finalString
            })
          })
        })
      ).then(function(pages) {
        return pages.join('\r\n')
        // return pages
      })
    })
  }

  render() {
    return (
      <Container fluid className="mt-5">
        {this.state.pages && (
          <Row>
            <Col xs={12} md={6}>
              <Card>
                <CardBody>
                  <FormGroup>
                    <Label for="exampleSelect">Skapa</Label>
                    <Input type="select" name="select" id="exampleSelect">
                      <option>Anställning</option>
                      <option>Utbildning</option>
                    </Input>
                  </FormGroup>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12} md={6}>
              <Card>
                <CardBody>
                  <span
                    contenteditable="true"
                    dangerouslySetInnerHTML={{ __html: this.state.pages }}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        )}
      </Container>
    )
  }
}

export default ParsePdf
