import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import Dashboard from '../routes/Dashboard/Dashboard'
import Candidates from '../routes/Candidates/Candidates'
import CandidateProfile from '../routes/Candidates/CandidateProfile/CandidateProfile'
import MyCandidates from '../routes/Candidates/MyCandidates/MyCandidates'
import AllAssignments from '../routes/Assignments/AllAssignments/AllAssignments'
import MyAssignments from '../routes/Assignments/MyAssignments/MyAssignments'
import AssignmentNew from '../routes/Assignments/Assignment/AssignmentNew'
import Login from '../routes/Login/Login'
import Customers from '../routes/Customers/Customers'
import Customer from '../routes/Customers/Customer'
import ParsePdf from '../routes/Candidates/ParsePdf/ParsePdf'

import { userIsNotAuthenticatedRedir } from '../store/auth'
import RecruiterProfile from './RecruiterProfile/RecruiterProfile'

const routes = (
  <Switch>
    <RouteWithLayout
      exact
      layout={CoreLayout}
      path="/"
      component={Dashboard}
      name="Dashboard"
    />

    {/*Candidates*/}
    <RouteWithLayout
      layout={CoreLayout}
      path="/candidates/search"
      component={Candidates}
      name="Sök kandidater"
    />
    <RouteWithLayout
      layout={CoreLayout}
      path="/candidates/parsepdf"
      component={ParsePdf}
    />
    <RouteWithLayout
      layout={CoreLayout}
      path="/candidates/my"
      component={MyCandidates}
    />
    <RouteWithLayout
      layout={CoreLayout}
      path="/candidates/:userid"
      component={CandidateProfile}
    />

    <Redirect from="/candidates" to="/candidates/search" />

    {/*Assignments*/}
    <RouteWithLayout
      layout={CoreLayout}
      path="/assignments/all"
      component={AllAssignments}
    />
    <RouteWithLayout
      layout={CoreLayout}
      path="/assignments/my"
      component={MyAssignments}
    />
    <RouteWithLayout
      layout={CoreLayout}
      path="/assignments/:assignmentId"
      component={AssignmentNew}
    />
    <Redirect from="/assignments" to="/assignments/my" />

    {/*Customers*/}
    <RouteWithLayout
      layout={CoreLayout}
      path="/customers"
      exact
      component={Customers}
    />
    <RouteWithLayout
      layout={CoreLayout}
      path="/customers/:customerId"
      component={Customer}
    />

    <RouteWithLayout
      layout={CoreLayout}
      path="/profile"
      component={RecruiterProfile}
    />

    {/*Ads*/}
    {/*<RouteWithLayout layout={CoreLayout} path="/ads" exact component={Ads} />*/}

    <Route path="/login" component={userIsNotAuthenticatedRedir(Login)} />
  </Switch>
)

export default routes

function RouteWithLayout({ layout, component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        React.createElement(
          layout,
          props,
          React.createElement(component, props)
        )
      }
    />
  )
}
