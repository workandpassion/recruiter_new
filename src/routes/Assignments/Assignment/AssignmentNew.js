import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import SubHeader from '../../../layouts/Header/SubHeader'
import AssingmentBoard from '../components/AssignmentBoard/AssignmentBoard'
import MatchingCandidatesTable from '../components/MatchingCandidatesTable'
import Activities from '../components/Activities/Activities'
import AssignmentsFormNew from '../components/AssignmentsFormNew'
import SendMailToCandidates from '../components/SendMailToCandidates/SendMailToCandidates'
import {
  createAssignmentState,
  updateAssignmentState,
  updateAssignmentStates,
  updateAssignment
} from '../../../store/actions/assignments'
import {
  TabsContainer,
  Tabs,
  Tab,
  Badge,
  FontIcon,
  MenuButton,
  Button,
  DialogContainer,
  TextField,
  ListItem
} from 'react-md'
import {
  getAssignment,
  getProcessStateValues,
  getAssignmentStateValues
} from '../../../store/actions/assignments'
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Container,
  Row,
  Col,
  Popover,
  PopoverBody,
  Card,
  CardBody,
  CardTitle
} from 'reactstrap'
import classnames from 'classnames'
import _ from 'lodash'
import moment from 'moment'
import Analytics from '../components/Analytics/Analytics'

class AssignmentNew extends React.Component {
  constructor(props) {
    super(props)

    let states = this.props.assignments.selectedAssignment
      ? this.props.assignments.selectedAssignment.states
      : null

    this.state = {
      activeTab: '1',
      assignmentModal: false,
      mailModal: false,
      doneFetching: false,
      popoverOpen: false,
      commentDialogVisible: false,
      mState:
        this.props.assignments.selectedAssignment &&
        this.props.assignments.selectedAssignment.states[states.length - 1],
      addingRemovingRecruiter: false,
      checkedCandidates: []
    }

    this.toggle = this.toggle.bind(this)
    this.toggleAssigmentModal = this.toggleAssigmentModal.bind(this)
    this.togglePopover = this.togglePopover.bind(this)
    this.toggleCommentDialog = this.toggleCommentDialog.bind(this)
    this.handleCommentInputChange = this.handleCommentInputChange.bind(this)
    this.addRemoveRecruiterToAssignment = this.addRemoveRecruiterToAssignment.bind(
      this
    )
    this.toggleMailModal = this.toggleMailModal.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !prevProps.assignments.selectedAssignment &&
      this.props.assignments.selectedAssignment &&
      !this.state.mState
    ) {
      let { states } = this.props.assignments.selectedAssignment

      this.setState({
        mState: states[states.length - 1]
      })
    }

    if (
      prevProps.assignments.fetchingAssignment &&
      !this.props.assignments.fetchingAssignment
    ) {
      let { states } = this.props.assignments.selectedAssignment
      this.setState({
        mState: states[states.length - 1]
      })
    }

    let mCheckedCandidates = []
    this.props.assignmentBoard.lists &&
      this.props.assignmentBoard.lists.map(list => {
        mCheckedCandidates = _.concat(
          mCheckedCandidates,
          _.filter(list.items, { checked: true })
        )
      })

    if (this.state.checkedCandidates.length !== mCheckedCandidates.length) {
      this.setState({
        checkedCandidates: mCheckedCandidates
      })
    }
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getProcessStateValues())
    dispatch(getAssignmentStateValues())
    dispatch(getAssignment(this.props.match.params.assignmentId, true)).then(
      () => {
        let assignment = this.props.assignments.selectedAssignment

        this.setState({
          doneFetching: true,
          hasAd: this.props.assignments.selectedAssignment.ad !== null
        })

        document.title =
          assignment.subject +
          ' ' +
          assignment.customer.name +
          ' | wap recruiter'
      }
    )
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  toggleAssigmentModal(e) {
    console.log(e)
    this.setState({
      assignmentModal: !this.state.assignmentModal,
      mEvent: e
    })
  }

  toggleMailModal(mailType) {
    this.setState({
      mailModal: !this.state.mailModal,
      mailType: mailType
    })
  }

  createNewAssignmentState(value) {
    let { dispatch, assignments } = this.props
    let { selectedAssignment } = assignments
    let assignmentId = selectedAssignment.id
    let { mState } = this.state
    let mAssignmentState = {
      date: moment(),
      value: value
    }

    let stateExists = _.find(selectedAssignment.states, function(state) {
      return state.value.id === value
    })

    if (stateExists) {
      dispatch(updateAssignmentStates(assignmentId, stateExists))
    } else {
      dispatch(createAssignmentState(assignmentId, mAssignmentState))
    }
  }

  togglePopover() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    })
  }

  toggleCommentDialog() {
    let { dispatch, assignments } = this.props
    let { selectedAssignment } = assignments
    let { mState, commentDialogVisible } = this.state

    this.setState({
      commentDialogVisible: !commentDialogVisible
    })

    if (
      this.state.mState.notes !==
        selectedAssignment.states[selectedAssignment.states.length - 1].notes &&
      commentDialogVisible
    ) {
      dispatch(
        updateAssignmentState(
          selectedAssignment.id,
          Object.assign({}, mState, {
            value: mState.value.id
          })
        )
      )
    }
  }

  handleCommentInputChange(value, event) {
    console.log(event)
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        mState: {
          ...this.state.mState,
          [name]: value
        }
      })
    }
  }

  getNewMatchesCount() {
    let { selectedAssignment } = this.props.assignments
    let matchedCandidates = selectedAssignment
      ? selectedAssignment.matchedCandidates
      : null
    if (selectedAssignment && matchedCandidates) {
      return _.filter(matchedCandidates, { new: true }).length
    }
  }

  addRemoveRecruiterToAssignment(recruiter) {
    this.setState({
      loadsave: true
    })
    let { dispatch } = this.props
    let assignment = this.props.assignments.selectedAssignment
    let mRecruiters = Object.assign({}, assignment.recruiters)

    if (!recruiter.id) {
      recruiter = this.props.user.profile
    }
    let recruiterIndex =
      assignment &&
      _.findIndex(assignment.recruiters, {
        id: recruiter.id
      })

    mRecruiters = _.map(mRecruiters, 'id')
    if (recruiterIndex > -1) {
      mRecruiters.splice(recruiterIndex, 1)
    } else {
      mRecruiters = _.concat(mRecruiters, recruiter.id)
    }

    let mAssignment = {
      ...assignment,
      contacts: _.map(assignment.contacts, 'id'),
      counties: _.map(assignment.counties, 'id'),
      occupations: _.map(assignment.occupations, 'id'),
      skills: _.map(assignment.skills, 'id'),
      recruiter_offices: _.map(assignment.recruiter_offices, 'id'),
      recruitment_processes: _.map(assignment.recruitment_processes, 'id'),
      recruiters: mRecruiters,
      states: _.map(assignment.states, 'id'),
      ad: assignment.ad && assignment.ad.id,
      customer: assignment.customer.id
    }

    dispatch(updateAssignment(mAssignment, false)).then(() => {
      this.setState({
        loadsave: false
      })
    })
  }

  render() {
    let {
      selectedAssignment,
      unnestedSelectedAssignment,
      processStateValues,
      assignmentStateValues,
      fetchingAssignment
    } = this.props.assignments
    let assignmentState =
      selectedAssignment &&
      selectedAssignment.states[selectedAssignment.states.length - 1]
    let { doneFetching, hasAd, checkedCandidates } = this.state
    let recruiterIndex =
      selectedAssignment &&
      _.findIndex(selectedAssignment.recruiters, {
        id: this.props.user.profile.id
      })
    let actions = [
      // {
      //   title: 'Skicka mail till kandidater',
      //   icon: 'fa-envelope',
      //   fn: this.toggleMailModal
      // },
      {
        title: 'Redigera uppdrag',
        icon: 'fa-edit',
        fn: this.toggleAssigmentModal
      },
      {
        title:
          recruiterIndex > -1
            ? 'Ta bort mig från uppdrag'
            : 'Lägg till mig i uppdrag',
        icon: recruiterIndex > -1 ? 'fa-user-times' : 'fa-user-plus',
        fn:
          selectedAssignment && selectedAssignment.recruiters.length > 1
            ? this.addRemoveRecruiterToAssignment
            : recruiterIndex === -1
              ? this.addRemoveRecruiterToAssignment
              : () => alert('Det måste finnas en ansvarig för ett uppdrag.'),
        loading: this.state.addingRemovingRecruiter
      }
    ]

    let assignmentStateMenuItems = []

    assignmentStateValues &&
      assignmentStateValues.map(value => {
        assignmentStateMenuItems.push(
          <ListItem
            key={value.id}
            primaryText={value.name}
            rightIcon={
              <FontIcon style={{ color: value.color }}>{value.icon}</FontIcon>
            }
            onClick={() => this.createNewAssignmentState(value.id)}
          />
        )
      })

    let mailMenuItems = [
      <ListItem
        key="failed"
        primaryText="Gick ej vidare"
        onClick={() => this.toggleMailModal('failed')}
      />,
      <ListItem
        key="delayed"
        primaryText="Dragit ut på tiden"
        onClick={() => this.toggleMailModal('delayed')}
      />,
      <ListItem
        key="cancelled"
        primaryText="Process avbruten"
        onClick={() => this.toggleMailModal('cancelled')}
      />
    ]

    return (
      <div>
        {selectedAssignment && (
          <SubHeader actions={actions}>
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames(
                    { active: this.state.activeTab === '1' },
                    'text-uppercase'
                  )}
                  onClick={() => {
                    this.toggle('1')
                  }}
                >
                  Uppdrag
                </NavLink>
              </NavItem>
              <Badge
                badgeContent={this.getNewMatchesCount()}
                primary
                invisibleOnZero
                badgeId="notifications-1"
                onClick={this.refreshMatches}
              >
                <NavItem>
                  <NavLink
                    className={classnames(
                      { active: this.state.activeTab === '2' },
                      'text-uppercase'
                    )}
                    onClick={() => {
                      this.toggle('2')
                    }}
                  >
                    Matchande kandidater
                  </NavLink>
                </NavItem>
              </Badge>

              <NavItem>
                <NavLink
                  className={classnames(
                    { active: this.state.activeTab === '3' },
                    'text-uppercase'
                  )}
                  onClick={() => {
                    this.toggle('3')
                  }}
                >
                  Aktivitet
                </NavLink>
              </NavItem>
              {hasAd && (
                <NavItem>
                  <NavLink
                    className={classnames(
                      { active: this.state.activeTab === '4' },
                      'text-uppercase'
                    )}
                    onClick={() => {
                      this.toggle('4')
                    }}
                  >
                    Analytics
                  </NavLink>
                </NavItem>
              )}
            </Nav>
          </SubHeader>
        )}
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Container fluid className="mt-5">
              {selectedAssignment && (
                <Row>
                  <Col xs={12} className="d-flex align-items-center">
                    <MenuButton
                      id="process-state-button"
                      style={{
                        background: assignmentState
                          ? assignmentState.value.color
                          : '#ababab'
                      }}
                      raised
                      primary
                      menuItems={assignmentStateMenuItems}
                      iconChildren={
                        assignmentState && assignmentState.value.icon
                      }
                    >
                      {assignmentState
                        ? assignmentState.value.name
                        : 'Ej startad'}
                    </MenuButton>
                    {assignmentState && (
                      <Button
                        id={'Popover-' + assignmentState.id}
                        icon
                        primary={assignmentState.notes !== ''}
                        className="ml-3"
                        onMouseOver={this.togglePopover}
                        onMouseOut={this.togglePopover}
                        swapTheming
                        onClick={this.toggleCommentDialog}
                      >
                        comments
                      </Button>
                    )}
                    {assignmentState &&
                      assignmentState.notes && (
                        <Popover
                          placement="bottom"
                          isOpen={this.state.popoverOpen}
                          target={'Popover-' + assignmentState.id}
                          toggle={this.togglePopover}
                        >
                          <PopoverBody>{assignmentState.notes}</PopoverBody>
                        </Popover>
                      )}
                    {checkedCandidates.length > 0 && (
                      <MenuButton
                        className="ml-5"
                        id="send-mail-button"
                        raised
                        secondary
                        iconChildren="mail"
                        menuItems={mailMenuItems}
                      >
                        Maila kandidater
                      </MenuButton>
                    )}
                  </Col>
                </Row>
              )}
            </Container>
            {selectedAssignment &&
              processStateValues &&
              doneFetching && <AssingmentBoard />}

            {selectedAssignment && (
              <Container fluid>
                <Row>
                  <Col xs={12} md={4} lg={3}>
                    <Card>
                      <CardBody>
                        <CardTitle>
                          {selectedAssignment.customer.name}
                        </CardTitle>
                        {selectedAssignment.contacts.map(contact => (
                          <Row id={contact.id}>
                            <Col xs={12}>
                              {contact.name} ({contact.title})
                            </Col>
                            {contact.phone && (
                              <Col xs={12}>{contact.phone}</Col>
                            )}
                            {contact.email && (
                              <Col xs={12}>
                                <a href={'mailto:' + contact.email}>
                                  {contact.email}
                                </a>
                              </Col>
                            )}
                          </Row>
                        ))}
                      </CardBody>
                    </Card>
                  </Col>
                  <Col xs={12} md={4} lg={3}>
                    <Card>
                      <CardBody>
                        <CardTitle>Kompetenser</CardTitle>
                        <Row>
                          {selectedAssignment.skills.map(skill => (
                            <Col xs={12}>{skill.name}</Col>
                          ))}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                  <Col xs={12} md={4} lg={3}>
                    <Card>
                      <CardBody>
                        <CardTitle>Befattningar</CardTitle>
                        <Row>
                          {selectedAssignment.occupations.map(occupation => (
                            <Col xs={12}>{occupation.name}</Col>
                          ))}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                  <Col xs={12} md={4} lg={3}>
                    <Card>
                      <CardBody>
                        <CardTitle>Rekryterare</CardTitle>
                        <Row>
                          {selectedAssignment.recruiters.map(recruiter => (
                            <Col xs={12}>
                              {recruiter.first_name} {recruiter.last_name}
                            </Col>
                          ))}
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </Container>
            )}
            <DialogContainer
              id="candidate-process-dialog"
              aria-describedby="candidate-process-dialog"
              visible={this.state.commentDialogVisible}
              onHide={this.toggleCommentDialog}
              actions={[
                {
                  label: 'Ok',
                  primary: true,
                  onClick: this.toggleCommentDialog
                }
              ]}
              width={800}
            >
              <TextField
                id="floating-multiline"
                label="Kommentar"
                lineDirection="right"
                rows={2}
                placeholder="Skriv en kommentar..."
                defaultValue={assignmentState && assignmentState.notes}
                fullWidth
                onChange={this.handleCommentInputChange}
                name="notes"
              />
            </DialogContainer>
          </TabPane>
          <TabPane tabId="2">
            {selectedAssignment && <MatchingCandidatesTable />}
          </TabPane>
          <TabPane tabId="3">{selectedAssignment && <Activities />}</TabPane>
          <TabPane tabId="4">
            {selectedAssignment &&
              hasAd && <Analytics assignment={selectedAssignment} />}
          </TabPane>
        </TabContent>

        {selectedAssignment && (
          <AssignmentsFormNew
            assignment={selectedAssignment}
            unnested={unnestedSelectedAssignment}
            show={this.state.assignmentModal}
            mEvent={this.state.mEvent}
          />
        )}

        {selectedAssignment &&
          checkedCandidates.length > 0 && (
            <SendMailToCandidates
              show={this.state.mailModal}
              mailType={this.state.mailType}
              checkedCandidates={checkedCandidates}
              assignment={selectedAssignment}
              toggle={this.toggleMailModal}
            />
          )}
      </div>
    )
  }
}

export default withRouter(connect(state => state)(AssignmentNew))
