import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { getAssignments } from '../../../store/actions/assignments'
import { Card, CardBody, Container } from 'reactstrap'
import AssignmentsTable from '../components/AssignmentsTable'
import SubHeader from '../../../layouts/Header/SubHeader'

let clickedId
class AllAssignments extends React.Component {
  constructor(props) {
    super(props)

    document.title = 'Alla uppdrag | wap recruiter'

    this.state = {
      loadsave: true
    }
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getAssignments()).then(() => {
      this.setState({
        loadsave: false,
        assignments: this.props.assignments.allAssignments
      })
    })
  }

  render() {
    return (
      <div>
        <SubHeader />
        <Container fluid className="contentContainer">
          <Card>
            <CardBody>
              <AssignmentsTable
                assignmentsList={
                  this.props.assignments.allAssignments
                    ? this.props.assignments.allAssignments
                    : []
                }
                loading={this.state.loadsave}
                clickHandler={this.setClickedAssigmentInRedux}
                showMineColumn
              />
            </CardBody>
          </Card>
        </Container>
      </div>
    )
  }
}

export default withRouter(connect(state => state)(AllAssignments))
