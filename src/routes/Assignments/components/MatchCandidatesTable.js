import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import ReactTable from 'react-table'
import PropTypes from 'prop-types'
import {
  createCandidateProcess,
  getProcessStateValues,
  getMatchedCandidates,
  removeCandidateFromMatchedCandidates
} from '../../../store/actions/assignments'
import { setCandidate } from '../../../store/actions/candidates'
import { apiClient } from '../../../store/axios.config'
import { Container, Row, Col } from 'reactstrap'
import { Button, Switch, DialogContainer, Toolbar } from 'react-md'
import CandidateSubComponent, {
  CandidateInfo,
  Matches
} from './CandidateSubComponent'
import moment from 'moment'
import _ from 'lodash'

class MatchCandidatesTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      filterCounties: this.props.assignments.unnestedSelectedAssignment
        .counties,
      matchedCandidates: this.props.assignments.selectedAssignment
        .matchedCandidates,
      visible: false,
      pageX: null,
      pageY: null
    }

    this.setClickedUserInRedux = this.setClickedUserInRedux.bind(this)
    this.addCandidateToProcess = this.addCandidateToProcess.bind(this)
    this.toggleCountySwitches = this.toggleCountySwitches.bind(this)
    this.filterCounties = this.filterCounties.bind(this)
    this.toggle = this.toggle.bind(this)
    this.matchCandidates = this.matchCandidates.bind(this)

    let { dispatch } = this.props
    dispatch(getProcessStateValues())
  }

  toggle(e) {
    // provide a pageX/pageY to the dialog when making visible to make the
    // dialog "appear" from that x/y coordinate
    let { pageX, pageY } = e
    if (e.changedTouches) {
      pageX = e.changedTouches[0].pageX
      pageY = e.changedTouches[0].pageY
    }

    this.setState({ visible: !this.state.visible, pageX, pageY })
  }

  setClickedUserInRedux(e, user) {
    e.persist()
    let { dispatch } = this.props
    dispatch(setCandidate(user)).then(() => {
      this.props.history.push(
        `/candidates/${this.props.candidates.selectedCandidate.user}`
      )
      // this.toggle(e)
    })
  }

  componentDidUpdate(prevProps, prevState) {
    // if (
    //   prevProps.assignments.selectedAssignment.matchedCandidates !==
    //   this.props.assignments.selectedAssignment.matchedCandidates
    // ) {
    //   this.setState({
    //     matchedCandidates: this.props.assignments.selectedAssignment
    //       .matchedCandidates
    //   })
    // }

    console.log(prevState)

    if (prevState.filterCounties !== this.state.filterCounties) {
      this.filterCounties()
    }

    if (
      prevProps.assignments.selectedAssignment.matchedCandidates !==
      this.props.assignments.selectedAssignment.matchedCandidates
    ) {
      let { matchedCandidates } = this.props.assignments.selectedAssignment
      let mCandidates = _.filter(matchedCandidates, function(user) {
        return user.counties.length > 0
      })

      this.setState({
        matchedCandidates: mCandidates
      })
    }
  }

  componentDidMount() {
    this.matchCandidates()
  }

  matchCandidates(force = false) {
    let { dispatch } = this.props
    this.setState({
      loadsave: true
    })

    dispatch(
      getMatchedCandidates(
        this.props.assignments.selectedAssignment.skills,
        this.props.assignments.selectedAssignment.occupations,
        this.props.assignments.unnestedSelectedAssignment.counties,
        force
      )
    ).then(() => {
      let { matchedCandidates } = this.props.assignments.selectedAssignment

      let mCandidates = _.filter(matchedCandidates, function(user) {
        return user.counties.length > 0
      })
      this.setState({
        matchedCandidates: mCandidates
      })
    })
  }

  addCandidateToProcess(candidate) {
    let { dispatch, assignments } = this.props

    let process = {
      user: candidate.user,
      comment: '',
      states: []
    }

    let state = {
      recruiter: this.props.user.profile.id,
      value: this.props.assignments.processStateValues[0].id,
      comment: ''
    }

    dispatch(
      createCandidateProcess(assignments.selectedAssignment.id, process, state)
    ).then(result => {
      console.log(result)
      dispatch(removeCandidateFromMatchedCandidates(candidate))
    })
  }

  toggleCountySwitches(countyId) {
    console.log('toggle switch')
    let { filterCounties } = this.state
    let newCounties = Object.assign([], filterCounties)
    if (countyId === 'sweden') {
      if (filterCounties.length === 0) {
        this.setState({
          filterCounties: this.props.assignments.unnestedSelectedAssignment
            .counties
        })
      } else {
        this.setState({
          filterCounties: []
        })
      }
    } else {
      if (
        _.find(filterCounties, function(o) {
          return o == countyId
        })
      ) {
        _.remove(newCounties, function(n) {
          return n === countyId
        })
      } else {
        newCounties.push(countyId)
      }
      this.setState({
        filterCounties: newCounties
      })
    }
  }

  filterCounties() {
    console.log('filterCounties')
    let { filterCounties } = this.state
    let { matchedCandidates } = this.props.assignments.selectedAssignment
    let mCandidates = matchedCandidates

    if (filterCounties.length > 0) {
      mCandidates = _.filter(matchedCandidates, function(candidate) {
        return _.find(filterCounties, function(county) {
          return _.find(candidate.counties, function(candidateCounty) {
            return candidateCounty === county
          })
        })
      })
    }

    this.setState({
      matchedCandidates: mCandidates
    })
  }

  renderSwitches() {
    let {
      counties,
      fetchingMatchedCandidates
    } = this.props.assignments.selectedAssignment

    let { filterCounties } = this.state

    let switches = [
      <Switch
        key="sweden"
        id="sweden"
        name="sweden"
        label="Hela Sverige"
        labelBefore
        className="mr-5"
        disabled={fetchingMatchedCandidates}
        onChange={() => this.toggleCountySwitches('sweden')}
        checked={filterCounties.length === 0}
      />
    ]
    counties &&
      counties.length > 0 &&
      counties.map(county => {
        switches.push(
          <Switch
            key={county.id}
            id={county.id}
            name={county.name}
            label={county.name}
            labelBefore
            className="mr-5"
            disabled={fetchingMatchedCandidates}
            onChange={() => this.toggleCountySwitches(county.id)}
            checked={
              _.findIndex(filterCounties, function(fCounty) {
                return fCounty === county.id
              }) > -1
            }
          />
        )
      })
    return switches
  }

  render() {
    // console.log(this.props.assignments.selectedAssignment.matchedCandidates)
    let {
      fetchingMatchedCandidates
    } = this.props.assignments.selectedAssignment
    let numCriterias = this.props.skills.length + this.props.occupations.length
    const { visible, pageX, pageY } = this.state

    return (
      <Container fluid className="matchedCandidates my-5">
        <Row>
          <Col className="d-flex flex-row">{this.renderSwitches()}</Col>
          <Button
            flat
            primary
            swapTheming
            style={{ marginRight: 15 }}
            disabled={fetchingMatchedCandidates}
            onClick={() => this.matchCandidates(true)}
          >
            Uppdatera
          </Button>
        </Row>
        <Row>
          <Col>
            <ReactTable
              data={this.state.matchedCandidates}
              className="-highlight"
              filterable
              previousText="Föregående"
              nextText="Nästa"
              loadingText="Hämtar matchande kandidater..."
              noDataText="Inga matchande kandidater"
              pageText="Sida"
              ofText="av"
              rowsText="rader"
              loading={
                this.props.assignments.selectedAssignment
                  .fetchingMatchedCandidates
              }
              defaultSorted={[
                {
                  id: 'matches',
                  desc: true
                }
              ]}
              defaultPageSize={20}
              defaultFilterMethod={(filter, row, column) => {
                const id = filter.pivotId || filter.id
                return row[id] !== undefined
                  ? String(row[id])
                      .toLowerCase()
                      .startsWith(filter.value.toLowerCase())
                  : true
              }}
              getTdProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: (e, handleOriginal) => {
                    console.log('A Td Element was clicked!')
                    console.log('it produced this event:', e)
                    console.log('It was in this column:', column)
                    console.log('It was in this row:', rowInfo)
                    console.log('It was in this table instance:', instance)
                    if (column.id && rowInfo) {
                      this.setClickedUserInRedux(e, rowInfo.row._original)
                    } else {
                      handleOriginal()
                    }
                  }
                }
              }}
              columns={[
                {
                  Header: 'Förnamn',
                  accessor: 'first_name'
                },
                {
                  Header: 'Efternamn',
                  accessor: 'last_name'
                },
                {
                  Header: 'Titel',
                  accessor: 'title'
                },
                {
                  id: 'birthday',
                  Header: 'Ålder',
                  accessor: c => moment().diff(c.birthday, 'years'),
                  maxWidth: 70
                },
                {
                  Header: 'Stad',
                  accessor: 'city'
                },
                {
                  Header: 'Matchar',
                  accessor: 'matches',
                  filterable: false,
                  maxWidth: 100,
                  Cell: row => (
                    <div
                      style={{
                        width: '100%',
                        height: 13,
                        marginTop: 3,
                        backgroundColor: '#dadada',
                        borderRadius: '0px'
                      }}
                    >
                      <div
                        style={{
                          width: `${row.value / numCriterias * 100}%`,
                          height: '100%',
                          backgroundColor: getColor(
                            row.value / numCriterias * 100
                          ),
                          borderRadius: '2px',
                          transition: 'all .2s ease-out'
                        }}
                      />
                    </div>
                  )
                }
              ]}
              SubComponent={row => {
                let candidate = row.original
                return (
                  <MatchCandidatesTableSubComponent
                    candidate={candidate}
                    assignment={this.props.assignments.selectedAssignment}
                    addCandidateToProcess={this.addCandidateToProcess}
                  />
                )
              }}
            />
          </Col>
        </Row>

        <DialogContainer
          id="simple-full-page-dialog"
          visible={visible}
          pageX={pageX}
          pageY={pageY}
          fullPage
          onHide={this.toggle}
          aria-labelledby="simple-full-page-dialog-title"
        >
          <Toolbar
            fixed
            colored
            title="New Event"
            titleId="simple-full-page-dialog-title"
            nav={
              <Button icon onClick={this.toggle}>
                close
              </Button>
            }
          />
        </DialogContainer>
      </Container>
    )
  }
}

MatchCandidatesTable.propTypes = {
  skills: PropTypes.array,
  occupations: PropTypes.array
}

export default withRouter(connect(state => state)(MatchCandidatesTable))

function filterCounties() {}

function getColor(percentValue) {
  // value from 0 to 1
  let value = 1 - percentValue / 100
  let hue = ((1 - value) * 120).toString(10)
  return ['hsl(', hue, ',100%,50%)'].join('')
}

class MatchCandidatesTableSubComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      candidate: undefined,
      imageUrl: process.env.PUBLIC_URL + '/imgs/no_picture.jpg'
    }

    this.onError = this.onError.bind(this)
  }

  componentDidMount() {
    apiClient
      .get('/recruiter/profiles/' + this.props.candidate.user)
      .then(result => {
        this.setState({
          candidate: result.data,
          imageUrl:
            'https://api.wapcard.se/api/v1/profiles/' +
            result.data.user +
            '/picture/500'
        })
      })
  }

  onError() {
    this.setState({
      imageUrl: process.env.PUBLIC_URL + '/imgs/no_picture.jpg'
    })
  }

  render() {
    let { candidate } = this.props
    return (
      <div style={{ padding: '10px 10px 10px 33px' }}>
        <Row>
          <Col className="d-flex">
            <img
              src={this.state.imageUrl}
              className="profile-pic img-fluid"
              style={{ borderRadius: '50%' }}
              onError={this.onError}
            />
            <CandidateInfo candidate={this.props.candidate} />
            <Matches
              candidate={this.state.candidate}
              assignment={this.props.assignment}
            />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col>
            <Button
              flat
              secondary
              swapTheming
              onClick={() => this.props.addCandidateToProcess(candidate)}
            >
              Lägg till i uppdrag
            </Button>
          </Col>
        </Row>
      </div>
    )
  }
}
