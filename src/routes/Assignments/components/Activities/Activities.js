import React from 'react'
import { connect } from 'react-redux'
import { Container, Row, Col } from 'reactstrap'
import RecruitmentProcesses from './RecruitmentProcesses'
import AssignmentStates from './AssignmentStates'
import moment from 'moment'
import 'moment/locale/sv'
import _ from 'lodash'
import { List, ListItem, Avatar } from 'react-md'

moment.locale('sv')

class Activities extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      activities: []
    }

    this.setupActivities = this.setupActivities.bind(this)
  }

  componentDidMount() {
    this.setupActivities()
  }

  getType(state) {
    if (state.value.icon === 'Inkommande') {
      return 'created'
    } else {
      return 'changed'
    }
  }

  setupActivities() {
    let { selectedAssignment } = this.props.assignments
    let { states, recruitment_processes } = selectedAssignment

    let mActivites = []

    recruitment_processes &&
      recruitment_processes.map(process => {
        let createdArray = _.filter(process.states, function(s) {
          return s.value.icon === 'Inkommande'
        })

        process.states.map((state, index) => {
          if (state.value.icon === 'Inkommande') {
            mActivites.push({
              id: 'created_' + state.id,
              recruiter: state.recruiter
                ? state.recruiter.first_name + ' ' + state.recruiter.last_name
                : null,
              date: state.start_date ? state.start_date : state.date,
              user: process.user,
              type: 'created',
              createType: state.value.name
            })
          }

          if (state.start_date && state.value.icon !== 'Inkommande') {
            mActivites.push({
              id: 'moved_' + state.id,
              recruiter:
                state.recruiter.first_name + ' ' + state.recruiter.last_name,
              date: state.start_date,
              user: process.user,
              type: 'moved',
              toState: state.value.name,
              fromState:
                process.states[index - 1] &&
                process.states[index - 1].value.name
            })

            mActivites.push({
              id: 'changed_' + state.id,
              recruiter:
                state.recruiter.first_name + ' ' + state.recruiter.last_name,
              date: state.date,
              user: process.user,
              type: 'changed',
              currentState: state.value.name
            })
          }
        })
      })

    mActivites.sort(function(a, b) {
      a = new Date(a.date)
      b = new Date(b.date)
      return a > b ? -1 : a < b ? 1 : 0
    })

    {
      return mActivites.map((activity, index) => (
        <div key={activity.id}>
          {index === 0 && (
            <h3 style={{ textTransform: 'capitalize' }}>
              {moment(activity.date).format('dddd DD MMM YYYY')}
            </h3>
          )}
          {index > 0 &&
            mActivites[index - 1].date !== activity.date && (
              <h3 className="mt-3" style={{ textTransform: 'capitalize' }}>
                {moment(activity.date).format('dddd DD MMM YYYY')}
              </h3>
            )}

          <ListItem
            leftAvatar={<ImageAvatar candidate={activity.user} />}
            primaryText={
              activity.user.first_name + ' ' + activity.user.last_name
            }
            secondaryText={this.getSecondaryText(activity)}
          />
        </div>
      ))
    }
  }

  getSecondaryText(activity) {
    switch (activity.type) {
      case 'moved':
        return (
          activity.recruiter +
          ' flyttade från ' +
          activity.fromState +
          ' till ' +
          activity.toState
        )

      case 'created':
        let trimmedIncoming = activity.createType.replace('Inkommen via', '')
        return 'Lades till via ' + trimmedIncoming

      case 'changed':
        return activity.recruiter + ' uppdaterade i ' + activity.currentState
    }
  }

  render() {
    let { activities } = this.state
    return (
      <Container style={{ background: '#ffffff', padding: 50, marginTop: 50 }}>
        <List>{this.setupActivities()}</List>
      </Container>
    )
  }
}

export default connect(state => state)(Activities)

class ImageAvatar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      imageUrl: `https://api.wapcard.se/api/v1/profiles/${
        props.candidate.id
      }/picture/500`
    }
  }

  render() {
    return (
      <Avatar role="presentation">
        {this.state.imageUrl ? (
          <img
            src={this.state.imageUrl}
            style={{ width: '100%', height: 'auto' }}
            onError={() => this.setState({ imageUrl: undefined })}
          />
        ) : (
          this.props.candidate.first_name.charAt(0)
        )}
      </Avatar>
    )
  }
}
