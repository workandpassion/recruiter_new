import React from 'react'
import { connect } from 'react-redux'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import {
  Row,
  Col,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from 'reactstrap'

class AssignmentForm extends React.Component {
  constructor(props) {
    super(props)

    let mCustomerValue = undefined
    let mContactsValue = []
    let mTypeValue = undefined

    if (props.assignment) {
      mCustomerValue = Object.assign(
        {
          value: props.assignment.customer.id,
          label: props.assignment.customer.name
        },
        props.assignment.customer
      )

      props.assignment.contacts &&
        props.assignment.contacts.map(contact => {
          mContactsValue.push(
            Object.assign(
              {
                value: contact.id,
                label: contact.name
              },
              contact
            )
          )
        })

      mTypeValue = props.assignment.employment_type
    }

    this.state = {
      ad: props.assignment &&
        props.assignment.ad && {
          ...props.assignment.ad,
          ad_partners: _.map(props.assignment.ad.ad_partners, 'id')
        },
      ad_partner: undefined,
      loadsave: false,
      customerModal: false,
      customerValue: mCustomerValue,
      contactModal: false,
      contactValue: mContactsValue,
      typeValue: mTypeValue,
      occupationValue: undefined,
      skillValue: undefined,
      occupationOptions: undefined,
      skillOptions: undefined,
      countyOptions: undefined,
      countyValue: undefined,
      editMode: !!props.assignment && props.assignment.subject,
      assignment: props.assignment
        ? {
            ...props.assignment,
            difficulty: props.assignment.difficulty
              ? props.assignment.difficulty
              : 1,
            counties: _.map(this.props.assignment.counties, 'id'),
            contacts: _.map(props.assignment.contacts, 'id'),
            occupations: _.map(props.assignment.occupations, 'id'),
            skills: _.map(props.assignment.skills, 'id'),
            recruiter_offices: _.map(props.assignment.recruiter_offices, 'id'),
            recruiters: _.map(props.assignment.recruiters, 'id'),
            states: _.map(props.assignment.states, 'id'),
            customer: props.assignment.customer.id,
            ad: props.assignment.ad ? props.assignment.ad.id : undefined,
            recruitment_processes: _.map(
              props.assignment.recruitment_processes,
              'id'
            ),
            matchedCandidates: []
          }
        : {
            recruiters: [props.user.profile.id],
            occupations: [],
            skills: [],
            difficulty: 1
          }
    }
  }

  render() {
    return (
      <AvForm>
        <Row>
          <Col xs={12} lg={6}>
            <AvGroup>
              <AvField
                name="subject"
                label="Uppdragstitel"
                required
                onChange={this.handleInputChange}
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={3}>
            <AvGroup>
              <AvField
                name="price"
                label="Pris"
                type="number"
                onChange={this.handleInputChange}
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={3}>
            <AvGroup>
              <Label>Typ</Label>
              <Select
                placeholder={'Välj typ'}
                value={this.state.typeValue}
                options={typeOptions}
                onChange={this.onTypeChange}
              />
              <AvField
                type="hidden"
                name="employment_type"
                value={this.state.typeValue && this.state.typeValue.value}
                required
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Kund</Label>
              <Creatable
                placeholder={'Välj kund'}
                value={this.state.customerValue}
                options={this.getCustomerOptions()}
                onChange={this.onCustomerChange}
                promptTextCreator={label => {
                  return 'Skapa kund ' + label
                }}
                onNewOptionClick={option => {
                  this.addCustomer(option)
                }}
              />
              <AvField
                type="hidden"
                name="customer"
                value={
                  this.state.customerValue && this.state.customerValue.value
                    ? this.state.customerValue.value
                    : ''
                }
                required
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Kontaktperson</Label>
              <Creatable
                multi
                disabled={!this.state.customerValue}
                placeholder={
                  this.state.customerValue
                    ? 'Välj kontaktperson(er)'
                    : 'Välj kund först'
                }
                value={this.state.contactValue}
                options={this.getContactOptions()}
                onChange={this.onContactChange}
                promptTextCreator={label => {
                  return 'Skapa kontaktperson ' + label
                }}
                onNewOptionClick={option => {
                  this.addContact(option)
                }}
              />
              <AvField
                type="hidden"
                name="contact_person"
                value={this.state.contactValue && this.state.contactValue.value}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Uppdragsbefattningar</Label>
              <Select.Async
                multi
                placeholder={'Välj befattningar'}
                value={this.state.occupationValue}
                loadOptions={this.getOccupationOptionsAsync}
                onChange={this.onOccupationChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Kompetenser</Label>
              <Select.Async
                multi
                placeholder={'Välj kompetenser'}
                value={this.state.skillValue}
                loadOptions={this.getSkillOptionsAsync}
                onChange={this.onSkillChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Län</Label>
              <Select.Async
                multi
                placeholder={'Välj län'}
                value={this.state.countyValue}
                loadOptions={this.getCountyOptionsAsync}
                onChange={this.onCountyChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={3}>
            <AvGroup>
              <AvField
                type="select"
                name="difficulty"
                label="Svårighetsgrad"
                onChange={this.handleInputChange}
              >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </AvField>
            </AvGroup>
          </Col>

          <Col xs={12}>
            <AvField
              type="textarea"
              name="body"
              label="Kravspec / Uppdragsinformation"
              onChange={this.handleInputChange}
            />
          </Col>
          <Col xs={12}>
            <AvField
              type="textarea"
              name="comment"
              label="Interna kommentarer"
              onChange={this.handleInputChange}
            />
          </Col>
        </Row>
      </AvForm>
    )
  }
}
