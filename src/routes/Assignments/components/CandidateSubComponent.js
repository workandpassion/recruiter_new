import React from 'react'
import { withRouter } from 'react-router-dom'
import RecruitementProcessStates from '../../../components/RecruitmentProcessStates/RecruitementProcessStates'
import ProcessStateForm from './ProcessStateForm'
import { Collapse, Row, Col, Progress } from 'reactstrap'
import { apiClient } from '../../../store/axios.config'
import classNames from 'classnames'
import _ from 'lodash'

class CandidateSubComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      imageUrl:
        'https://api.wapcard.se/api/v1/profiles/' +
        this.props.candidate.id +
        '/picture/500',
      candidate: undefined,
      collapse: false,
      showProcessStateForm: false,
      clickedProcessState: undefined
    }

    this.onError = this.onError.bind(this)
    this.toggleCollapse = this.toggleCollapse.bind(this)
    this.toggleProcessStateForm = this.toggleProcessStateForm.bind(this)
    this.gotoCandidate = this.gotoCandidate.bind(this)
    this.setClickedProcessState = this.setClickedProcessState.bind(this)
  }

  componentDidMount() {}

  onError() {
    this.setState({
      imageUrl: process.env.PUBLIC_URL + '/imgs/no_picture.jpg'
    })
  }

  toggleCollapse() {
    if (this.state.collapse) {
      this.setState({ collapse: false })
    } else {
      this.setState({
        loadsave: true
      })

      apiClient
        .get('recruiter/profiles/' + this.props.candidate.id)
        .then(result => {
          this.setState({
            candidate: result.data,
            collapse: true,
            loadsave: false
          })
        })
    }
  }

  componentDidUpdate(prevProps, prevState) {}

  setClickedProcessState(processState) {
    this.setState({
      clickedProcessState: processState,
      showProcessStateForm: true
    })
  }

  toggleProcessStateForm() {
    this.setState({
      clickedProcessState: undefined,
      showProcessStateForm: !this.state.showProcessStateForm
    })
  }

  gotoCandidate() {
    let { dispatch } = this.props
    this.props.history.push(`/candidates/${this.props.candidate.id}`)
  }

  render() {
    let { process } = this.props
    let chevronClass = classNames(
      'fa',
      this.state.collapse
        ? 'fa-chevron-down'
        : this.state.loadsave ? 'fa-spin fa-spinner' : 'fa-chevron-right'
    )

    return (
      <div
        className="subComponent"
        style={{ padding: '20px', background: '#ffffff' }}
      >
        <div className="d-flex">
          <div className="subProfilePic" onClick={this.gotoCandidate}>
            <img
              src={this.state.imageUrl}
              onError={this.onError}
              className="profile-pic img-fluid"
            />
          </div>
          <RecruitementProcessStates
            states={process.states}
            onClick={this.toggleProcessStateForm}
            editFn={this.setClickedProcessState}
          />
        </div>
        <div className="comment mt-3">{process.comment}</div>
        <div className="mt-3">
          <div onClick={this.toggleCollapse} style={{ cursor: 'pointer' }}>
            <i className={chevronClass} /> <span>Visa mer</span>
          </div>
          <Collapse isOpen={this.state.collapse} style={{ paddingTop: 20 }}>
            <Row>
              <CandidateInfo candidate={this.state.candidate} />
              {this.state.candidate && (
                <Matches
                  candidate={this.state.candidate}
                  assignment={this.props.selectedAssignment}
                />
              )}
            </Row>
          </Collapse>
        </div>
        {this.state.showProcessStateForm && (
          <ProcessStateForm
            show={this.state.showProcessStateForm}
            toggle={this.toggleProcessStateForm}
            processState={this.state.clickedProcessState}
            process={process}
          />
        )}
      </div>
    )
  }
}

export default withRouter(CandidateSubComponent)

export class CandidateInfo extends React.Component {
  render() {
    let { candidate } = this.props
    return (
      <Col className="d-flex flex-column">
        <h6>Kontaktinfo</h6>
        <div>
          <i className="fa fa-envelope before-text" />{' '}
          {candidate && (
            <a href={'mailto:' + candidate.email}>{candidate.email}</a>
          )}
        </div>
        {candidate &&
          candidate.mobile_phone_number && (
            <div>
              <i className="fa fa-phone before-text" />{' '}
              <span>{candidate.mobile_phone_number}</span>
            </div>
          )}
        {candidate &&
          candidate.phone_number && (
            <div>
              <i className="fa fa-phone before-text" /> {candidate.phone_number}
            </div>
          )}
      </Col>
    )
  }
}

export class Matches extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      matches: undefined
    }

    this.getMatches = this.getMatches.bind(this)
  }

  componentDidMount() {
    this.props.assignment && this.props.candidate && this.getMatches()
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.candidate && this.props.candidate) {
      this.getMatches()
    }
  }

  getMatches() {
    let { assignment, candidate } = this.props
    let mMatches = []
    assignment.skills &&
      assignment.skills.map(skill => {
        let mSkill = _.find(candidate.skills, { id: skill.id })
        if (mSkill) {
          mMatches.push(Object.assign({ type: 'skill' }, mSkill))
        }
      })

    assignment.occupations &&
      assignment.occupations.map(occupation => {
        let mOccupation = _.find(candidate.occupations, { id: occupation.id })
        if (mOccupation) {
          mMatches.push(Object.assign({ type: 'occupation' }, mOccupation))
        }
      })
    console.log(mMatches)
    this.setState({
      matches: mMatches
    })
  }

  render() {
    return (
      <Col>
        <h6>Matchar</h6>
        {this.state.matches &&
          this.state.matches.length > 0 &&
          this.state.matches.map(match => {
            let mClass = classNames(
              'fa before-text bg-green',
              match.type === 'skill' ? 'fa-rocket' : 'fa-briefcase'
            )
            return (
              <Row key={match.id}>
                <Col xs={5}>
                  <i className={mClass} /> {match.name}
                </Col>
                {match.type === 'skill' && (
                  <Col>
                    <Progress
                      value={match.experience}
                      max={5}
                      style={{ height: 10 }}
                      color="success"
                    />
                  </Col>
                )}
              </Row>
            )
          })}
        {!this.state.matches && <i className="fa fa-spin fa-spinner" />}
      </Col>
    )
  }
}
