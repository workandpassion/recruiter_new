import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import ReactTable from 'react-table'
import { Container, Row, Col } from 'reactstrap'
import { apiClient } from '../../../store/axios.config'
import axios from 'axios'
import _ from 'lodash'
import moment from 'moment'
import {
  getMatchingCandidates,
  createCandidateProcess,
  removeCandidateFromMatchedCandidates
} from '../../../store/actions/assignments'
import CandidateSubComponent, {
  CandidateInfo,
  Matches
} from './CandidateSubComponent'
import { Button, Toolbar, DialogContainer } from 'react-md'
import { setCandidate } from '../../../store/actions/candidates'
import CandidateProfile from '../../Candidates/CandidateProfile/CandidateProfile'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'

let CancelToken = axios.CancelToken
let source = CancelToken.source()

class MatchingCandidatesTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      page: 0,
      tableState: { page: 0, pageSize: 20 },
      expandedRows: {},
      visibleExpandedRows: {}
    }

    this.addCandidateToProcess = this.addCandidateToProcess.bind(this)
    this.setClickedUserInRedux = this.setClickedUserInRedux.bind(this)
    this.refreshMatches = this.refreshMatches.bind(this)
  }

  setClickedUserInRedux(candidate) {
    let { dispatch } = this.props
    dispatch(setCandidate(candidate)).then(() => {
      this.props.history.push(
        `/candidates/${this.props.candidates.selectedCandidate.user}`
      )
    })
  }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(
      getMatchingCandidates(this.props.assignments.selectedAssignment.id)
    ).then(result => {
      this.setState({
        data: result.matchedCandidates
      })
    })
  }

  componentDidUpdate(prevProps, prevState) {
    let { dispatch } = this.props
    let prevOccupations = prevProps.assignments.unnestedSelectedAssignment
      ? prevProps.assignments.unnestedSelectedAssignment.occupations
      : []
    let thisOccupations = this.props.assignments.unnestedSelectedAssignment
      ? this.props.assignments.unnestedSelectedAssignment.occupations
      : []

    let prevOccupationsDiff = _.difference(prevOccupations, thisOccupations)
    let thisOccupationsDiff = _.difference(thisOccupations, prevOccupations)

    let prevSkills = prevProps.assignments.unnestedSelectedAssignment
      ? prevProps.assignments.unnestedSelectedAssignment.skills
      : []
    let thisSkills = this.props.assignments.unnestedSelectedAssignment
      ? this.props.assignments.unnestedSelectedAssignment.skills
      : []

    let prevSkillsDiff = _.difference(prevSkills, thisSkills)
    let thisSkillsDiff = _.difference(thisSkills, prevSkills)

    let prevCounties = prevProps.assignments.unnestedSelectedAssignment
      ? prevProps.assignments.unnestedSelectedAssignment.counties
      : []

    let thisCounties = this.props.assignments.unnestedSelectedAssignment
      ? this.props.assignments.unnestedSelectedAssignment.counties
      : []

    let prevCountiesDiff = _.difference(prevCounties, thisCounties)
    let thisCountiesDiff = _.difference(thisCounties, prevCounties)

    if (
      prevOccupationsDiff.length > 0 ||
      thisOccupationsDiff.length > 0 ||
      prevSkillsDiff.length > 0 ||
      thisSkillsDiff.length > 0 ||
      prevCountiesDiff.length > 0 ||
      thisCountiesDiff.length > 0
    ) {
      console.log('get matching candidates')

      if (this.props.assignments.selectedAssignment) {
        dispatch(
          getMatchingCandidates(this.props.assignments.selectedAssignment.id)
        )
      }
    }

    if (prevState.expandedRows !== this.state.expandedRows) {
      this.setVisibleExpandedRows()
    }

    if (prevState.tableState !== this.state.tableState) {
      this.setVisibleExpandedRows()
    }
  }

  setVisibleExpandedRows() {
    const { expandedRows, tableState } = this.state
    const { page, pageSize } = tableState

    let offset = page * pageSize
    let visibleExpandedRows = {}

    for (let key in expandedRows) {
      if (expandedRows.hasOwnProperty(key)) {
        console.log(key + ' -> ' + expandedRows[key])
        console.log('offset: ' + offset)
        if (key >= offset && key <= offset + pageSize) {
          let mIndex = key - offset
          visibleExpandedRows[mIndex] = true
        }
      }
    }

    this.setState({
      visibleExpandedRows: visibleExpandedRows
    })
  }

  addCandidateToProcess(candidate) {
    let { dispatch, assignments } = this.props

    let process = {
      user: candidate.user,
      comment: '',
      states: []
    }

    let state = {
      recruiter: this.props.user.profile.id,
      value: this.props.assignments.processStateValues[0].id,
      comment: '',
      start_date: moment().format('YYYY-MM-DD')
    }

    dispatch(
      createCandidateProcess(assignments.selectedAssignment.id, process, state)
    ).then(result => {
      let indexInList = _.findIndex(this.state.data, { user: candidate.user })
      let mExpandedRows = Object.assign({}, this.state.expandedRows)
      if (indexInList in mExpandedRows) {
        delete mExpandedRows[indexInList]
      }

      for (let key in mExpandedRows) {
        if (key > indexInList) {
          delete mExpandedRows[key]
          mExpandedRows[key - 1] = true
        }
      }

      this.setState({
        expandedRows: mExpandedRows
      })
      dispatch(removeCandidateFromMatchedCandidates(candidate))
    })
  }

  refreshMatches() {
    let { dispatch } = this.props
    let { selectedAssignment } = this.props.assignments

    dispatch(getMatchingCandidates(selectedAssignment.id))
  }

  render() {
    let {
      fetchingMatchedCandidates,
      matchedCandidates
    } = this.props.assignments.selectedAssignment
    return (
      <Container fluid className="my-5">
        <Row>
          <Col className="d-flex justify-content-end">
            <Button
              flat
              swapTheming
              secondary
              onClick={this.refreshMatches}
              className="mb-2"
            >
              Uppdatera
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <ReactTable
              expanded={this.state.visibleExpandedRows}
              className="-highlight"
              sortable={false}
              onFetchData={(state, instance) => {
                // console.log(state)
                this.setState({
                  tableState: {
                    page: state.page,
                    pageSize: state.pageSize
                  }
                })
              }}
              getTrProps={(state, rowInfo, column) => {
                return {
                  style: {
                    backgroundColor:
                      rowInfo && rowInfo.row._original.new && '#53d67b'
                  }
                }
              }}
              loading={fetchingMatchedCandidates}
              data={matchedCandidates}
              defaultPageSize={this.state.tableState.pageSize}
              getTdProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: (e, handleOriginal) => {
                    // console.log('It was in this row:', rowInfo)
                    if (column.expander || rowInfo) {
                      let mIndex = Number(rowInfo.index)
                      let mExpandedRows = Object.assign(
                        {},
                        this.state.expandedRows
                      )
                      if (mIndex in mExpandedRows) {
                        delete mExpandedRows[mIndex]
                      } else {
                        mExpandedRows[mIndex] = true
                      }

                      this.setState({
                        expandedRows: mExpandedRows
                      })
                    }
                  }
                }
              }}
              columns={[
                {
                  Header: 'Förnamn',
                  accessor: 'first_name'
                },
                {
                  Header: 'Efternamn',
                  accessor: 'last_name'
                },
                {
                  Header: 'Stad',
                  accessor: 'city'
                },
                {
                  Header: 'Titel',
                  accessor: 'title'
                },
                {
                  Header: 'Kön',
                  accessor: 'gender',
                  maxWidth: 100,
                  Cell: row => {
                    switch (row.value) {
                      case 'male':
                        return <span>Man</span>
                      case 'female':
                        return <span>Kvinna</span>
                      case 'other':
                        return <span>Annat / Okänt</span>
                    }
                  },
                  Filter: ({ filter, onChange }) => (
                    <select
                      onChange={event => onChange(event.target.value)}
                      style={{ width: '100%' }}
                      value={filter ? filter.value : ''}
                    >
                      <option value="" />
                      <option value="female">Kvinna</option>
                      <option value="male">Man</option>
                      <option value="other">Annat/Okänt</option>
                    </select>
                  )
                },
                {
                  Header: 'Matchar',
                  accessor: 'score',
                  maxWidth: 100
                },
                {
                  Header: 'Profil',
                  accessor: 'progress',
                  filterable: false,
                  maxWidth: 100,
                  Cell: row => (
                    <div
                      style={{
                        width: '100%',
                        height: 13,
                        marginTop: 3,
                        backgroundColor: '#dadada',
                        borderRadius: '0px'
                      }}
                    >
                      <div
                        style={{
                          width: `${row.value}%`,
                          height: '100%',
                          backgroundColor: getColor(row.value),
                          borderRadius: '2px',
                          transition: 'all .2s ease-out'
                        }}
                      />
                    </div>
                  )
                },
                {
                  Header: 'Anv.id',
                  accessor: 'user_id',
                  Cell: row => <small>{row.value}</small>
                }
              ]}
              SubComponent={row => {
                let candidate = row.original
                return (
                  <MatchCandidatesTableSubComponent
                    candidate={candidate}
                    assignment={this.props.assignments.selectedAssignment}
                    addCandidateToProcess={this.addCandidateToProcess}
                    gotoCandidate={this.toggleProfileDialog}
                  />
                )
              }}
            />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default withRouter(connect(state => state)(MatchingCandidatesTable))

function getColor(percentValue) {
  // value from 0 to 1
  let value = 1 - percentValue / 100
  let hue = ((1 - value) * 120).toString(10)
  return ['hsl(', hue, ',100%,50%)'].join('')
}

class MatchCandidatesTableSubComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      candidate: undefined,
      imageUrl: process.env.PUBLIC_URL + '/imgs/no_picture.jpg',
      candidateProfileVisible: false,
      pageX: null,
      pageY: null
    }

    this.onError = this.onError.bind(this)
    this.toggleProfileDialog = this.toggleProfileDialog.bind(this)
    this.addCandidate = this.addCandidate.bind(this)
  }

  componentDidMount() {
    apiClient
      .get('/recruiter/profiles/' + this.props.candidate.user)
      .then(result => {
        this.setState({
          candidate: result.data,
          imageUrl:
            'https://api.wapcard.se/api/v1/profiles/' +
            result.data.user +
            '/picture/500?id=' +
            result.data.user
        })
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.candidate !== this.props.candidate) {
      this.setState({
        imageUrl:
          'https://api.wapcard.se/api/v1/profiles/' +
          this.props.candidate.user +
          '/picture/500?id=' +
          this.props.candidate.user,
        loading: false
      })
    }
  }

  addCandidate(candidate) {
    this.setState({
      loading: true
    })
    this.props.addCandidateToProcess(candidate)
  }

  toggleProfileDialog(e) {
    let { pageX, pageY } = e
    if (e.changedTouches) {
      pageX = e.changedTouches[0].pageX
      pageY = e.changedTouches[0].pageY
    }

    this.setState({
      candidateProfileVisible: !this.state.candidateProfileVisible,
      pageX,
      pageY
    })
  }

  onError() {
    this.setState({
      imageUrl: process.env.PUBLIC_URL + '/imgs/no_picture.jpg'
    })
  }

  render() {
    let { candidate } = this.props
    let { candidateProfileVisible, pageX, pageY } = this.state

    return (
      <div style={{ padding: '10px 10px 10px 33px' }}>
        <Row>
          <Col className="d-flex">
            <img
              src={this.state.imageUrl}
              className="profile-pic img-fluid"
              style={{ borderRadius: '50%' }}
              onError={this.onError}
            />
            <CandidateInfo candidate={this.props.candidate} />
            <Matches
              candidate={this.state.candidate}
              assignment={this.props.assignment}
            />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col>
            <LoadingButton
              loading={this.state.loading}
              onClick={() => this.addCandidate(candidate)}
            >
              Lägg till i uppdrag
            </LoadingButton>
            <Button
              className="ml-2"
              flat
              secondary
              swapTheming
              onClick={this.toggleProfileDialog}
            >
              Visa profil
            </Button>
          </Col>
        </Row>
        <DialogContainer
          id="simple-full-page-dialog"
          visible={candidateProfileVisible}
          pageX={pageX}
          pageY={pageY}
          fullPage
          onHide={this.toggleProfileDialog}
          aria-labelledby="simple-full-page-dialog-title"
        >
          <Toolbar
            fixed
            colored
            title={candidate.first_name + ' ' + candidate.last_name}
            titleId="simple-full-page-dialog-title"
            nav={
              <Button icon onClick={this.toggleProfileDialog}>
                close
              </Button>
            }
          />
          <section className="md-toolbar-relative">
            <CandidateProfile userId={candidate.user} />
          </section>
        </DialogContainer>
      </div>
    )
  }
}
