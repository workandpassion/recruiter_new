import React from 'react'
import {
  DialogContainer,
  List,
  ListItem,
  Avatar,
  FontIcon,
  Button
} from 'react-md'
import { connect } from 'react-redux'
import { apiClient } from '../../../../store/axios.config'
import _ from 'lodash'
import update from 'immutability-helper'
import moment from 'moment'

class SendMailToCandidates extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: false,
      mailsDone: []
    }

    this.hide = this.hide.bind(this)
    this.handleSentMails = this.handleSentMails.bind(this)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.show !== this.props.show) {
      this.setState({
        visible: this.props.show
      })
    }
  }

  hide() {
    this.setState({
      mailsDone: []
    })
    this.props.toggle()
  }

  handleSentMails(process, add = true) {
    const { checkedCandidates } = this.props

    let newState
    if (add) {
      newState = update(this.state.mailsDone, { $push: [process.id] })
    } else {
      let mIndex = this.state.mailsDone.indexOf(process.id)
      newState = update(this.state.mailsDone, { $splice: [[mIndex, 1]] })
    }

    this.setState({
      mailsDone: newState
    })
  }

  render() {
    const { visible, mailsDone } = this.state
    const { assignment, mailType, checkedCandidates } = this.props

    const actions = []

    // actions.push({ secondary: true, children: 'Cancel', onClick: this.hide })
    actions.push(
      <Button flat primary onClick={this.hide}>
        Stäng
      </Button>
    )

    return (
      <DialogContainer
        id="mail-dialog"
        visible={visible}
        title={'Skickar mail'}
        width={600}
        actions={mailsDone.length === checkedCandidates.length ? actions : []}
      >
        <List>
          {checkedCandidates.map(candidate => (
            <MailListItem
              key={candidate.id}
              candidate={candidate}
              assignment={assignment}
              event={mailType}
              callback={this.handleSentMails}
            />
          ))}
        </List>
      </DialogContainer>
    )
  }
}

export default connect(state => state)(SendMailToCandidates)

class MailListItem extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      mailStatus: MAIL_WAITING
    }

    this.getIcon = this.getIcon.bind(this)
    this.getRightIcon = this.getRightIcon.bind(this)
    this.sendMail = this.sendMail.bind(this)
    this.retryMail = this.retryMail.bind(this)
  }

  componentDidMount() {
    this.sendMail()
  }

  getIcon() {
    const { mailStatus } = this.state

    switch (mailStatus) {
      case MAIL_WAITING:
        return (
          <Avatar icon={<FontIcon iconClassName="fa fa-spin fa-spinner" />} />
        )
      case MAIL_SENT:
        return <Avatar icon={<FontIcon>check</FontIcon>} />
      case MAIL_ERROR:
        return <Avatar icon={<FontIcon error>error</FontIcon>} suffix="red" />
      case MAIL_REJECTED:
        return <Avatar icon={<FontIcon error>error</FontIcon>} suffix="red" />
      case MAIL_ALREADY_SENT:
        return (
          <Avatar icon={<FontIcon error>warning</FontIcon>} suffix="orange" />
        )
      default:
        return (
          <Avatar icon={<FontIcon iconClassName="fa fa-spin fa-spinner" />} />
        )
    }
  }

  getRightIcon() {
    const { mailStatus } = this.state

    switch (mailStatus) {
      case MAIL_ALREADY_SENT:
        return (
          <FontIcon onClick={this.retryMail} style={{ cursor: 'pointer' }}>
            redo
          </FontIcon>
        )
      default:
        return null
    }
  }

  getStatusText() {
    const { mailStatus, sentDate, rejectMessage } = this.state
    switch (mailStatus) {
      // case MAIL_WAITING:
      //   return 'Väntar'
      // case MAIL_SENT:
      //   return 'Skickades'
      case MAIL_ERROR:
        return 'Något gick fel'
      case MAIL_ALREADY_SENT:
        return (
          'Detta mail skickades ' +
          moment(sentDate).format('YYYY-MM-DD HH:mm') +
          '\nKlicka på pilen till höger för att skicka igen'
        )
      case MAIL_REJECTED:
        return rejectMessage
      default:
        return null
    }
  }

  retryMail() {
    const { candidate, callback } = this.props

    callback(candidate, false)

    this.sendMail(true)
  }

  sendMail(resend = false) {
    const { assignment, candidate, event, callback } = this.props

    this.setState({
      mailStatus: MAIL_WAITING
    })

    apiClient
      .post(
        'recruiter/assignments/' +
          assignment.id +
          '/recruitment-processes/' +
          candidate.id +
          '/emails/',
        {
          event: event,
          resend: resend ? 'true' : 'false'
        }
      )
      .then(result => {
        if (result.data.reject_reason) {
          this.setState({
            mailStatus: MAIL_REJECTED,
            rejectMessage: result.data.reject_reason
          })
        } else if (
          !moment(result.data.created_at).isAfter(
            moment().subtract(15, 'seconds')
          )
        ) {
          this.setState({
            mailStatus: MAIL_ALREADY_SENT,
            sentDate: result.data.created_at
          })
        } else {
          this.setState({
            mailStatus: MAIL_SENT
          })
        }

        callback(candidate)
      })
      .catch(error => {
        this.setState({
          mailStatus: MAIL_ERROR
        })

        callback(candidate)
      })
  }

  render() {
    const { candidate } = this.props
    return (
      <ListItem
        leftAvatar={this.getIcon()}
        rightIcon={this.getRightIcon()}
        primaryText={candidate.user.first_name + ' ' + candidate.user.last_name}
        secondaryText={this.getStatusText()}
        threeLines
      />
    )
  }
}

const MAIL_WAITING = 'mail-waiting'
const MAIL_SENT = 'mail-sent'
const MAIL_ERROR = 'mail-error'
const MAIL_ALREADY_SENT = 'mail-already-sent'
const MAIL_REJECTED = 'mail-rejected'
