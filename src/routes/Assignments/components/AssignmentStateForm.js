import React from 'react'
import { connect } from 'react-redux'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import { Row, Col, Modal, ModalBody, ModalFooter, Button } from 'reactstrap'
import _ from 'lodash'
import {
  createAssignmentState,
  updateAssignmentState,
  updateAssignment
} from '../../../store/actions/assignments'
import { apiClient } from '../../../store/axios.config'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'

class AssignmentStateForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      assignmentState: this.props.assignmentState.value
        ? this.props.assignmentState
        : undefined,
      stateOptions: undefined
    }

    this.handleValidSubmit = this.handleValidSubmit.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.shouldOptionsBeDisabled = this.shouldOptionsBeDisabled.bind(this)
  }

  componentDidMount() {
    apiClient.get('/recruiter/assignments-state-values').then(result => {
      this.setState({
        stateOptions: result.data
      })
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.stateOptions && this.state.stateOptions) {
      this.setState({
        assignmentState: {
          ...this.state.assignmentState,
          value: this.getDefaultValue()
        }
      })
    }

    let stateAssignmentState = this.state.assignmentState
    let prevPropsAssignmentState = prevProps.assignmentState

    if (prevProps.assignmentState !== this.state.assignmentState) {
      console.log('assignmentState')
      // this.setState({
      //   assignmentState: this.props.assignmentState
      // })
    }
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        assignmentState: {
          ...this.state.assignmentState,
          [name]: value
        }
      })
    }
  }

  handleValidSubmit(event, values) {
    let { dispatch } = this.props
    let assignmentId = this.props.assignments.selectedAssignment.id

    if (this.props.assignmentState.value) {
      let mAssignmentState = {
        date: this.state.assignmentState.date,
        id: this.state.assignmentState.id,
        notes: this.state.assignmentState.notes,
        value: this.state.assignmentState.value
      }
      dispatch(
        updateAssignmentState(assignmentId, mAssignmentState)
      ).then(() => {
        this.setState({
          assignmentState: undefined
        })
        this.props.toggle()
      })
    } else {
      dispatch(
        createAssignmentState(assignmentId, this.state.assignmentState)
      ).then(() => {
        this.setState({
          assignmentState: undefined
        })
        this.props.toggle()
      })
    }
  }

  shouldOptionsBeDisabled(value) {
    let { states } = this.props.assignments.selectedAssignment
    if (value) {
      return _.find(states, { value: { id: value } })
    } else {
      return false
    }
  }

  getDefaultValue() {
    let { states } = this.props.assignments.selectedAssignment
    let { stateOptions } = this.state
    if (states.length > 0) {
      if (this.props.assignmentState && this.props.assignmentState.value) {
        let index = _.findIndex(this.state.stateOptions, {
          id: this.props.assignmentState.value.id
        })

        return this.state.stateOptions[index].id
      } else {
        let index = _.findIndex(this.state.stateOptions, {
          id: states[states.length - 1].value.id
        })
        return index + 1 < this.state.stateOptions.length
          ? this.state.stateOptions[index + 1].id
          : this.state.stateOptions[index].id
      }
    }
    return this.state.stateOptions[0].id
  }

  render() {
    return (
      <Modal isOpen={this.props.show} toggle={this.props.toggle}>
        <AvForm
          onValidSubmit={this.handleValidSubmit}
          model={this.state.assignmentState}
        >
          <ModalBody>
            <Row>
              <Col xs={12}>
                <AvGroup>
                  {this.state.stateOptions && (
                    <AvField
                      type="select"
                      name="value"
                      label="Status"
                      onChange={this.handleInputChange}
                      value={this.getDefaultValue()}
                    >
                      {this.state.stateOptions.map(option => {
                        return (
                          <option
                            key={option.id}
                            value={option.id}
                            disabled={this.shouldOptionsBeDisabled(option.id)}
                          >
                            {option.name}
                          </option>
                        )
                      })}
                    </AvField>
                  )}
                </AvGroup>
              </Col>
              <Col xs={12}>
                <AvGroup>
                  <AvField
                    type="textarea"
                    name="notes"
                    label="Kommentar"
                    onChange={this.handleInputChange}
                  />
                </AvGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <LoadingButton
              color="primary"
              type="submit"
              loading={
                this.props.assignments.selectedAssignment
                  .creatingAssignmentState
              }
            >
              {this.props.assignmentState.value ? 'Redigera' : 'Lägg till'}
            </LoadingButton>{' '}
            <Button color="secondary" onClick={this.props.toggle}>
              Avbryt
            </Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    )
  }
}

export default connect(state => state)(AssignmentStateForm)
