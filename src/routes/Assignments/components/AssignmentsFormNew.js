import React from 'react'
import { DialogContainer, Toolbar, Button } from 'react-md'
import {
  Container,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap'
import { connect } from 'react-redux'
import classnames from 'classnames'
import AdForm from './AdForm'
import AssignmentsForm from './AssignmentsForm'
import moment from 'moment'

class AssignmentsFormNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: false,
      pageX: null,
      pageY: null,
      activeTab: '1',
      hasAssignment: props.assignment !== undefined
    }

    this.toggle = this.toggle.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.toggleTab = this.toggleTab.bind(this)
    this.createdAssignment = this.createdAssignment.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.show !== this.props.show) {
      this.toggle(this.props.mEvent)
    }
  }

  toggle(e) {
    console.log(e)
    let { pageX, pageY } = e
    if (e.changedTouches) {
      pageX = e.changedTouches[0].pageX
      pageY = e.changedTouches[0].pageY
    }

    this.setState({ visible: !this.state.visible, pageX, pageY })
  }

  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      this.setState({
        assignment: {
          ...this.state.assignment,
          recruiter_offices:
            this.props.user.profile.offices.length > 0
              ? [this.props.user.profile.offices[0].id]
              : [],
          recruitment_processes: [],
          states: [],
          start_date: moment().format('YYYY-MM-DD'),
          end_date: moment().format('YYYY-MM-DD'),
          [name]: value
        }
      })
    }
  }

  createdAssignment() {
    this.setState({
      hasAssignment: true
    })
  }

  render() {
    const { visible, pageX, pageY } = this.state

    return (
      <DialogContainer
        id="simple-full-page-dialog"
        visible={visible}
        pageX={pageX}
        pageY={pageY}
        fullPage
        onHide={this.toggle}
        aria-labelledby="simple-full-page-dialog-title"
      >
        <Toolbar
          fixed
          colored
          title={this.props.assignment ? 'Redigera uppdrag' : 'Nytt uppdrag'}
          titleId="simple-full-page-dialog-title"
          nav={
            <Button icon onClick={this.toggle}>
              close
            </Button>
          }
        />
        <Container className="py-5">
          <section className="md-toolbar-relative">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === '1'
                  })}
                  onClick={() => {
                    this.toggleTab('1')
                  }}
                >
                  Uppdrag
                </NavLink>
              </NavItem>
              {this.state.hasAssignment && (
                <NavItem>
                  <NavLink
                    // disabled={!this.state.hasAssignment}
                    className={classnames({
                      active: this.state.activeTab === '2',
                      hasAd: this.props.assignment && this.props.assignment.ad,
                      noAd: this.props.assignment && !this.props.assignment.ad
                    })}
                    onClick={() => {
                      this.toggleTab('2')
                    }}
                  >
                    Annons
                  </NavLink>
                </NavItem>
              )}
            </Nav>

            <TabContent activeTab={this.state.activeTab} className="pt-5">
              <TabPane tabId="1">
                <AssignmentsForm
                  assignment={this.props.assignment}
                  unnested={this.props.unnested}
                  onCreatedAssignment={this.createdAssignment}
                />
              </TabPane>
              <TabPane tabId="2">
                {this.state.hasAssignment && (
                  <AdForm
                    assignment={
                      this.props.assignments.selectedAssignment &&
                      this.props.assignments.selectedAssignment
                    }
                  />
                )}
              </TabPane>
            </TabContent>
          </section>
        </Container>
      </DialogContainer>
    )
  }
}

export default connect(state => state)(AssignmentsFormNew)
