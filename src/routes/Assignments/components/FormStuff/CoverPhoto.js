import React from 'react'
import Dropzone from 'react-dropzone'
import Cropper from 'react-cropper'
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap'
import { apiClient } from '../../../../store/axios.config'

class CoverPhoto extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      files: [],
      hasCoverImage: !!props.ad,
      cropperModal: false,
      croppedImage: undefined,
      croppedUrl: undefined
    }

    this.onDrop = this.onDrop.bind(this)
    this.onError = this.onError.bind(this)
    this.onRemove = this.onRemove.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.croppedImage !== this.state.croppedImage) {
      this.props.callBack(this.state.croppedImage)
    }
  }

  onDrop(files) {
    this.setState(
      {
        files
      },
      this.toggleModal()
    )
  }

  toggleModal() {
    this.setState({
      cropperModal: !this.state.cropperModal
    })
  }

  onError() {
    this.setState({
      hasCoverImage: false
    })
  }

  onRemove() {
    let { hasCoverImage } = this.state
    let { ad } = this.props
    hasCoverImage &&
      apiClient.delete('recruiter/ads/' + ad.id + '/cover-image/')
    this.setState({ files: [] })
  }

  _crop() {
    let blob = dataURItoBlob(
      this.refs.cropper
        .getCroppedCanvas({
          width: 1000,
          imageSmoothingEnabled: true,
          imageSmoothingQuality: 'high'
        })
        .toDataURL()
    )
    this.setState({
      croppedImage: blob,
      croppedUrl: this.refs.cropper.getCroppedCanvas().toDataURL(),
      cropperModal: false
    })
  }

  render() {
    let { files, croppedUrl } = this.state

    return (
      <div>
        <Dropzone
          accept="image/*"
          className="dropZone coverPhoto"
          onDrop={this.onDrop}
          disabled={files.length > 0}
        >
          {files.length > 0 && (
            <div
              className="preview"
              style={{
                backgroundImage: 'url("' + croppedUrl + '")'
              }}
            >
              <i className="fa fa-times remove" onClick={this.onRemove} />
            </div>
          )}
          {this.props.ad.id &&
            files.length === 0 && (
              <div
                className="preview"
                style={{
                  backgroundImage:
                    'url("' +
                    'https://assignments.workandpassion.bid/api/v1/ads/' +
                    this.props.ad.id +
                    '/cover-image/' +
                    '")'
                }}
              />
            )}
        </Dropzone>
        <Modal isOpen={this.state.cropperModal} toggle={this.toggleModal}>
          <ModalBody className="pa-0">
            <Cropper
              ref="cropper"
              src={files.length === 1 ? files[0].preview : ''}
              style={{ height: 400, width: '100%' }}
              // Cropper.js options
              aspectRatio={16 / 7}
              guides={false}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this._crop()}>
              Klar
            </Button>{' '}
            <Button color="secondary" onClick={this.toggleModal}>
              Avbryt
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

export default CoverPhoto

function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  let byteString
  if (dataURI.split(',')[0].indexOf('base64') >= 0) {
    byteString = atob(dataURI.split(',')[1])
  } else {
    byteString = unescape(dataURI.split(',')[1])
  }

  // separate out the mime component
  let mimeString = dataURI
    .split(',')[0]
    .split(':')[1]
    .split(';')[0]

  // write the bytes of the string to a typed array
  let ia = new Uint8Array(byteString.length)
  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i)
  }

  return new Blob([ia], { type: mimeString })
}
