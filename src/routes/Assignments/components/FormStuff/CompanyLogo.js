import React from 'react'
import Dropzone from 'react-dropzone'
import { apiClient } from '../../../../store/axios.config'

class CompanyLogo extends React.Component {
  constructor(props) {
    super(props)
    this.state = { files: [], hasLogo: true }

    this.onDrop = this.onDrop.bind(this)
    this.onError = this.onError.bind(this)
    this.onRemove = this.onRemove.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.files.length !== this.state.files.length) {
      this.props.callBack(this.state.files[0])
    }
  }

  onDrop(files) {
    this.setState({
      files
    })
  }

  onError() {
    this.setState({
      hasLogo: false
    })
  }

  onRemove() {
    let { hasLogo } = this.state
    let { ad } = this.props
    hasLogo && apiClient.delete('recruiter/ads/' + ad.id + '/logo/')
    this.setState({ files: [] })
  }

  render() {
    let { files } = this.state

    return (
      <Dropzone
        accept="image/*"
        className="dropZone companyLogo"
        onDrop={this.onDrop}
        disabled={files.length > 0}
        maxSize={1000000}
      >
        {files.length > 0 && (
          <div
            className="preview"
            style={{ backgroundImage: 'url("' + files[0].preview + '")' }}
          >
            <i className="fa fa-times remove" onClick={this.onRemove} />
          </div>
        )}
        {this.props.ad.id &&
          files.length === 0 && (
            <div
              className="preview"
              style={{
                backgroundImage:
                  'url("' +
                  'https://assignments.workandpassion.bid/api/v1/ads/' +
                  this.props.ad.id +
                  '/logo/' +
                  '")'
              }}
            />
          )}
        <small>
          {'https://assignments.workandpassion.bid/api/v1/ads/' +
            this.props.ad.id +
            '/logo/'}
        </small>
      </Dropzone>
    )
  }
}

export default CompanyLogo
