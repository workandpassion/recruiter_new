import React from 'react'
import ReactTable from 'react-table'
import { apiClient } from '../../../store/axios.config'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  deleteAssignment,
  getAssignments,
  setAssignment,
  deleteAd
} from '../../../store/actions/assignments'
import { getAllCustomers, getCustomer } from '../../../store/actions/customers'

class AssignmentsTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      assignmentStateValues: []
    }

    this.getAssignmentStateValues = this.getAssignmentStateValues.bind(this)
    this.clickHandler = this.clickHandler.bind(this)
    this.deleteAssignment = this.deleteAssignment.bind(this)
  }

  componentDidMount() {
    this.getAssignmentStateValues()
  }

  getPageSize(processesLength) {
    if (processesLength < 6) {
      return 5
    } else if (processesLength > 5 && processesLength < 11) {
      return 10
    } else if (processesLength > 10 && processesLength < 21) {
      return 20
    } else if (processesLength > 20) {
      return 25
    }
  }

  getTypeString(inputString) {
    switch (inputString) {
      case 'hire':
        return 'Uthyrning'
      case 'recruite':
        return 'Rekrytering'
      case 'hire-purchase':
        return 'Hyrköp'
      default:
        return 'N/A'
    }
  }

  getAssignmentStateValues() {
    apiClient.get('/recruiter/assignments-state-values').then(result => {
      this.setState({
        assignmentStateValues: result.data
      })
    })
  }

  clickHandler(assignment) {
    let { dispatch } = this.props
    dispatch(setAssignment(assignment)).then(() => {
      this.props.history.push(`/assignments/${assignment.id}`)
    })
    // this.props.history.push(`/assignments/${assignment.id}`)
  }

  deleteAssignment(assignment) {
    let { dispatch } = this.props
    if (
      window.confirm(
        'Är du säker på att du vill ta bort det här uppdraget?'
      ) === true
    ) {
      let ad = assignment.ad
      dispatch(deleteAssignment(assignment)).then(() => {
        dispatch(deleteAd(ad))
        dispatch(getCustomer(assignment.customer.id))
        dispatch(getAllCustomers())
        dispatch(getAssignments())
      })
    }
  }

  render() {
    return (
      <ReactTable
        data={this.props.assignmentsList}
        pageSize={this.getPageSize(this.props.assignmentsList.length)}
        className="-highlight"
        filterable
        previousText="Föregående"
        nextText="Nästa"
        loadingText="Hämtar uppdrag..."
        noDataText="Inga uppdrag"
        pageText="Sida"
        ofText="av"
        rowsText="rader"
        loading={this.props.loading}
        defaultSorted={[
          {
            id: 'start_date',
            desc: true
          }
        ]}
        getTdProps={(state, rowInfo, column, instance) => {
          return {
            onClick: (e, handleOriginal) => {
              if (column.className === 'delete') {
                this.deleteAssignment(rowInfo.row._original)
              }
              if (column.id && rowInfo) {
                // this.props.clickHandler(rowInfo.row._original)
                this.clickHandler(rowInfo.row._original)
              }
            }
          }
        }}
        columns={[
          {
            Header: 'Kund',
            accessor: 'customer.name'
          },
          {
            Header: 'Name',
            accessor: 'subject' // String-based value accessors!
          },
          {
            Header: 'Startdatum',
            accessor: 'start_date'
          },
          {
            Header: 'Typ',
            accessor: 'employment_type',
            Cell: row => this.getTypeString(row.value),
            filterMethod: (filter, row) => {
              if (filter.value === '') {
                return true
              }
              return row[filter.id] === filter.value
            },
            Filter: ({ filter, onChange }) => (
              <select
                onChange={event => onChange(event.target.value)}
                style={{ width: '100%' }}
                value={filter ? filter.value : ''}
              >
                <option value="" />
                <option value="hire">Uthyrning</option>
                <option value="recruite">Rekrytering</option>
                <option value="hire-purchase">Hyrköp</option>
              </select>
            )
          },
          {
            id: 'state',
            Header: 'Status',
            accessor: d =>
              d.states[d.states.length - 1]
                ? d.states[d.states.length - 1].value.name
                : 'N/A',
            Filter: ({ filter, onChange }) => (
              <select
                onChange={event => onChange(event.target.value)}
                style={{ width: '100%' }}
                value={filter ? filter.value : ''}
              >
                <option value="" />
                {this.state.assignmentStateValues.map(state => {
                  return (
                    <option key={state.id} value={state.name}>
                      {state.name}
                    </option>
                  )
                })}
              </select>
            )
          },
          {
            Header: 'Rekryterare',
            accessor: 'recruiters',
            Cell: row => {
              let recruiters = []
              row.value.map(recruiter => {
                recruiters.push(
                  recruiter.first_name + ' ' + recruiter.last_name
                )
              })
              return recruiters.length > 1
                ? recruiters[0] + ' +' + (recruiters.length - 1)
                : recruiters[0]
            }
          },
          {
            Header: <i className="fa fa-users" />,
            accessor: 'recruitment_processes',
            filterable: false,
            maxWidth: 40,
            Cell: row => {
              return row.value.length
            }
          },
          {
            Header: <i className="fa fa-thermometer-0" />,
            accessor: 'difficulty',
            filterable: false,
            maxWidth: 40,
            Cell: row => {
              switch (row.value) {
                case 1:
                  return <i className="fa fa-thermometer-1 difficultyValue" />
                case 2:
                  return <i className="fa fa-thermometer-2 difficultyValue" />
                case 3:
                  return <i className="fa fa-thermometer-3 difficultyValue" />
                case 4:
                  return <i className="fa fa-thermometer-4 difficultyValue" />
                default:
                  return <i className="fa fa-thermometer-0 difficultyValue" />
              }
            }
          },
          {
            Header: <i className="fa fa-user" />,
            accessor: 'mine',
            filterable: false,
            maxWidth: this.props.showMineColumn ? 40 : 0,
            Cell: row =>
              row.value === true && (
                <i className="fa fa-user d-flex justify-content-center align-items-center fg-green" />
              )
          },
          {
            Header: '',
            filterable: false,
            className: 'delete',
            maxWidth: 30,
            Cell: row => <i className="fa fa-minus-circle fg-red tableDelete" />
          }
        ]}
      />
    )
  }
}

export default withRouter(connect(state => state)(AssignmentsTable))
