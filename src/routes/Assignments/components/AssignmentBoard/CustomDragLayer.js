import React from 'react'
import { DragLayer } from 'react-dnd'
import CardDragPreview from './CardDragPreview'
import $ from 'jquery'

const layerStyles = {
  position: 'fixed',
  pointerEvents: 'none',
  zIndex: 100,
  left: 0,
  top: 0,
  width: '100%',
  height: '100%'
}

function getItemStyles(props) {
  const { initialOffset, currentOffset } = props
  if (!initialOffset || !currentOffset) {
    return {
      display: 'none'
    }
  }

  let { x, y } = currentOffset

  // if (props.snapToGrid) {
  //   x -= initialOffset.x
  //   y -= initialOffset.y
  //   ;[x, y] = snapToGrid(x, y)
  //   x += initialOffset.x
  //   y += initialOffset.y
  // }

  const transform = `translate(${x}px, ${y}px)`
  return {
    transform,
    WebkitTransform: transform
  }
}

/* global jQuery */
class CustomDragLayer extends React.Component {
  render() {
    const { process, isDragging, currentOffset } = this.props

    if (!isDragging) {
      return null
    }
    const $boardwrapper = $('.boardwrapper')
    const $nextElement = $('.statelist--container.mouseIsOver').next()
    const $prevElement = $('.statelist--container.mouseIsOver').prev()
    const $thisElement = $('.statelist--container.mouseIsOver')

    if (isDragging && $thisElement[0]) {
      // console.log(isElementInViewport($nextElement))
      if (!isElementInViewport($thisElement)) {
        // $boardwrapper.scrollLeft(1200)
        scrollParentToChild($boardwrapper, $thisElement)
      }
    }

    return (
      <div style={layerStyles} className="test">
        <div style={getItemStyles(this.props)}>
          <CardDragPreview {...process} />
        </div>
      </div>
    )
  }
}

export default DragLayer(monitor => ({
  process: monitor.getItem(),
  isDragging: monitor.isDragging(),
  initialOffset: monitor.getInitialSourceClientOffset(),
  currentOffset: monitor.getSourceClientOffset()
}))(CustomDragLayer)

function isElementInViewport(el) {
  //special bonus for those using jQuery
  if (typeof jQuery === 'function' && el instanceof jQuery) {
    el = el[0]
  }

  let rect = el.getBoundingClientRect()

  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight ||
        document.documentElement.clientHeight) /*or $(window).height() */ &&
    rect.right <=
      (window.innerWidth ||
        document.documentElement.clientWidth) /*or $(window).width() */
  )
}

function scrollParentToChild(parent, child) {
  if (typeof jQuery === 'function' && parent instanceof jQuery) {
    parent = parent[0]
    child = child[0]
  }
  // Where is the parent on page
  let parentRect = parent.getBoundingClientRect()
  // What can you see?
  let parentViewableArea = {
    height: parent.clientHeight,
    width: parent.clientWidth
  }

  // Where is the child
  let childRect = child.getBoundingClientRect()
  // Is the child viewable?
  // let isViewable =
  //   childRect.top >= parentRect.top &&
  //   childRect.top <= parentRect.top + parentViewableArea.height

  parent.scrollLeft = childRect.left + parent.scrollLeft - parentRect.left
  let scrollPos = childRect.left + parent.scrollLeft - parentRect.left

  // $(parent).animate({ scrollLeft: scrollPos }, 500)
  // if you can't see the child try to scroll parent
  // if (!isViewable) {
  //   // scroll by offset relative to parent
  //   // parent.scrollTop = childRect.top + parent.scrollTop - parentRect.top
  // }
}
