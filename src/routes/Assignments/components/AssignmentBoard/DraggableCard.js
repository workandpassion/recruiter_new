import React from 'react'
import { DragSource } from 'react-dnd'
import { getEmptyImage } from 'react-dnd-html5-backend'
import CandidateCard from './CandidateCard'

const itemSource = {
  beginDrag(props, monitor, component) {
    let { process } = props

    return { process }
  },
  endDrag(props, monitor, component) {
    let { process } = props

    return { process }
  }
}

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

class DraggableCard extends React.Component {
  componentDidMount() {
    this.props.connectDragPreview(getEmptyImage(), {
      // IE fallback: specify that we'd rather screenshot the node
      // when it already knows it's being dragged so we can hide it with CSS.
      captureDraggingState: true
    })
  }

  render() {
    const { connectDragSource, isDragging, process } = this.props

    return connectDragSource(
      <div
        style={{
          opacity: isDragging ? 0.5 : 1
        }}
      >
        <CandidateCard
          process={process}
          movingCard={this.props.movingCard}
          checkCallback={this.props.checkCallback}
        />
      </div>
    )
  }
}

export default DragSource('CANDIDATE', itemSource, collect)(DraggableCard)
