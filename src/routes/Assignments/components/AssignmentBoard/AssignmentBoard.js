import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { setupBoard, moveCard } from '../../../../store/actions/assignmentBoard'
import { Container, Row, Col } from 'reactstrap'
import {
  DialogContainer,
  List,
  ListItem,
  TextField,
  MenuButton
} from 'react-md'
import StateList from './StateList'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import CustomDragLayer from './CustomDragLayer'

class AssignmentBoard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      testDialogVisible: false,
      dialogVisible: true
    }

    let { dispatch } = this.props
    dispatch(setupBoard())

    this.handleCandidateMove = this.handleCandidateMove.bind(this)
    this.toggleTestDialog = this.toggleTestDialog.bind(this)
    this.toggleDialog = this.toggleDialog.bind(this)
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.assignments.fetchingAssignment &&
      !this.props.assignments.fetchingAssignment
    ) {
      let { dispatch } = this.props
      dispatch(setupBoard())
    }
  }

  handleCandidateMove(assignmentId, process, processStateValues) {
    let { dispatch } = this.props

    dispatch(moveCard(process, processStateValues))
  }

  toggleTestDialog() {
    this.setState({
      testDialogVisible: !this.state.testDialogVisible
    })
  }

  toggleDialog() {
    this.setState({
      dialogVisible: !this.state.dialogVisible
    })
  }

  handleKeyDown = (e, type) => {
    const key = e.which || e.keyCode
    if (key === 13 || key === 32) {
      // also close on enter or space keys
      if (type === 'testdialog') {
        this.toggleTestDialog()
      } else {
        this.toggleDialog()
      }
    }
  }

  render() {
    let { testDialogVisible, dialogVisible } = this.state
    let { lists } = this.props.assignmentBoard
    let aborted = lists && lists[lists.length - 1]

    return (
      <div>
        <CustomDragLayer />
        <Container fluid className="boardwrapper my-3">
          <Row>
            <Col xs={12} className="d-flex">
              {lists &&
                lists.map((list, index) => {
                  if (index < lists.length - 1) {
                    return (
                      <StateList
                        key={list.id}
                        id={list.id}
                        name={list.name}
                        items={list.items}
                        handleCandidateMove={this.handleCandidateMove}
                        processStateValues={list.processStateValues}
                      />
                    )
                  }
                })}
            </Col>
            {aborted && (
              <Col xs={12} className="d-flex">
                <StateList
                  key={aborted.id}
                  id={aborted.id}
                  name={aborted.name}
                  items={aborted.items}
                  handleCandidateMove={this.handleCandidateMove}
                  processStateValues={aborted.processStateValues}
                />
              </Col>
            )}
          </Row>
        </Container>

        <DialogContainer
          id="test-dialog"
          visible={testDialogVisible}
          title="Typ av test"
          onHide={this.toggleTestDialog}
        >
          <List
            onClick={this.toggleTestDialog}
            onKeyDown={e => this.handleKeyDown(e, 'testdialog')}
          >
            <ListItem primaryText="TalentQ" />
            <ListItem primaryText="Assesio" />
          </List>
        </DialogContainer>
      </div>
    )
  }
}

export default withRouter(
  connect(state => state)(DragDropContext(HTML5Backend)(AssignmentBoard))
)
