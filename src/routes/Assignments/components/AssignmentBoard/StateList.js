import React from 'react'
import { connect } from 'react-redux'
import DraggableCard from './DraggableCard'
import classnames from 'classnames'
import { DropTarget } from 'react-dnd'
import _ from 'lodash'
import { Badge } from 'react-md'
import { Input } from 'reactstrap'
import { checkCard } from '../../../../store/actions/assignmentBoard'

const listTarget = {
  drop(props, monitor) {
    const delta = monitor.getDifferenceFromInitialOffset()
    const item = monitor.getItem()

    let { assignments, processStateValues } = props

    let assignmentId = assignments.selectedAssignment.id
    let process = monitor.getItem().process

    props.handleCandidateMove(assignmentId, process, processStateValues)
  }
}

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  }
}

class StateList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      checked: false
    }

    this.checkAllCards = this.checkAllCards.bind(this)
    this.cardInListChecked = this.cardInListChecked.bind(this)
  }

  checkAllCards() {
    const { dispatch, items } = this.props
    const { checked } = this.state

    for (let i = 0; i < items.length; i++) {
      !checked && !items[i].checked && dispatch(checkCard(items[i]))
      checked && items[i].checked && dispatch(checkCard(items[i]))
    }

    this.setState({
      checked: !this.state.checked
    })
  }

  cardInListChecked() {
    const { items } = this.props
    const { checked } = this.state

    // Uncheck if a card is unchecked
    checked &&
      _.find(items, { checked: false }) &&
      this.setState({
        checked: false
      })

    // Check if all children is checked
    let checkedArr = _.filter(items, item => {
      return item.checked
    })
    !checked &&
      checkedArr.length === items.length &&
      this.setState({
        checked: true
      })
  }

  render() {
    let { items, name, processStateValues } = this.props
    const { connectDropTarget, isOver } = this.props
    const { checked } = this.state

    let stateListClass = classnames(
      'statelist--container',
      items && items.length > 0 && 'hasItems',
      isOver && 'md-paper md-paper--2 mouseIsOver',
      name === 'Inkommande' && 'incoming',
      name === 'Avbruten' && 'aborted',
      _.startsWith(name, 'Sign') && 'signed'
    )

    return connectDropTarget(
      <div
        className={stateListClass}
        // style={{ border: `1px solid ${processStateValues[0].color}` }}
      >
        <Badge
          id="notification-badge-toggle"
          style={{ width: '100%', height: 30 }}
          secondary
          aria-haspopup
          badgeId="count-badge"
          badgeContent={items.length}
          invisibleOnZero
        >
          <div
            className="statelist--title"
            style={{ background: processStateValues[0].color }}
          >
            {name}
            {items.length > 0 && (
              <Input
                type="checkbox"
                className="check-all"
                checked={checked}
                onClick={this.checkAllCards}
              />
            )}
          </div>
        </Badge>

        {items &&
          items.length > 0 &&
          items[0] &&
          items.map((item, index) => {
            return (
              <DraggableCard
                key={item.id}
                process={item}
                movingCard={this.props.assignmentBoard.movingProcess}
                checkCallback={this.cardInListChecked}
              />
            )
          })}
      </div>
    )
  }
}

export default connect(state => state)(
  DropTarget('CANDIDATE', listTarget, collect)(StateList)
)
