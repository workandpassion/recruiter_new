import React, { PropTypes } from 'react'
import CandidateCard from './CandidateCard'

const styles = {
  display: 'inline-block',
  width: 200,
  transform: 'rotate(-7deg)',
  WebkitTransform: 'rotate(-7deg)'
}

// const propTypes = {
//   process: PropTypes.object
// }

const CardDragPreview = props => {
  // console.log(props)

  return (
    <div style={styles}>
      <CandidateCard process={props.process} />
    </div>
  )
}

// CardDragPreview.propTypes = propTypes

export default CardDragPreview
