import React from 'react'
import { connect } from 'react-redux'
import {
  Card,
  CardTitle,
  CardText,
  CardActions,
  FontIcon,
  Avatar,
  LinearProgress,
  Collapse,
  DialogContainer,
  Button,
  injectInk,
  TextField,
  Toolbar
} from 'react-md'
import { Popover, PopoverBody, Row, Col } from 'reactstrap'
import _ from 'lodash'
import moment from 'moment'
import classNames from 'classnames'
import ProcessStateForm from '../ProcessStateForm'
import { updateCandidateProcessState } from '../../../../store/actions/assignments'
import CandidateProfile from '../../../Candidates/CandidateProfile/CandidateProfile'
import { AvForm, AvField } from 'availity-reactstrap-validation'
import { checkCard, moveCard } from '../../../../store/actions/assignmentBoard'
import { apiClient } from '../../../../store/axios.config'

class CandidateCard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      datepickerVisible: false,
      value: undefined,
      imageUrl: `https://api.wapcard.se/api/v1/profiles/${props.process.user &&
        props.process.user.id}/picture/500?undefined`,
      dateHidden: false,
      popoverOpen: false,
      candidateDialogVisible: false,
      candidateProfileVisible: false,
      pageX: null,
      pageY: null,
      fetchingMails: false,
      sentMails: []
    }

    this.toggleStateForm = this.toggleStateForm.bind(this)
    this.togglePopover = this.togglePopover.bind(this)
    this.toggleCandidateDialog = this.toggleCandidateDialog.bind(this)
    this.toggleProfileDialog = this.toggleProfileDialog.bind(this)
    this.onUpdatedState = this.onUpdatedState.bind(this)
    this.handleCandidateMove = this.handleCandidateMove.bind(this)
    this.handleCheck = this.handleCheck.bind(this)
  }

  componentWillMount() {
    this.actions = [
      {
        label: 'Ok',
        primary: true,
        onClick: this.toggleCandidateDialog
      }
    ]
  }

  componentDidMount() {
    let { process } = this.props
    let currentState = process.states.slice(-1)[0]
    this.setState({
      status:
        currentState && currentState.completed_at
          ? 'completed_at'
          : currentState && currentState.scheduled_at && 'scheduled_at',
      dateHidden:
        _.startsWith(currentState.value.name, 'Inkommen') ||
        _.startsWith(currentState.value.name, 'Avbruten'),
      fetchingMails: true
    })

    apiClient
      .get(
        'recruiter/assignments/' +
          this.props.assignments.selectedAssignment.id +
          '/recruitment-processes/' +
          process.id +
          '/emails/'
      )
      .then(result => {
        this.setState({
          sentMails: result.data,
          fetchingMails: false
        })
      })
      .catch(error => {
        this.setState({
          fetchingMails: false
        })
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.process !== this.props.process) {
      console.log(this.props)
      let { process } = this.props
      let currentState = process.states.slice(-1)[0]
      this.setState({
        status:
          currentState && currentState.completed_at
            ? 'completed_at'
            : currentState && currentState.scheduled_at && 'scheduled_at',
        dateHidden:
          _.startsWith(currentState.value.name, 'Inkommen') ||
          _.startsWith(currentState.value.name, 'Avbruten')
      })
    }
  }

  toggleStateForm() {
    this.setState({
      showStateForm: !this.state.showStateForm
    })
  }

  togglePopover() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    })
  }

  toggleCandidateDialog() {
    this.setState({
      candidateDialogVisible: !this.state.candidateDialogVisible
    })
  }

  toggleProfileDialog(e) {
    let { pageX, pageY } = e
    if (e.changedTouches) {
      pageX = e.changedTouches[0].pageX
      pageY = e.changedTouches[0].pageY
    }

    this.setState({
      candidateProfileVisible: !this.state.candidateProfileVisible,
      pageX,
      pageY
    })
  }

  onUpdatedState(state) {
    let { dispatch, assignments, process } = this.props
    let { selectedAssignment } = assignments
    dispatch(updateCandidateProcessState(selectedAssignment.id, process, state))
  }

  handleCandidateMove(event, values) {
    let { dispatch, process } = this.props
    const { lists } = this.props.assignmentBoard

    let moveTo = [_.find(lists, { id: values.newProcessState })]

    dispatch(moveCard(process, moveTo))
  }

  handleCheck() {
    const { dispatch, process } = this.props
    dispatch(checkCard(process))
    this.props.checkCallback()
  }

  render() {
    const { lists } = this.props.assignmentBoard
    const InkedListItem = injectInk(ProcessListItem)
    let { process } = this.props
    let {
      status,
      dateHidden,
      candidateDialogVisible,
      candidateProfileVisible,
      pageX,
      pageY
    } = this.state
    let currentState = process.states.slice(-1)[0]

    let cardClass = classNames(
      'candidatecard mb-2',
      status && status,
      currentState.comment && 'hasComment',
      process.checked && 'checked'
    )

    let isThisCardMoving = this.props.movingCard
      ? this.props.movingCard === process.id && true
      : false

    return (
      <Card className={cardClass}>
        {isThisCardMoving && (
          <LinearProgress id={process.id} progress={undefined} />
        )}
        <CardTitle
          title={
            <div onClick={this.toggleCandidateDialog}>
              {process.user.first_name + ' ' + process.user.last_name}
            </div>
          }
          subtitle={
            process.states.length > 0 &&
            _.find(process.states, function(state) {
              return _.startsWith(state.value.name, 'Inkommen')
            }).value.name
          }
          avatar={
            <Avatar role="presentation" onClick={this.handleCheck}>
              <i className="fa fa-check checked" />
              {this.state.imageUrl ? (
                <img
                  src={this.state.imageUrl}
                  style={{ width: '100%', height: 'auto' }}
                  onError={() => this.setState({ imageUrl: undefined })}
                />
              ) : (
                process.user.first_name.charAt(0)
              )}
            </Avatar>
          }
        />
        <Collapse collapsed={isThisCardMoving}>
          <CardActions
            onClick={this.toggleStateForm}
            className="justify-content-around"
          >
            <div className="d-flex align-items-center md-cell--3">
              <FontIcon>swap_horiz</FontIcon>
              <small>{moment().diff(moment(currentState.date), 'days')}d</small>
            </div>

            <div className="d-flex align-items-center justify-content-center md-cell--6">
              {!dateHidden && (
                <div className="d-flex align-items-center justify-content-center">
                  <FontIcon className="status">
                    {status
                      ? status === 'completed_at' ? 'check_circle' : 'today'
                      : 'today'}
                  </FontIcon>

                  <small>
                    {status
                      ? status === 'completed_at'
                        ? moment(currentState.completed_at).format('YYMMDD')
                        : moment(currentState.scheduled_at).format('YYMMDD')
                      : 'Datum'}
                  </small>
                </div>
              )}
            </div>

            <div className="d-flex align-items justify-content-end md-cell--3">
              <FontIcon
                className="comments"
                id={'Popover-' + process.id}
                onMouseOver={this.togglePopover}
                onMouseOut={this.togglePopover}
              >
                mode_comment
              </FontIcon>
            </div>

            {this.state.showStateForm && (
              <ProcessStateForm
                process={process}
                processState={currentState}
                show={this.state.showStateForm}
                toggle={this.toggleStateForm}
                dateHidden={dateHidden}
              />
            )}
          </CardActions>
        </Collapse>
        {currentState.comment && (
          <Popover
            placement="bottom"
            isOpen={this.state.popoverOpen}
            target={'Popover-' + process.id}
            toggle={this.togglePopover}
          >
            <PopoverBody>{currentState.comment}</PopoverBody>
          </Popover>
        )}

        <DialogContainer
          id="candidate-process-dialog"
          aria-describedby="candidate-process-dialog"
          title={
            <div>
              {' '}
              {process.user.first_name + ' ' + process.user.last_name}{' '}
              <i
                className="fa fa-eye show-candidate"
                onClick={this.toggleProfileDialog}
              />
            </div>
          }
          visible={candidateDialogVisible}
          onHide={this.toggleCandidateDialog}
          actions={this.actions}
          width={800}
        >
          <div>
            {process.states.map(state => (
              <InkedListItem
                key={state.id}
                state={state}
                updateState={this.onUpdatedState}
              />
            ))}
          </div>

          <hr />
          <div className="row">
            <div className="col">
              <AvForm onValidSubmit={this.handleCandidateMove}>
                <AvField
                  type="select"
                  name="newProcessState"
                  label="Flytta till"
                  style={{ width: 'auto' }}
                  defaultValue={
                    process.states[process.states.length - 1].value.id
                  }
                >
                  {lists &&
                    lists.map(listoption => (
                      <option value={listoption.id}>{listoption.name}</option>
                    ))}
                </AvField>
                <Button type="submit" flat swapTheming secondary>
                  Flytta
                </Button>
              </AvForm>
            </div>
          </div>
          <hr />
          <Row>
            <Col xs={12}>
              <h6>Skickade mail</h6>
            </Col>
            {this.state.sentMails &&
              this.state.sentMails.map(mail => (
                <Col xs={12} key={mail.id}>
                  <Row>
                    <small className="col">
                      {moment(mail.created_at).format('YYYY-MM-DD HH:mm')}
                    </small>
                  </Row>
                  <span>
                    {mail.event} {mail.status} {mail.reject_reason}
                  </span>
                </Col>
              ))}
          </Row>
        </DialogContainer>

        <DialogContainer
          id="simple-full-page-dialog"
          visible={candidateProfileVisible}
          pageX={pageX}
          pageY={pageY}
          fullPage
          onHide={this.toggleProfileDialog}
          aria-labelledby="simple-full-page-dialog-title"
        >
          <Toolbar
            fixed
            colored
            title={process.user.first_name + ' ' + process.user.last_name}
            titleId="simple-full-page-dialog-title"
            nav={
              <Button icon onClick={this.toggleProfileDialog}>
                close
              </Button>
            }
          />
          <section className="md-toolbar-relative">
            <CandidateProfile userId={process.user.id} />
          </section>
        </DialogContainer>
      </Card>
    )
  }
}

export default connect(state => state)(CandidateCard)

class ProcessListItem extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      hideComment: true,
      mState: props.state
    }

    this.toggleComment = this.toggleComment.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  toggleComment() {
    this.setState({
      hideComment: !this.state.hideComment
    })
  }

  getSecondaryText() {
    let { state } = this.props
    let text = []

    if (state.scheduled_at) {
      text.push(
        <div
          key={'scheduled_at' + state.id}
          className="d-flex justify-content-center align-items-center"
        >
          <FontIcon className="scheduled_at">today</FontIcon>
          {moment(state.scheduled_at).format('DD MMM YYYY')}
        </div>
      )
    }

    if (state.completed_at) {
      text.push(
        <div
          key={'completed_at' + state.id}
          className="d-flex justify-content-center align-items-center ml-2"
        >
          <FontIcon className="completed_at">check_circle</FontIcon>
          {moment(state.completed_at).format('DD MMM YYYY')}
        </div>
      )
    }

    return text
  }

  handleInputChange(value, event) {
    console.log(event)
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        mState: {
          ...this.state.mState,
          [name]: value
        }
      })
    }
  }

  componentWillUnmount() {
    if (this.state.mState !== this.props.state) {
      this.props.updateState(this.state.mState)
    }
  }

  render() {
    let { state, ink } = this.props
    let { hideComment } = this.state
    return (
      <div className="process-list-item" onClick={this.toggleComment}>
        <div className="visible-wrapper">
          {ink}
          <Avatar style={{ background: state.value.color }}>
            {state.value.name.charAt(0)}
          </Avatar>
          <div className="process-list-item--content">
            <div className="text row">
              <span className="title col-12 mb-1">{state.value.name}</span>
              <span className="dates col-12 d-flex">
                {this.getSecondaryText()}
              </span>
            </div>
          </div>
        </div>
        <Collapse collapsed={hideComment}>
          <div className="my-2">{state.comment}</div>
        </Collapse>
      </div>
    )
  }
}
