import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import ReactTable from 'react-table'
import CandidateSubComponent from './CandidateSubComponent'
import { apiClient } from '../../../store/axios.config'
import _ from 'lodash'

class CandidatesTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      processes: Object.assign([], this.props.processes),
      processStateValues: [],
      expandedRows: {}
    }

    this.clickHandler = this.clickHandler.bind(this)
    this.getProcessStateValues = this.getProcessStateValues.bind(this)
  }

  componentDidMount() {
    this.getProcessStateValues()
  }

  clickHandler(row) {
    let { dispatch } = this.props
    this.props.history.push(`/candidates/${row.user.id}`)
  }

  getPageSize(processesLength) {
    if (processesLength < 6) {
      return 5
    } else if (processesLength > 5 && processesLength < 11) {
      return 10
    } else if (processesLength > 10 && processesLength < 21) {
      return 20
    } else if (processesLength > 20) {
      return 25
    }
  }

  getProcessStateValues() {
    apiClient
      .get('/recruiter/recruitment-processes-state-values')
      .then(result => {
        this.setState({
          processStateValues: result.data
        })
      })
  }

  render() {
    let { dispatch } = this.props

    return (
      <ReactTable
        expanded={this.state.expandedRows}
        data={this.props.processes}
        className="-highlight"
        filterable
        previousText="Föregående"
        nextText="Nästa"
        loadingText="Hämtar kandidater..."
        noDataText="Inga kandidater"
        pageText="Sida"
        ofText="av"
        rowsText="rader"
        // loading={this.props.loading}
        pageSize={this.getPageSize(this.props.processes.length)}
        defaultFilterMethod={(filter, row, column) => {
          const id = filter.pivotId || filter.id
          return row[id] !== undefined
            ? String(row[id])
                .toLowerCase()
                .startsWith(filter.value.toLowerCase())
            : true
        }}
        getTdProps={(state, rowInfo, column, instance) => {
          return {
            onClick: (e, handleOriginal) => {
              console.log('A Td Element was clicked!')
              console.log('it produced this event:', e)
              console.log('It was in this column:', column)
              console.log('It was in this row:', rowInfo)
              console.log('It was in this table instance:', instance)
              if (column.expander || rowInfo) {
                let mIndex = Number(rowInfo.index)
                let mExpandedRows = Object.assign({}, this.state.expandedRows)
                if (mIndex in mExpandedRows) {
                  delete mExpandedRows[mIndex]
                } else {
                  mExpandedRows[mIndex] = true
                }

                this.setState({
                  expandedRows: mExpandedRows
                })
              } else {
                handleOriginal()
              }
              // if (column.id && rowInfo) {
              //   this.clickHandler(rowInfo.row._original)
              // } else {
              //   handleOriginal()
              // }
            }
          }
        }}
        columns={[
          {
            Header: 'Förnamn',
            accessor: 'user.first_name'
          },
          {
            Header: 'Efternamn',
            accessor: 'user.last_name'
          },
          {
            Header: 'Status',
            id: 'state',
            accessor: s =>
              s.states.length > 0 ? s.states.slice(-1).pop().value.name : 'N/A',
            Filter: ({ filter, onChange }) => (
              <select
                onChange={event => onChange(event.target.value)}
                style={{ width: '100%' }}
                value={filter ? filter.value : ''}
              >
                <option value="" />
                {this.state.processStateValues.map(state => {
                  return (
                    <option key={state.id} value={state.name}>
                      {state.name}
                    </option>
                  )
                })}
              </select>
            )
          }
        ]}
        SubComponent={row => {
          let process = row.original
          return (
            <CandidateSubComponent
              process={process}
              candidate={process.user}
              selectedAssignment={this.props.assignments.selectedAssignment}
            />
          )
        }}
      />
    )
  }
}

export default withRouter(connect(state => state)(CandidatesTable))
