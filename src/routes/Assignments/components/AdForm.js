import React from 'react'
import { connect } from 'react-redux'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import { Editor } from 'react-draft-wysiwyg'
import {
  EditorState,
  convertToRaw,
  convertFromHTML,
  ContentState
} from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'
import { Row, Col, Label } from 'reactstrap'
import Datetime from 'react-datetime'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'
import _ from 'lodash'
import moment from 'moment'
import $ from 'jquery'
import {
  updateAssignment,
  updateAd,
  createAd
} from '../../../store/actions/assignments'
import axios from 'axios/index'
import CoverPhoto from './FormStuff/CoverPhoto'
import CompanyLogo from './FormStuff/CompanyLogo'
import Select from 'react-select'
import { apiClient } from '../../../store/axios.config'
import { Media } from 'react-md'

let CryptoJS = require('crypto-js')

class AdForm extends React.Component {
  constructor(props) {
    super(props)

    let editorState = this.getSampleMarkup(props)

    this.state = {
      changes: false,
      loadsave: false,
      loadBroadbeen: false,
      editorState: editorState ? editorState : EditorState.createEmpty(),
      ad: {
        id: props.assignment && props.assignment.ad && props.assignment.ad.id,
        ad_partners: [],
        title:
          props.assignment.ad && props.assignment.ad.title
            ? props.assignment.ad.title
            : props.assignment.subject +
              ' till ' +
              props.assignment.customer.name,
        body: htmlEncode(
          draftToHtml(convertToRaw(editorState.getCurrentContent()))
        ),
        job_type: props.assignment && props.assignment.employment_type,
        job_title: props.assignment && props.assignment.subject,
        employer_name: props.assignment && props.assignment.customer.name,
        skills: props.assignment && _.map(props.assignment.skills, 'id'),
        occupation:
          props.assignment &&
          props.assignment.occupations[0] &&
          props.assignment.occupations[0].id,
        county:
          props.assignment &&
          props.assignment.counties[0] &&
          props.assignment.counties[0].id,
        published_at: moment().format('YYYY-MM-DD'),
        published_until: props.assignment.ad
          ? props.assignment.ad.published_until
          : '',
        deadline: props.assignment.ad ? props.assignment.ad.deadline : '',
        video_url: props.assignment.ad ? props.assignment.ad.video_url : ''
      }
    }

    this.onEditorStateChange = this.onEditorStateChange.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValidSubmit = this.handleValidSubmit.bind(this)
    this.handleBroadbeen = this.handleBroadbeen.bind(this)
    this.setupXml = this.setupXml.bind(this)
    this.getCountyOptionsAsync = this.getCountyOptionsAsync.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.assignment && this.props.assignment) {
      this.setState({
        ad: {
          title:
            this.props.assignment &&
            this.props.assignment.subject +
              ' till ' +
              this.props.assignment.customer.name
        }
      })
    }

    if (prevState.logo !== this.state.logo) {
      this.setState({
        changes: true
      })
    }

    if (prevState.coverImage !== this.state.coverImage) {
      this.setState({
        changes: true
      })
    }
  }

  getSampleMarkup(props) {
    let recruiter_name =
      props.user.profile.first_name + ' ' + props.user.profile.last_name

    let sampleMarkup =
      props.assignment.ad &&
      props.assignment.ad.body &&
      htmlDecode(props.assignment.ad.body)

    if (!props.assignment.ad) {
      sampleMarkup =
        '<h5>Dina arbetsuppgifter</h5><p>Dina arbetsuppgifter...</p>'
      sampleMarkup += '<h5>Din profil</h5><p>Din profil...</p>'
      sampleMarkup +=
        '<h5>Sista ansökningsdag</h5>' +
        '<p>Skicka in din ansökan snarast då urval sker löpande. Vid eventuella frågor om tjänsten hänvisar vi till ' +
        recruiter_name +
        ' på tel. <a href="tel:' +
        props.user.profile.mobile_phone_number +
        '">' +
        props.user.profile.mobile_phone_number +
        '</a> alternativt <a href="mailto:' +
        props.user.profile.email +
        '">' +
        props.user.profile.email +
        '</a></p>'
      sampleMarkup += '<h5>Om kunden</h5><p>Om kunden</p>'
    }

    const blocksFromHtml = htmlToDraft(sampleMarkup)
    const { contentBlocks, entityMap } = blocksFromHtml
    const contentState = ContentState.createFromBlockArray(
      contentBlocks,
      entityMap
    )
    return EditorState.createWithContent(contentState)
  }

  onEditorStateChange(editorState) {
    this.setState({
      editorState,
      ad: Object.assign({}, this.state.ad, {
        body: htmlEncode(
          draftToHtml(convertToRaw(editorState.getCurrentContent()))
        )
      }),
      changes: true
    })
  }

  setupXml() {
    this.setState({
      loadBroadbeen: true
    })

    let { counties } = this.props.assignment

    let _self = this
    let { ad, cityValue } = this.state
    let xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        let xmlDoc = xhttp.responseXML //important to use responseXML here
        xmlDoc
          .getElementsByTagName('JobTitle')[0]
          .appendChild(xmlDoc.createTextNode(ad.job_title))
        xmlDoc
          .getElementsByTagName('JobReference')[0]
          .appendChild(xmlDoc.createTextNode(ad.id))
        xmlDoc
          .getElementsByTagName('JobType')[0]
          .appendChild(xmlDoc.createTextNode(ad.job_type))
        // TODO get city and county
        xmlDoc
          .getElementsByTagName('Location_Query')[0]
          .appendChild(
            xmlDoc.createTextNode(
              cityValue.label + ',' + counties[0].name + ', Sweden'
            )
          )
        xmlDoc
          .getElementsByTagName('Industry')[0]
          .appendChild(xmlDoc.createTextNode(ad.occupation))
        xmlDoc
          .getElementsByTagName('JobDescription')[0]
          .appendChild(xmlDoc.createTextNode(htmlDecode(ad.body)))
        xmlDoc
          .getElementsByName('jobId')[0]
          .appendChild(xmlDoc.createTextNode(ad.id))
        console.log(xmlDoc)

        let wordArray = CryptoJS.enc.Utf8.parse(
          new XMLSerializer().serializeToString(xmlDoc)
        )
        let base64 = CryptoJS.enc.Base64.stringify(wordArray)

        _self.handleBroadbeen(base64)
      }
    }
    xhttp.open('GET', process.env.PUBLIC_URL + '/broadbeen_template.xml', true)
    xhttp.send()
  }

  getCountyOptionsAsync(input, callback) {
    let mCountiesUrls = []
    let mCities = []

    let { counties, ad } = this.props.assignment

    counties.length &&
      counties.map(county => {
        mCountiesUrls.push('locations/' + county.id)
      })

    Promise.all(
      mCountiesUrls.map(url =>
        apiClient.get(url).then(resp => resp.data.municipalities)
      )
    )
      .then(citiesArray => {
        citiesArray.map(cityArray => {
          cityArray.map(city => {
            mCities.push(
              Object.assign({}, city, {
                value: city.id,
                label: city.name
              })
            )

            if (ad && city.id === ad.city) {
              this.setState({
                cityValue: Object.assign({}, city, {
                  value: city.id,
                  label: city.name
                })
              })
            }
          })
        })
      })
      .then(() => {
        callback(null, {
          options: mCities,
          complete: false
        })
      })
  }

  handleDateChange(date, name) {
    this.setState({
      ad: {
        ...this.state.ad,
        [name]:
          name === 'deadline'
            ? moment(date).format('YYYY-MM-DD')
            : moment(date).format('YYYY-MM-DD HH:mm')
      },
      changes: true
    })
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      let value = target.type === 'checkbox' ? target.checked : target.value
      let name = target.name

      if (name === 'video_url') {
        const myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i
        let videoID = value.match(myregexp)
        if (videoID) {
          value = videoID[1]
        }
      }

      this.setState({
        ad: {
          ...this.state.ad,
          [name]: value
        },
        changes: true
      })
    }
  }

  handleValidSubmit(event, values) {
    this.setState({
      loadsave: true
    })

    let { dispatch } = this.props

    let { ad, logo, coverImage } = this.state

    let submitFn = this.props.assignment.ad ? updateAd : createAd

    dispatch(submitFn(ad, logo, coverImage))
      .then(result => {
        this.roastAd(result.ad)

        this.setState({ changes: false })
        let mAssignment = Object.assign(
          {},
          this.props.assignments.unnestedSelectedAssignment,
          {
            ad: result.ad.id
          }
        )

        dispatch(updateAssignment(mAssignment)).then(() => {
          this.setState({
            loadsave: false
          })
        })
      })
      .catch(error => {
        this.setState({
          loadsave: false
        })
      })
  }

  roastAd(ad) {
    let mTitle = ad.title
      .replace(/ /g, '-')
      .replace(/å|ä/g, 'a')
      .replace(/ö/g, 'o')
      .toLowerCase()

    let path = '/jobs/' + ad.id + '/' + mTitle

    axios.put(
      'https://api.roast.io/api/v1/sites/856c867b-2082-4ac7-95c7-57e0d6fbb12d/ssr',
      {
        paths: ['/jobs', path]
      },
      {
        headers: {
          Authorization:
            'Bearer ' +
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhdXRoMHw1YTAyYjk1MmI4NGFjYzQ2M2M2ZjI2NzEiLCJhdWQiOiJUVG9jdGdHZGoxT1d5eVI5bDNwOXBKd0U0Rm5xRzZWMyIsImlhdCI6MTUxNzgzNDI1MCwiZXhwIjoxODMzNDEwMjUwfQ.A62800qFCtmCOeCrZmoXKUvuq7w2FoiCC_ZHaDurVL0'
        }
      }
    )
  }

  handleBroadbeen(vacancy_details) {
    let transaction = {
      transaction: {
        filters: {
          custom_fields: [
            {
              name: 'jobId',
              value: this.state.ad.id
            }
          ]
        },
        // notify_on_delivery: 'https://yoursite.com/jobs/1234/posting_complete',
        close_on_completion: 'true',
        vacancy_details: vacancy_details,
        applyonline: {
          url: 'https://jobb.maxkompetens.se/jobs/' + this.state.ad.id
        }
      }
    }

    apiClient.post('recruiter/broadbean-proxy/', transaction).then(result => {
      window.open(result.data.url)
      this.setState({
        loadBroadbeen: false
      })
    })
  }

  render() {
    const { editorState, ad, cityValue, changes } = this.state

    return (
      <div>
        <AvForm model={ad} onValidSubmit={this.handleValidSubmit}>
          <Row>
            <Col xs={12}>
              <AvGroup>
                <AvField
                  name="title"
                  label="Titel"
                  required
                  onChange={this.handleInputChange}
                />
              </AvGroup>
            </Col>
            <Col xs={12}>
              <AvGroup>
                <Label>Annons</Label>
                <Editor
                  editorState={editorState}
                  onEditorStateChange={this.onEditorStateChange}
                  toolbar={{
                    options: [
                      'inline',
                      'blockType',
                      'list',
                      'textAlign',
                      'link',
                      'remove',
                      'history'
                    ],
                    blockType: {
                      inDropdown: true,
                      options: ['Normal', 'H5', 'Blockquote', 'Code'],
                      className: undefined,
                      component: undefined,
                      dropdownClassName: undefined
                    }
                  }}
                />
                <AvField
                  type="textarea"
                  name="test"
                  className="d-none"
                  value={draftToHtml(
                    convertToRaw(editorState.getCurrentContent())
                  )}
                />
              </AvGroup>
            </Col>
            <Col xs={12} md={3}>
              <AvField
                type="select"
                name="job_type"
                label="Anställningstyp"
                onChange={this.handleInputChange}
              >
                <option value={undefined}>---</option>
                <option value="Permanent">Permanent</option>
                <option value="Contract">Contract</option>
                <option value="Temporary">Temporary</option>
              </AvField>
            </Col>
            <Col xs={12} md={3}>
              <AvGroup>
                <Label>Kommun *</Label>
                <Select.Async
                  placeholder={'Välj kommun'}
                  value={cityValue}
                  loadOptions={this.getCountyOptionsAsync}
                  onChange={value => {
                    value &&
                      this.setState({
                        cityValue: value,
                        ad: Object.assign({}, ad, { city: value.id }),
                        changes: true
                      })
                  }}
                />
                <AvField
                  type="hidden"
                  name="city"
                  required
                  value={cityValue ? cityValue.id : ''}
                  onChange={this.handleInputChange}
                  helpMessage="Kommuner hämtas från uppdragets valda Län"
                />
              </AvGroup>
            </Col>
            <Col xs={12} md={3}>
              <AvGroup>
                <Label>Sista ansökningsdag *</Label>
                <Datetime
                  ref="deadline"
                  onChange={date => this.handleDateChange(date, 'deadline')}
                  inputProps={{ required: true }}
                  name="deadline"
                  required
                  dateFormat="YYYY-MM-DD"
                  timeFormat={false}
                  defaultValue={ad.deadline ? ad.deadline : undefined}
                  // isValidDate={validStart}
                />
                <AvField
                  type="hidden"
                  name="published_until"
                  required
                  value={ad ? ad.published_until : ''}
                />
              </AvGroup>
            </Col>
            <Col xs={12} md={3}>
              <AvGroup>
                <Label>Publicerad t.o.m</Label>
                <Datetime
                  ref="published_until"
                  onChange={date =>
                    this.handleDateChange(date, 'published_until')
                  }
                  name="published_until"
                  dateFormat="YYYY-MM-DD"
                  timeFormat={false}
                  defaultValue={
                    ad.published_until
                      ? moment(ad.published_until).format('YYYY-MM-DD')
                      : undefined
                  }
                  // isValidDate={validStart}
                />
                <AvField
                  type="hidden"
                  name="published_until"
                  value={ad.published_until}
                  required
                />
              </AvGroup>
            </Col>
          </Row>

          <Row>
            <Col xs={12} md={6}>
              <Label>Omslagsbild</Label>
              <CoverPhoto
                ad={ad}
                callBack={coverPhoto =>
                  this.setState({ coverImage: coverPhoto })
                }
              />
            </Col>
            <Col xs={12} md={6}>
              <Label>Företagslogo (max 1mb)</Label>
              <CompanyLogo
                ad={ad}
                callBack={logo => this.setState({ logo: logo })}
              />
            </Col>
            <Col xs={12} md={9} className="mt-3">
              <AvGroup>
                <AvField
                  name="video_url"
                  label="Video"
                  onChange={this.handleInputChange}
                  helpMessage="Youtube-id för film (ex. NmPjzQ_ne2I)"
                />
              </AvGroup>
            </Col>
            {this.state.ad.video_url && (
              <Col xs={12} md={6}>
                <Media className="mb-5">
                  <iframe
                    frameBorder={0}
                    allowFullScreen
                    src={
                      'https://www.youtube.com/embed/' + this.state.ad.video_url
                    }
                  />
                </Media>
              </Col>
            )}
          </Row>

          <LoadingButton
            type="submit"
            loading={this.state.loadsave}
            className="mt-3"
            disabled={!changes}
          >
            Spara
          </LoadingButton>

          {this.props.assignments.selectedAssignment.ad && (
            <LoadingButton
              onClick={this.setupXml}
              loading={this.state.loadBroadbeen}
              className="mt-3 ml-2"
              style={{ background: '#7aae01' }}
              disabled={changes}
            >
              Publicera via Broadbean
            </LoadingButton>
          )}
        </AvForm>
      </div>
    )
  }
}

export default connect(state => state)(AdForm)

function htmlEncode(value) {
  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
  //then grab the encoded contents back out.  The div never exists on the page.
  return $('<div/>')
    .text(value)
    .html()
}

function htmlDecode(value) {
  return $('<div/>')
    .html(value)
    .text()
}
