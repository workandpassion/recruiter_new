import React from 'react'
import moment from 'moment'
import { Line } from 'react-chartjs-2'
import { Card, CardBody, CardTitle, CardSubtitle } from 'reactstrap'

/* global gapi */
class UsersSinceStart extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: []
    }

    this.setUpData = this.setUpData.bind(this)
  }

  componentDidMount() {
    this.setUpData()
  }

  setUpData() {
    let { ad, assignment } = this.props
    let _this = this
    let now = moment() // .subtract(3, 'day');

    console.log(ad)
    let mTitle = ad.title
      .replace(/ /g, '-')
      .replace(/å|ä/g, 'a')
      .replace(/ö/g, 'o')
      .replace(/!?/g, '')
      .toLowerCase()

    let path = '/jobs/' + ad.id

    let usersSinceStart = query({
      ids: 'ga:168879665',
      dimensions: 'ga:date,ga:nthDay',
      metrics: 'ga:users',
      'start-date': moment(assignment.start_date).format('YYYY-MM-DD'),
      'end-date': moment(now).format('YYYY-MM-DD'),
      filters: 'ga:pagePath=~' + path + '/*'
    })

    let viewsSinceStart = query({
      ids: 'ga:168879665',
      dimensions: 'ga:date,ga:nthDay',
      metrics: 'ga:pageViews',
      'start-date': moment(assignment.start_date).format('YYYY-MM-DD'),
      'end-date': moment(now).format('YYYY-MM-DD'),
      filters: 'ga:pagePath=~' + path + '/*'
    })

    Promise.all([usersSinceStart, viewsSinceStart]).then(function(results) {
      let data1 = results[0].rows.map(function(row) {
        return +row[2]
      })

      let data2 = results[1].rows.map(function(row) {
        return +row[2]
      })

      let labels = results[0].rows.map(function(row) {
        return +row[0]
      })

      labels = labels.map(function(label) {
        return moment(label, 'YYYYMMDD').format('ddd')
      })

      let data = {
        labels: labels,
        datasets: [
          {
            label: 'Användare per dag',
            backgroundColor: 'rgba(17,167,173,0.5)',
            borderColor: 'rgba(17,167,173,1)',
            pointStrokeColor: '#fff',
            data: data1
          },
          {
            label: 'Sidvisningar',
            backgroundColor: 'rgba(255,180,0,0.5)',
            borderColor: 'rgba(255,180,0,1)',
            pointStrokeColor: '#fff',
            data: data2
          }
        ]
      }

      _this.setState({ data: data })
    })
  }

  render() {
    let { data } = this.state

    return (
      <Card>
        <CardBody>
          <CardTitle>Besökare på annons</CardTitle>
          <CardSubtitle>Sen start</CardSubtitle>
          <Line data={data} />
        </CardBody>
      </Card>
    )
  }
}

export default UsersSinceStart

function query(params) {
  return new Promise(function(resolve, reject) {
    let data = new gapi.analytics.report.Data({ query: params })
    data
      .once('success', function(response) {
        resolve(response)
      })
      .once('error', function(response) {
        reject(response)
      })
      .execute()
  })
}
