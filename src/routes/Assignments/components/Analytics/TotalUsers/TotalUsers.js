import React from 'react'
import { Badge } from 'reactstrap'
import moment from 'moment/moment'

/* global gapi */

class TotalUsers extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      totalUsers: 0
    }
  }
  componentDidMount() {
    const { assignment, ad } = this.props
    let now = moment()
    let path = '/jobs/' + ad.id
    let _this = this

    let totalUsers = query({
      ids: 'ga:168879665',
      metrics: 'ga:users',
      'start-date': moment(assignment.start_date).format('YYYY-MM-DD'),
      'end-date': moment(now).format('YYYY-MM-DD'),
      filters: 'ga:pagePath=~' + path + '/*'
    })

    Promise.all([totalUsers]).then(function(results) {
      console.log(results)

      _this.setState({
        totalUsers: results[0].rows[0]
      })
    })
  }

  render() {
    const { totalUsers } = this.state
    return (
      <h3 className="mr-2">
        <div id="total-users-container" className="stat-container">
          <div>Totalt antal besökare: {totalUsers}</div>
        </div>
      </h3>
    )
  }
}

export default TotalUsers

function query(params) {
  return new Promise(function(resolve, reject) {
    let data = new gapi.analytics.report.Data({ query: params })
    data
      .once('success', function(response) {
        resolve(response)
      })
      .once('error', function(response) {
        reject(response)
      })
      .execute()
  })
}
