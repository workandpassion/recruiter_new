import React from 'react'
// import google from 'googleapis'
import googleServiceAccountKey from './MaxkompetensWap-8e7c85bfa4de'
import CryptoJS from 'crypto-js'
import { Container, Row, Col } from 'reactstrap'
import ActiveUsers from './ActiveUsers/ActiveUsers'
import ThisVsLastWeek from './ThisVsLastWeek/ThisVsLastWeek'
import UsersSinceStart from './UsersSinceStart/UsersSinceStart'
import TotalUsers from './TotalUsers/TotalUsers'

/* global gapi */

class Analytics extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      gapiReady: false,
      ad: props.assignment.ad,
      assignment: props.assignment
    }

    this.createCharts = this.createCharts.bind(this)
  }

  loadGoogleApi() {
    const script = document.createElement('script')
    script.src = 'https://apis.google.com/js/platform.js'

    script.onload = () => {
      gapi.load('analytics', () => {
        this.setState({ gapiReady: true })
      })
    }

    document.body.appendChild(script)
  }

  componentDidMount() {
    this.loadGoogleApi()
  }

  componentDidUpdate(prevProps, prevState) {
    let _this = this
    if (!prevState.gapiReady && this.state.gapiReady) {
      this.getToken()
      gapi.analytics.ready(function() {
        _this.createCharts()
      })
    }
  }

  createCharts() {
    let { ad } = this.state

    let mTitle = ad.title
      .replace(/ /g, '-')
      .replace(/å|ä/g, 'a')
      .replace(/ö/g, 'o')
      .replace(/!?/g, '')
      .toLowerCase()

    let path = '/jobs/' + ad.id + '/' + mTitle

    let dataChart1 = new gapi.analytics.googleCharts.DataChart({
      query: {
        ids: 'ga:168879665', // <-- Replace with the ids value for your view.
        'start-date': '30daysAgo',
        'end-date': 'yesterday',
        metrics: 'ga:sessions,ga:users',
        dimensions: 'ga:date',
        filters: 'ga:pagePath=~' + path + '/*'
      },
      chart: {
        container: 'chart-1-container',
        type: 'LINE',
        options: {
          width: '100%'
        }
      }
    })
    dataChart1.execute()
  }

  getToken() {
    let pHeader = { alg: 'RS256', typ: 'JWT' }
    let sHeader = JSON.stringify(pHeader)
    let encodedHeader = Base64EncodeUrl(btoa(sHeader))

    let now = new Date()
    let oneHourExpiration =
      (now.getTime() - now.getMilliseconds()) / 1000 + 3000
    let pClaim = {}
    pClaim.aud = 'https://www.googleapis.com/oauth2/v4/token'
    pClaim.scope = 'https://www.googleapis.com/auth/analytics.readonly'
    pClaim.iss = googleServiceAccountKey.client_email
    pClaim.exp = oneHourExpiration
    pClaim.iat = Math.floor(Date.now() / 1000)

    let sClaim = JSON.stringify(pClaim)
    let encodedClaim = Base64EncodeUrl(btoa(sClaim))

    let byteArray = encodedHeader + '.' + encodedClaim
    let secret = googleServiceAccountKey.private_key

    let forge = require('node-forge')
    let privateKey = forge.pki.privateKeyFromPem(secret)

    let md = forge.md.sha256.create()
    md.update(byteArray, 'utf8')
    let signature = privateKey.sign(md)

    //convert signature to base64
    let encodedSignature = Base64EncodeUrl(btoa(signature))

    let sJWS = byteArray + '.' + encodedSignature

    let XHR = new XMLHttpRequest()
    let urlEncodedData = ''
    let urlEncodedDataPairs = []

    urlEncodedDataPairs.push(
      encodeURIComponent('grant_type') +
        '=' +
        encodeURIComponent('urn:ietf:params:oauth:grant-type:jwt-bearer')
    )
    urlEncodedDataPairs.push(encodeURIComponent('assertion') + '=' + sJWS)
    urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+')

    // We define what will happen if the data are successfully sent
    XHR.addEventListener('load', function(event) {
      let response = JSON.parse(XHR.responseText)
      let token = response['access_token']

      gapi.analytics.auth.authorize({
        serverAuth: {
          access_token: token
        }
      })
    })

    // We define what will happen in case of error
    XHR.addEventListener('error', function(event) {
      console.log('Oops! Something went wrong.')
    })

    XHR.open('POST', 'https://www.googleapis.com/oauth2/v4/token')
    XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    XHR.send(urlEncodedData)
  }

  render() {
    let { gapiReady, ad, assignment } = this.state
    return (
      <Container fluid className="mt-5">
        <Row>
          <Col xs={12} className="d-flex flex-row">
            {gapiReady && <TotalUsers ad={ad} assignment={assignment} />}
            {gapiReady &&
              process.env.NODE_ENV !== 'development' && (
                <ActiveUsers ad={ad} assignment={assignment} />
              )}
          </Col>
          <Col xs={12} md={4}>
            {gapiReady && <UsersSinceStart ad={ad} assignment={assignment} />}
          </Col>
          <Col xs={12} md={4}>
            {gapiReady && <ThisVsLastWeek ad={this.state.ad} />}
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Analytics

function base64url(source) {
  // Encode in classical base64
  let encodedSource = CryptoJS.enc.Base64.stringify(source)

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, '')

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, '-')
  encodedSource = encodedSource.replace(/\//g, '_')

  return encodedSource
}

function Base64EncodeUrl(str) {
  return str
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/\=+$/, '')
}
