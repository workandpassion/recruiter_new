import React from 'react'
import { connect } from 'react-redux'
import {
  addAssignment,
  updateAssignment
} from '../../../store/actions/assignments'
import { apiClient } from '../../../store/axios.config'
import Select, { Creatable } from 'react-select'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import { Row, Col, Label } from 'reactstrap'
import { Button } from 'react-md'
import moment from 'moment'
import CustomerForm from '../../Customers/components/CustomerForm'
import ContactPersonForm from '../../Customers/components/ContactPersonForm'
import _ from 'lodash'
import LoadingButton from '../../../components/LoadingButton/LoadingButton'

class AssignmentsForm extends React.Component {
  constructor(props) {
    super(props)

    let mCustomerValue = undefined
    let mContactsValue = []
    let mTypeValue = undefined

    if (props.assignment) {
      mCustomerValue = Object.assign(
        {
          value: props.assignment.customer.id,
          label: props.assignment.customer.name
        },
        props.assignment.customer
      )

      props.assignment.contacts &&
        props.assignment.contacts.map(contact => {
          mContactsValue.push(
            Object.assign(
              {
                value: contact.id,
                label: contact.name
              },
              contact
            )
          )
        })

      mTypeValue = props.assignment.employment_type
    }

    this.state = {
      loadsave: false,
      customerModal: false,
      customerValue: mCustomerValue,
      contactModal: false,
      contactValue: mContactsValue,
      typeValue: mTypeValue,
      occupationValue: undefined,
      skillValue: undefined,
      occupationOptions: undefined,
      skillOptions: undefined,
      countyOptions: undefined,
      countyValue: undefined,
      editMode: !!props.assignment && props.assignment.subject,
      assignment: props.assignment
        ? {
            ...props.assignment,
            difficulty: props.assignment.difficulty
              ? props.assignment.difficulty
              : 1,
            counties: _.map(this.props.assignment.counties, 'id'),
            contacts: _.map(props.assignment.contacts, 'id'),
            occupations: _.map(props.assignment.occupations, 'id'),
            skills: _.map(props.assignment.skills, 'id'),
            recruiter_offices: _.map(props.assignment.recruiter_offices, 'id'),
            recruiters: _.map(props.assignment.recruiters, 'id'),
            states: _.map(props.assignment.states, 'id'),
            customer: props.assignment.customer.id,
            ad: props.assignment.ad ? props.assignment.ad.id : undefined,
            recruitment_processes: _.map(
              props.assignment.recruitment_processes,
              'id'
            ),
            matchedCandidates: []
          }
        : {
            recruiters: [props.user.profile.id],
            occupations: [],
            skills: [],
            difficulty: 1
          }
    }

    this.getCustomerOptions = this.getCustomerOptions.bind(this)
    this.onCustomerChange = this.onCustomerChange.bind(this)
    this.addCustomer = this.addCustomer.bind(this)
    this.getContactOptions = this.getContactOptions.bind(this)
    this.onContactChange = this.onContactChange.bind(this)
    this.onTypeChange = this.onTypeChange.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleValidSubmit = this.handleValidSubmit.bind(this)
    this.toggleCustomerModal = this.toggleCustomerModal.bind(this)
    this.onOccupationChange = this.onOccupationChange.bind(this)
    this.onSkillChange = this.onSkillChange.bind(this)
    this.toggleContactModal = this.toggleContactModal.bind(this)
    this.getOccupationOptionsAsync = this.getOccupationOptionsAsync.bind(this)
    this.getSkillOptionsAsync = this.getSkillOptionsAsync.bind(this)
    this.getCountyOptionsAsync = this.getCountyOptionsAsync.bind(this)
    this.onCountyChange = this.onCountyChange.bind(this)
  }

  componentDidMount() {}

  getCustomerOptions() {
    let mCustomers = []
    this.props.customers.allCustomers &&
      this.props.customers.allCustomers.map(customer => {
        return mCustomers.push(
          Object.assign(
            {
              value: customer.id,
              label: customer.name
            },
            customer
          )
        )
      })

    return mCustomers
  }

  getContactOptions() {
    let mContacts = []
    this.state.customerValue &&
      this.state.customerValue.contacts &&
      this.state.customerValue.contacts.map(contact => {
        return mContacts.push(
          Object.assign(
            {
              value: contact.id,
              label: contact.name
            },
            contact
          )
        )
      })
    return mContacts
  }

  getOccupationOptionsAsync(input, callback) {
    let mOccupations = []
    apiClient.get('occupations/').then(result => {
      result.data.map(occupation => {
        return occupation.occupations.map(occ => {
          mOccupations.push({
            id: occ.id,
            value: occ.id,
            label: occ.name + ' (' + occupation.name + ')'
          })
        })
      })
      callback(null, {
        options: mOccupations,
        complete: true
      })

      let mOccupationValue = this.state.occupationValue
        ? this.state.occupationValue
        : []
      if (this.props.assignment) {
        let { occupations } = this.props.assignment
        occupations.map(occ => {
          let test = _.find(mOccupations, { value: occ.id })
          mOccupationValue.push(test)
        })
      }

      this.setState({
        occupationOptions: mOccupations,
        occupationValue: mOccupationValue
      })
    })
  }

  getSkillOptionsAsync(input, callback) {
    let mSkills = []

    apiClient.get('skills/').then(result => {
      result.data.map(skillParent => {
        return skillParent.skills.map(skill => {
          return mSkills.push({
            id: skill.id,
            value: skill.id,
            label: skill.name + ' (' + skillParent.name + ')'
          })
        })
      })
      callback(null, {
        options: mSkills,
        complete: true
      })

      let mSkillValue = this.state.skillValue ? this.state.skillValue : []

      if (this.props.assignment) {
        let { skills } = this.props.assignment
        skills.map(occ => {
          let test = _.find(mSkills, { value: occ.id })
          mSkillValue.push(test)
        })
      }

      this.setState({
        skillOptions: mSkills,
        skillValue: mSkillValue
      })
    })
  }

  getCountyOptionsAsync(input, callback) {
    let mCounties = []

    apiClient.get('locations/').then(result => {
      result.data.map(county => {
        return mCounties.push({
          id: county.id,
          value: county.id,
          label: county.name
        })
      })
      callback(null, {
        options: mCounties,
        complete: true
      })

      let mCountyValue = this.state.countyValue ? this.state.countyValue : []

      if (this.props.assignment) {
        let { counties } = this.props.assignment
        counties.map(occ => {
          let test = _.find(mCounties, { value: occ.id })
          mCountyValue.push(test)
        })
      }

      this.setState({
        countyOptions: mCounties,
        countyValue: mCountyValue
      })
    })
  }

  onCustomerChange(value) {
    this.setState({
      customerValue: value,
      contactValue: undefined,
      assignment: {
        ...this.state.assignment,
        customer: value ? value.value : undefined,
        contacts: []
      }
    })
  }

  onContactChange(value) {
    let mContacts = []
    value &&
      value.map(contact => {
        mContacts.push(contact.id)
      })

    this.setState({
      contactValue: value,
      assignment: {
        ...this.state.assignment,
        contacts: mContacts
      }
    })
  }

  onTypeChange(value) {
    this.setState({
      typeValue: value,
      assignment: {
        ...this.state.assignment,
        employment_type: value ? value.value : undefined
      }
    })
  }

  onOccupationChange(value) {
    let mOccupations = []
    value.map(occupation => {
      return mOccupations.push(occupation.id)
    })
    this.setState({
      occupationValue: value,
      assignment: {
        ...this.state.assignment,
        occupations: mOccupations
      }
    })
  }

  onSkillChange(value) {
    let mSkills = []
    value.map(skill => {
      return mSkills.push(skill.id)
    })
    this.setState({
      skillValue: value,
      assignment: {
        ...this.state.assignment,
        skills: mSkills
      }
    })
  }

  onCountyChange(value) {
    let mCounties = []
    value.map(county => {
      return mCounties.push(county.id)
    })
    this.setState({
      countyValue: value,
      assignment: {
        ...this.state.assignment,
        counties: mCounties
      }
    })
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name

      this.setState({
        assignment: {
          ...this.state.assignment,
          recruiter_offices:
            this.props.user.profile.offices.length > 0
              ? [this.props.user.profile.offices[0].id]
              : [],
          recruitment_processes: [],
          states: [],
          start_date: moment().format('YYYY-MM-DD'),
          end_date: moment().format('YYYY-MM-DD'),
          [name]: value
        }
      })
    }
  }

  handleValidSubmit(event, values) {
    this.setState({
      loadsave: true
    })
    let _self = this

    let { dispatch } = this.props

    // let mAssignment = Object.assign({}, this.state.assignment, {
    //   ad:
    //     this.state.assignment.ad && this.state.assignment.ad.length > 0
    //       ? _.map(this.state.assignment.ad, 'id')
    //       : undefined
    // })
    createAssignment(this.state.assignment)

    function createAssignment(assignment) {
      assignment.matchedCandidates = []
      if (_self.props.assignment && _self.props.assignment.subject) {
        dispatch(updateAssignment(assignment))
          .catch(error => {
            _self.setState({
              loadsave: false
            })
          })
          .then(() => {
            _self.setState({
              assignment: undefined,
              loadsave: false
            })
          })
      } else {
        dispatch(addAssignment(assignment))
          .catch(error => {
            _self.setState({
              loadsave: false
            })
          })
          .then(() => {
            _self.setState({
              assignment: undefined,
              loadsave: false
            })

            _self.props.onCreatedAssignment()
          })
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.assignment && !this.state.assignment) {
      this.setState({
        assignment: {
          ...this.props.assignment,
          counties: _.map(this.props.assignment.counties, 'id'),
          contacts: _.map(this.props.assignment.contacts, 'id'),
          occupations: _.map(this.props.assignment.occupations, 'id'),
          skills: _.map(this.props.assignment.skills, 'id'),
          recruiter_offices: _.map(
            this.props.assignment.recruiter_offices,
            'id'
          ),
          recruiters: _.map(this.props.assignment.recruiters, 'id'),
          recruitment_processes: _.map(
            this.props.assignment.recruitment_processes,
            'id'
          ),
          ad: this.props.unnested.ad,
          states: _.map(this.props.assignment.states, 'id'),
          customer: this.props.assignment.customer.id
        }
      })
    }

    let prevCustomers = prevProps.customers.allCustomers
    let thisCustomers = this.props.customers.allCustomers
    if (
      prevCustomers !== thisCustomers &&
      thisCustomers &&
      this.state.customerModal
    ) {
      let mCustomer = this.props.customers.allCustomers.slice(-1).pop()
      this.setState({
        assignment: {
          ...this.state.assignment,
          customer: mCustomer.id
        },
        customerValue: Object.assign(
          {
            value: mCustomer.id,
            label: mCustomer.name
          },
          mCustomer
        )
      })
    }

    if (
      typeof this.state.customerValue === 'object' &&
      prevProps.customers.fetchingCustomers &&
      !this.props.customers.fetchingCustomers
    ) {
      let prevCustomer = _.find(prevProps.customers.allCustomers, {
        id: this.state.customerValue.id
      })

      let mCustomer = _.find(this.props.customers.allCustomers, {
        id: this.state.customerValue.id
      })

      let mContact = _.differenceBy(
        mCustomer.contacts,
        prevCustomer.contacts,
        'id'
      )

      let mContactsValue = _.dropRight(this.state.contactValue)

      if (mContact && mContact.length > 0) {
        this.setState({
          assignment: {
            ...this.state.assignment,
            contacts:
              this.state.assignment.contacts &&
              this.state.assignment.contacts.length > 0
                ? _.concat(this.state.assignment.contacts, mContact[0].id)
                : [mContact[0].id]
          },
          contactValue: _.concat(
            mContactsValue,
            Object.assign(
              {
                value: mContact[0].id,
                label: mContact[0].name
              },
              mContact
            )
          )
        })
      }
    }
  }

  addCustomer(option) {
    this.setState({
      customerValue: option.value,
      customerModal: true
    })
  }

  addContact(option) {
    let mContacts = _.concat(this.state.contactValue, option.value)
    this.setState({
      contactValue: mContacts,
      contactModal: true
    })
  }

  toggleCustomerModal() {
    this.setState({
      customerModal: !this.state.customerModal
    })
  }

  toggleContactModal() {
    this.setState({
      contactModal: !this.state.contactModal
    })
  }

  render() {
    return (
      <AvForm
        onValidSubmit={this.handleValidSubmit}
        model={this.state.assignment}
      >
        <Row>
          <Col xs={12} lg={6}>
            <AvGroup>
              <AvField
                name="subject"
                label="Uppdragstitel"
                required
                onChange={this.handleInputChange}
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={3}>
            <AvGroup>
              <AvField
                name="price"
                label="Pris"
                type="number"
                onChange={this.handleInputChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={3}>
            <AvGroup>
              <Label>Typ</Label>
              <Select
                placeholder={'Välj typ'}
                value={this.state.typeValue}
                options={typeOptions}
                onChange={this.onTypeChange}
              />
              <AvField
                type="hidden"
                name="employment_type"
                value={this.state.typeValue && this.state.typeValue.value}
                required
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Kund</Label>
              <Creatable
                placeholder={'Välj kund'}
                value={this.state.customerValue}
                options={this.getCustomerOptions()}
                onChange={this.onCustomerChange}
                promptTextCreator={label => {
                  return 'Skapa kund ' + label
                }}
                onNewOptionClick={option => {
                  this.addCustomer(option)
                }}
              />
              <AvField
                type="hidden"
                name="customer"
                value={
                  this.state.customerValue && this.state.customerValue.value
                    ? this.state.customerValue.value
                    : ''
                }
                required
              />
            </AvGroup>
          </Col>
          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Kontaktperson</Label>
              <Creatable
                multi
                disabled={!this.state.customerValue}
                placeholder={
                  this.state.customerValue
                    ? 'Välj kontaktperson(er)'
                    : 'Välj kund först'
                }
                value={this.state.contactValue}
                options={this.getContactOptions()}
                onChange={this.onContactChange}
                promptTextCreator={label => {
                  return 'Skapa kontaktperson ' + label
                }}
                onNewOptionClick={option => {
                  this.addContact(option)
                }}
              />
              <AvField
                type="hidden"
                name="contact_person"
                value={this.state.contactValue && this.state.contactValue.value}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Uppdragsbefattningar</Label>
              <Select.Async
                multi
                placeholder={'Välj befattningar'}
                value={this.state.occupationValue}
                loadOptions={this.getOccupationOptionsAsync}
                onChange={this.onOccupationChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Kompetenser</Label>
              <Select.Async
                multi
                placeholder={'Välj kompetenser'}
                value={this.state.skillValue}
                loadOptions={this.getSkillOptionsAsync}
                onChange={this.onSkillChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={6}>
            <AvGroup>
              <Label>Län</Label>
              <Select.Async
                multi
                placeholder={'Välj län'}
                value={this.state.countyValue}
                loadOptions={this.getCountyOptionsAsync}
                onChange={this.onCountyChange}
              />
            </AvGroup>
          </Col>

          <Col xs={12} lg={3}>
            <AvGroup>
              <AvField
                type="select"
                name="difficulty"
                label="Svårighetsgrad"
                onChange={this.handleInputChange}
              >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </AvField>
            </AvGroup>
          </Col>

          <Col xs={12}>
            <AvField
              type="textarea"
              name="body"
              label="Kravspec / Uppdragsinformation"
              onChange={this.handleInputChange}
            />
          </Col>
          <Col xs={12}>
            <AvField
              type="textarea"
              name="comment"
              label="Interna kommentarer"
              onChange={this.handleInputChange}
            />
          </Col>
        </Row>
        {this.state.customerModal && (
          <CustomerForm
            show={this.state.customerModal}
            customer={{ name: this.state.customerValue }}
            parentCallback={this.toggleCustomerModal}
          />
        )}
        {this.state.contactModal && (
          <ContactPersonForm
            show={this.state.contactModal}
            customer={this.state.customerValue}
            contact_person={{
              name: this.state.contactValue[this.state.contactValue.length - 1]
            }}
            parentCallback={this.toggleContactModal}
          />
        )}
        <LoadingButton
          type="submit"
          loading={this.state.loadsave ? this.state.loadsave : undefined}
        >
          Spara
        </LoadingButton>
      </AvForm>
    )
  }
}

export default connect(state => state)(AssignmentsForm)

const typeOptions = [
  { label: 'Uthyrning', value: 'hire' },
  { label: 'Rekrytering', value: 'recruite' },
  { label: 'Hyrköp', value: 'hire-purchase' }
]
