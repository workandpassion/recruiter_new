import React from 'react'
import { connect } from 'react-redux'
import {
  createCandidateProcessState,
  updateCandidateProcessState
} from '../../../store/actions/assignments'
import { AvForm, AvField, AvGroup } from 'availity-reactstrap-validation'
import { Row, Col, Label, Modal, ModalBody, ModalFooter } from 'reactstrap'
import { Button } from 'react-md'
import DatePicker from 'react-datepicker'
import moment from 'moment'

import _ from 'lodash'

moment.locale('sv')
class ProcessStateForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      processState: this.props.processState
        ? Object.assign({ constructor: true }, this.props.processState)
        : null,
      stateOptions: this.props.assignments.processStateValues,
      scheduled_at: this.props.processState
        ? this.props.processState.scheduled_at &&
          moment(this.props.processState.scheduled_at)
        : null,
      completed_at: this.props.processState
        ? this.props.processState.completed_at &&
          moment(this.props.processState.completed_at)
        : null,
      shouldDatesBeVisible: true
    }

    this.handleValidSubmit = this.handleValidSubmit.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleScheduledChange = this.handleScheduledChange.bind(this)
    this.handleCompletedChange = this.handleCompletedChange.bind(this)
  }

  componentDidMount() {
    this.setState({
      processState: this.props.processState
    })
  }

  handleInputChange(event) {
    const target = event.target

    if (target) {
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name
      if (name === 'current') {
        if (event.target.checked) {
        }
      }

      this.setState({
        processState: {
          ...this.state.processState,
          [name]: value
        }
      })
    }
  }

  handleValidSubmit(event, values) {
    let { dispatch } = this.props
    let assignmentId = this.props.assignments.selectedAssignment.id
    let { process } = this.props
    let processId = process.id

    if (this.props.processState) {
      dispatch(
        updateCandidateProcessState(
          assignmentId,
          process,
          this.state.processState
        )
      ).then(() => {
        this.setState({
          processState: undefined
        })
        this.props.toggle()
      })
    } else {
      dispatch(
        createCandidateProcessState(
          assignmentId,
          processId,
          this.state.processState
        )
      ).then(() => {
        this.setState({
          processState: undefined
        })
        this.props.toggle()
      })
    }
  }

  shouldOptionsBeDisabled(value) {
    let { states } = this.props.process
    if (value) {
      if (
        this.props.processState &&
        this.props.processState.value.id === value
      ) {
        return false
      }
      return _.find(states, { value: { id: value } })
    } else {
      return false
    }
  }

  handleScheduledChange(date) {
    this.setState({
      scheduled_at: date,
      processState: {
        ...this.state.processState,
        scheduled_at: date
      }
    })
  }

  handleCompletedChange(date) {
    this.setState({
      completed_at: date,
      processState: {
        ...this.state.processState,
        completed_at: date
      }
    })
  }

  render() {
    let { process, processState } = this.props
    console.log(processState)
    return (
      <Modal
        isOpen={this.props.show}
        toggle={this.props.toggle}
        className="modal-lg"
      >
        <AvForm
          onValidSubmit={this.handleValidSubmit}
          model={this.state.processState}
        >
          <ModalBody>
            <Row>
              <Col xs={12}>
                <h3>
                  {process.user.first_name} {process.user.last_name} | {
                    processState.value.name
                  }
                </h3>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <AvGroup>
                  <AvField
                    type="textarea"
                    name="comment"
                    label="Kommentar"
                    onChange={this.handleInputChange}
                  />
                </AvGroup>
              </Col>
              {this.state.shouldDatesBeVisible && (
                <Col xs={6}>
                  <AvGroup>
                    <Label>Planerad</Label>
                    <DatePicker
                      selected={this.state.scheduled_at}
                      onChange={this.handleScheduledChange}
                      dateFormat="YYYY-MM-DD"
                      disabled={this.props.dateHidden}
                    />
                  </AvGroup>
                </Col>
              )}
              {this.state.shouldDatesBeVisible && (
                <Col xs={6}>
                  <AvGroup>
                    <Label>Genomförd</Label>
                    <DatePicker
                      selected={this.state.completed_at}
                      onChange={this.handleCompletedChange}
                      dateFormat="YYYY-MM-DD"
                      disabled={this.props.dateHidden}
                    />
                  </AvGroup>
                </Col>
              )}
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button flat primary swapTheming type="submit">
              OK
            </Button>{' '}
            <Button flat secondary swapTheming onClick={this.props.toggle}>
              Avbryt
            </Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    )
  }
}

export default connect(state => state)(ProcessStateForm)
