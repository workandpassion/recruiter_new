import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { getAssignments } from '../../../store/actions/assignments'
import { getAllCustomers } from '../../../store/actions/customers'
import { Card, CardBody, Container } from 'reactstrap'
import AssignmentsFormNew from '../components/AssignmentsFormNew'
import AssignmentsTable from '../components/AssignmentsTable'
import SubHeader from '../../../layouts/Header/SubHeader'

class MyAssignments extends React.Component {
  constructor(props) {
    super(props)

    document.title = 'Mina uppdrag | wap recruiter'

    this.state = {
      loadsave: true,
      showForm: false
    }

    this.toggleForm = this.toggleForm.bind(this)
  }

  componentDidMount() {
    let { dispatch } = this.props
    Promise.all([dispatch(getAssignments()), dispatch(getAllCustomers())]).then(
      () => {
        this.setState({
          loadsave: false
        })
      }
    )
  }

  toggleForm(e) {
    this.setState({
      showForm: !this.state.showForm,
      mEvent: e
    })
  }

  render() {
    const quicknavs = [
      { title: 'Nytt uppdrag', icon: 'fa-plus', fn: this.toggleForm }
    ]

    return (
      <div>
        <SubHeader actions={quicknavs} />
        <Container fluid className="contentContainer">
          <Card>
            <CardBody>
              <AssignmentsTable
                assignmentsList={
                  this.props.assignments.myAssignments
                    ? this.props.assignments.myAssignments
                    : []
                }
                loading={this.state.loadsave}
              />
            </CardBody>
          </Card>
        </Container>
        <AssignmentsFormNew
          show={this.state.showForm}
          mEvent={this.state.mEvent}
        />
      </div>
    )
  }
}

export default withRouter(connect(state => state)(MyAssignments))
