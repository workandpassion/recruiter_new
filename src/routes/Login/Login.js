import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { Container, Row, Col, Form, FormGroup, Label, Input } from 'reactstrap'
import LoadingButton from '../../components/LoadingButton/LoadingButton'
import { login, setTokenAfterUpdate } from '../../store/actions/user'
import Cookies from 'universal-cookie'
import maxlogo from './maxlogo.png'
import background from './background.jpg'

const cookies = new Cookies()

class Login extends React.Component {
  constructor(props) {
    super(props)

    this._handleLogin = this._handleLogin.bind(this)
  }

  componentDidMount() {
    // cookies.remove('update', { path: '/' })
    let { dispatch } = this.props
    if (cookies.get('update', { path: '/' })) {
      console.log('autologhin after update')
      dispatch(setTokenAfterUpdate(cookies.get('update', { path: '/' })))
      console.log('remove cookie')
      cookies.remove('update', { path: '/' })
    }
  }

  _handleLogin(e) {
    e.preventDefault()
    let { dispatch } = this.props
    let email = ReactDOM.findDOMNode(this.email)
    let password = ReactDOM.findDOMNode(this.password)

    let creds = {
      username: email.value,
      password: password.value
    }

    dispatch(login(creds))
      .then(result => {})
      .catch(error => {})
  }

  render() {
    const { loggingIn } = this.props.user
    return (
      <Container fluid className="h-100">
        <Row
          style={{ minHeight: '100vh', background: 'url(' + background + ')' }}
        >
          <Col xs={12} sm={8} md={6} lg={3} style={{ background: 'white' }}>
            <Row>
              <Col xs={12} className="p-5">
                <img src={maxlogo} className="img-fluid d-block mx-auto" />
              </Col>
            </Row>
            <div className="px-3">
              <Form>
                <FormGroup>
                  <Label for="email">Epost</Label>
                  <Input
                    type="email"
                    name="email"
                    ref={input => {
                      this.email = input
                    }}
                    placeholder="förnamn.efternamn@maxkompetens.se"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="password">Lösenord</Label>
                  <Input
                    type="password"
                    name="password"
                    ref={input => {
                      this.password = input
                    }}
                    placeholder="Lösenord"
                  />
                </FormGroup>
                <LoadingButton
                  loading={loggingIn}
                  onClick={this._handleLogin}
                  className="w-100"
                >
                  Logga in
                </LoadingButton>
              </Form>
            </div>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default connect(state => state)(Login)
