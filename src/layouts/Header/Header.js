import React from 'react'
import { connect } from 'react-redux'
import { logout } from '../../store/actions/user'
import { withRouter, Link } from 'react-router-dom'
import $ from 'jquery'
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from 'reactstrap'

const defaultPic = process.env.PUBLIC_URL + '/imgs/no_picture.jpg'
// const defaultPic = ''

class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      dropdownOpen: false,
      imageUrl:
        'https://api.wapcard.se/api/v1/profiles/' +
        props.user.profile.id +
        '/picture/500?' +
        props.user.profile.picture,
      showBackButton: true
    }

    this.dispatch = props.dispatch

    this.onError = this.onError.bind(this)
    this._handleLogout = this._handleLogout.bind(this)
    this._toggleSidemenu = this._toggleSidemenu.bind(this)
    this.toggle = this.toggle.bind(this)
    this.goBack = this.goBack.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    let routeChanged = nextProps.location !== this.props.location
    // this.setState({ showBackButton: routeChanged })
  }

  onError() {
    this.setState({
      imageUrl: defaultPic
    })
  }

  _handleLogout(e) {
    e.preventDefault()
    this.dispatch(logout())
  }

  _toggleSidemenu() {
    let $body = $('body')
    $body.toggleClass('menu-expanded')
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  goBack(e) {
    e.preventDefault()
    this.props.history.goBack()
  }

  render() {
    let { profile } = this.props.user
    return (
      <header id="header" className="container-fluid">
        <div
          id="menubar"
          className="d-lg-none d-xl-none"
          onClick={() => this._toggleSidemenu()}
        >
          <i className="fa fa-bars" />
        </div>
        <div className="pull-left left_content d-none d-sm-none d-md-none d-lg-flex">
          {/*<h4 className="mb-0">WAP recruiter</h4>*/}
          {this.state.showBackButton && (
            <a id="go-back" href="#" onClick={this.goBack}>
              <i className="fa fa-chevron-left" />
            </a>
          )}

          <a href="/" id="go-home">
            <i className="fa fa-home" />
          </a>
        </div>
        <div className="right_content">
          <Dropdown
            isOpen={this.state.dropdownOpen}
            toggle={this.toggle}
            className="profile_menu"
          >
            <DropdownToggle caret id="dropdownMenuLink">
              <img
                src={this.state.imageUrl}
                onError={this.onError}
                alt=""
                id="profile_img"
              />
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem>
                <Link to="/profile">Profil</Link>
              </DropdownItem>
              <DropdownItem onClick={this._handleLogout}>Logga ut</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </header>
    )
  }
}

export default withRouter(connect(state => state)(Header))
