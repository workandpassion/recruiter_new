import React from 'react'
import BreadCrumbs from '../../components/Breadcrumbs/Breadcrumbs'
import { Container, Tooltip, Row, Col } from 'reactstrap'
import classNames from 'classnames'
import QuickNav from '../../components/QuickNav/QuickNav'
import { Button } from 'react-md'

class SubHeader extends React.Component {
  render() {
    return (
      <Container fluid id="subHeader">
        <Row className="justify-content-between align-items-center">
          <div className="left">
            <div className="pull-left">
              <BreadCrumbs />
            </div>
            <div className="tabs">{this.props.children}</div>
          </div>
          <div className="right">
            <ActionButtons actions={this.props.actions} />
            <QuickNav items={this.props.actions} />
          </div>
        </Row>
      </Container>
    )
  }
}

export default SubHeader

export class ActionButtons extends React.Component {
  constructor(props) {
    super(props)

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e, item) {
    if (item.fn) {
      e.persist()
      item.fn(e)
    }
  }
  render() {
    let actionClass = classNames(
      'actionButtons pull-right d-none d-sm-none d-md-flex',
      this.props.showOnHover && 'showOnHover'
    )
    return (
      <div className={actionClass}>
        {this.props.actions &&
          this.props.actions.map(action => {
            return (
              <ActionButton
                key={action.icon}
                icon={action.icon}
                id={action.id ? action.id : action.icon}
                helpText={action.title}
                onClick={e => this.handleClick(e, action)}
                loading={action.loading}
              />
            )
          })}
      </div>
    )
  }
}

class ActionButton extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      tooltipOpen: false
    }
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    })
  }

  render() {
    let actionClass = classNames(
      'fa',
      this.props.loading ? 'fa-spin fa-spinner' : this.props.icon
    )
    return (
      <div>
        <Button
          icon
          primary
          swapTheming
          iconClassName={actionClass}
          id={this.props.id}
          onClick={this.props.onClick}
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 5
          }}
        />
        <Tooltip
          delay={0}
          placement="bottom"
          isOpen={this.state.tooltipOpen}
          target={this.props.id}
          toggle={this.toggle}
        >
          {this.props.helpText}
        </Tooltip>
      </div>
    )
  }
}
