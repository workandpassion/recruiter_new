import React from 'react'
import Header from '../Header/Header'
import Sidemenu from '../Sidemenu/Sidemenu'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Notifications from 'react-notification-system-redux'
import { userIsAuthenticatedRedir } from '../../store/auth'
import { apiClient } from '../../store/axios.config'
import { update } from '../../store/actions/user'
import { getAssignments } from '../../store/actions/assignments'

const appVersion = '1.21'

class CoreLayout extends React.Component {
  constructor(props) {
    super(props)

    let { dispatch } = this.props
    let _self = this
    this.state = {
      appVersion: appVersion,
      updateAvailable: false,
      first: true
    }

    let sleep = time => new Promise(resolve => setTimeout(resolve, time))
    let poll = (promiseFn, time, clear) =>
      promiseFn().then(
        sleep(this.state.first ? 1000 : time).then(() => {
          this.setState({
            first: false
          })
          // console.log(Number(this.state.appVersion) > Number(appVersion))
          // console.log('Local appVersion: ' + Number(appVersion))
          // console.log('Remote appVersion: ' + Number(this.state.appVersion))
          if (Number(this.state.appVersion) > Number(appVersion)) {
            console.log('update available')
            this.setState({
              updateAvailable: true
            })
            this.showUpdateNotification()
            poll(promiseFn, time)
          } else {
            poll(promiseFn, time)
          }
        })
      )

    // poll(() => new Promise(() => console.log('Hello World!')), 1000)
    this.props.user.token.length > 0 &&
      process.env.NODE_ENV !== 'development' &&
      poll(
        () =>
          apiClient.get('recruiter/settings/').then(result => {
            this.setState({
              appVersion: result.data.version
            })
          }),
        300000
      )

    this.update = this.update.bind(this)
    this.showUpdateNotification = this.showUpdateNotification.bind(this)
  }

  showUpdateNotification() {
    let { dispatch } = this.props
    dispatch(
      Notifications.info({
        uid: 'once-please', // you can specify your own uid if required
        title: 'Uppdatering tillgänglig',
        message: 'Det finns en uppdatering för WAP recruiter.',
        position: 'br',
        autoDismiss: 0,
        action: {
          label: 'Uppdatera till v.' + this.state.appVersion,
          callback: this.update
        }
      })
    )
  }

  update() {
    let { dispatch } = this.props
    dispatch(update())
  }

  render() {
    const { notifications } = this.props
    return (
      <div className="main_container h-100">
        <Header />
        <div id="page_wrapper">
          <Sidemenu />
          <div className="core-layout__viewport" role="main">
            <div
              className="container-fluid px-0"
              id="core-layout__viewport__content"
            >
              {this.props.children}
            </div>
          </div>
        </div>
        <Notifications notifications={notifications} />
      </div>
    )
  }
}

export default userIsAuthenticatedRedir(
  withRouter(connect(state => state)(CoreLayout))
)
