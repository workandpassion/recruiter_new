import React from 'react'
import $ from 'jquery'
import Menuitem from './Menuitem/Menuitem'
import { connect } from 'react-redux'

class Sidemenu extends React.Component {
  constructor(props) {
    super(props)

    this.showMenu = this.showMenu.bind(this)
    this.hideMenu = this.hideMenu.bind(this)
  }

  componentDidMount() {
    let $sidemenu = $('.core-layout__sidemenu')
    let _self = this

    $sidemenu.mouseenter(function() {
      _self.showMenu()
    })
    $sidemenu.mouseleave(function() {
      _self.hideMenu()
    })
  }

  showMenu() {
    let $body = $('body')
    $body.addClass('menu-expanded overflow-x-hidden')
  }

  hideMenu() {
    let $body = $('body')
    $body.removeClass('menu-expanded')
    $('.page_wrapper').one(
      'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',
      function() {
        $body.removeClass('overflow-x-hidden')
        $('.page_wrapper').off(
          'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd'
        )
      }
    )
  }

  render() {
    return (
      <div className="core-layout__sidemenu">
        <div className="sidemenu">
          <ul className="menu-items">
            <Menuitem
              to="/"
              icon="fa-tachometer-alt"
              title="Dashboard"
              indexlink
            />
            <Menuitem icon="fa-users" title="Kandidater">
              <Menuitem
                to="/candidates/search"
                icon="fa-search"
                title="Sök kandidater"
                details="Hitta kandidater i databasen"
              />
              <Menuitem
                to="/candidates/my"
                icon="fa-users"
                title="Mina kandidater"
                details="Sparade och rekommenderade"
              />
            </Menuitem>
            <Menuitem icon="fa-archive" title="Uppdrag">
              <Menuitem
                to="/assignments/my"
                icon="fa-th-list"
                title="Mina uppdrag"
                detail="Visa dina uppdrag"
              />
              <Menuitem
                to="/assignments/all"
                icon="fa-list-ul"
                title="Alla uppdrag"
                detail="Visa alla uppdrag"
              />
              <Menuitem to="/customers" icon="fa-briefcase" title="Kunder" />
            </Menuitem>
          </ul>
        </div>
      </div>
    )
  }
}

export default connect(state => state)(Sidemenu)
