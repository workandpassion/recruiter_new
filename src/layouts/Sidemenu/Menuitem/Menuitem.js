import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import classnames from 'classnames'

import './Menuitem.scss'

import { Collapse } from 'reactstrap'

class Menuitem extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      hasChildren: !!props.children,
      count: this.props.count,
      childIsEmpty: false,
      collapsed: false
    }

    this._handleClick = this._handleClick.bind(this)
  }

  componentDidMount() {
    this.props.children &&
      this.props.children.length > 1 &&
      this.props.children.map(child => {
        if (child.props.to === this.props.location.pathname) {
          this.setState({
            collapsed: true
          })
        }
        return true
      })

    this.props.children &&
      this.props.children.props &&
      this.props.children.props.to &&
      this.props.children.props.to === this.props.location.pathname &&
      this.setState({
        collapsed: true
      })
  }

  _handleClick(e) {
    // e.preventDefault();
    this.setState({ collapsed: !this.state.collapsed })
    return false
  }

  render() {
    let chevronClass = classnames(
      'has-children fa',
      this.state.collapsed ? 'fa-chevron-down' : 'fa-chevron-left'
    )

    let parentClass = 'menu-item'

    if (
      this.props.children &&
      this.props.children.props &&
      this.props.children.props.to
    ) {
      parentClass = classnames(
        'menu-item hasChildren',
        this.props.children &&
          this.props.children.props.to === this.props.location.pathname &&
          'activeParent'
      )
    }

    this.props.children &&
      this.props.children.length > 1 &&
      this.props.children.map(child => {
        if (child.props.to === this.props.location.pathname) {
          parentClass = classnames(
            'menu-item hasChildren',
            child.props.to === this.props.location.pathname && 'activeParent'
          )
        }
        return true
      })

    return (
      <li className="menu-item-wrapper">
        {!this.state.hasChildren && (
          <NavLink
            exact
            to={this.props.to}
            className="menu-item"
            activeClassName="menu-item--active"
            onClick={e => this._handleClick(e)}
          >
            <div className="text-wrapper">
              <span className="title">{this.props.title}</span>
              <span className="details">{this.props.details}</span>
            </div>
            <div className="d-flex align-items-center">
              <span className="icon-thumbnail">
                <i className={'fa ' + this.props.icon} />
              </span>
            </div>
          </NavLink>
        )}

        {this.state.hasChildren && (
          <div className={parentClass} onClick={e => this._handleClick(e)}>
            <div className="text-wrapper">
              <span className="title">{this.props.title}</span>
              <span className="details">{this.props.details}</span>
            </div>
            <div className="d-flex align-items-center">
              {this.state.hasChildren && <span className={chevronClass} />}
              <span className="icon-thumbnail">
                <i className={'fa ' + this.props.icon} />
              </span>
            </div>
          </div>
        )}

        {this.state.hasChildren && (
          <Collapse isOpen={this.state.collapsed} className="submenuWrapper">
            <ul className="sub-menu">{this.props.children}</ul>
          </Collapse>
        )}
      </li>
    )
  }
}

export default withRouter(Menuitem)
