import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from './store/createStore'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import { PersistGate } from 'redux-persist/es/integration/react'

// import 'font-awesome/css/font-awesome.css'
import WebFontLoader from 'webfontloader'

const { persistor, store, history } = configureStore()

window.jQuery = window.$ = require('jquery')

WebFontLoader.load({
  google: {
    families: [
      'Material Icons',
      'Oswald:300,400,500,600,700',
      'Source Serif Pro:400'
    ]
  }
})

ReactDOM.render(
  <PersistGate persistor={persistor}>
    <App store={store} history={history} />
  </PersistGate>,
  document.getElementById('root')
)

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default
    ReactDOM.render(
      <PersistGate persistor={persistor}>
        <NextApp store={store} history={history} />
      </PersistGate>,
      document.getElementById('root')
    )
  })
}

// async function init() {
//   const store = await configureStore()

//   let render = () => {
//     const App = require('./App').default

//   }
//   // registerServiceWorker()
//   render()
// }

// init()
