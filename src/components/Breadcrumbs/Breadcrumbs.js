import React from 'react'
import { Link, Route as RRoute } from 'react-router-dom'
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import Route from 'route-parser'
// import { getStore } from '../../store/createStore'
import { connect } from 'react-redux'

const isFunction = value => typeof value === 'function'

// const store = getStore()
let store

function getRouteNames() {
  let mRoutes = {
    '/': <i className="fa fa-home" />,
    '/candidates': 'Kandidater',
    '/candidates/my': 'Mina kandidater',
    '/candidates/parsepdf': 'Parse PDF',
    '/candidates/search': 'Sök kandidater',
    '/candidates/:userid':
      store.candidates.selectedCandidate &&
      store.candidates.selectedCandidate.first_name +
        ' ' +
        store.candidates.selectedCandidate.last_name,
    '/assignments': 'Uppdrag',
    '/assignments/my': 'Mina uppdrag',
    '/assignments/all': 'Alla uppdrag',
    '/assignments/:assignmentId':
      store.assignments.selectedAssignment &&
      store.assignments.selectedAssignment.subject +
        ' ' +
        store.assignments.selectedAssignment.customer.name,
    '/customers': 'Kunder',
    '/customers/:customerId':
      store.customers.selectedCustomer && store.customers.selectedCustomer.name,
    '/ads': 'Annonser',
    '/profile': 'Profil'
  }

  return mRoutes
}

const findRouteName = url => getRouteNames[url]

const getPaths = pathname => {
  const paths = ['/']

  if (pathname === '/') return paths

  pathname.split('/').reduce((prev, curr, index) => {
    const currPath = `${prev}/${curr}`
    paths.push(currPath)
    return currPath
  })

  return paths
}

function getRouteMatch(routes, path) {
  if (routes) {
    return Object.keys(routes)
      .map(key => {
        const params = new Route(key).match(path)
        return {
          didMatch: params !== false,
          params,
          key
        }
      })
      .filter(item => item.didMatch)[0]
  }
}

const BreadcrumbsItem = ({ match, ...rest }) => {
  const routeMatch = getRouteMatch(getRouteNames(), match.url)
  const routeValue = getRouteNames()[routeMatch.key]
  const name = isFunction(routeValue)
    ? routeValue(routeMatch.params)
    : routeValue
  const routeName = findRouteName(match.url)

  if (name) {
    return match.isExact ? (
      <BreadcrumbItem active>{name}</BreadcrumbItem>
    ) : (
      <BreadcrumbItem>
        <Link to={match.url || ''}>{name}</Link>
      </BreadcrumbItem>
    )
  }
  return null
}

// export default props => (
//   <RRoute path="/:path" component={Breadcrumbs} {...props} />
// )

class Breadcrumbs extends React.Component {
  constructor(props) {
    super(props)

    store = this.props
  }

  getComponent() {}

  render() {
    return <RRoute path="/:path" component={mBreadcrumbs} {...this.props} />
  }
}

export default connect(state => state)(Breadcrumbs)

const mBreadcrumbs = ({ location: { pathname }, match, ...rest }) => {
  const paths = getPaths(pathname)

  return (
    <Breadcrumb>
      {paths.map(p => <RRoute key={p} path={p} component={BreadcrumbsItem} />)}
    </Breadcrumb>
  )
}
