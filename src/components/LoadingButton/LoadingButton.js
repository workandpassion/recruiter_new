import React from 'react'
import { Button } from 'react-md'
import classNames from 'classnames'
import PropTypes from 'prop-types'

class LoadingButton extends Button {
  render() {
    let { loading } = this.props
    let buttonClass = classNames('fa', loading && 'fa-spin fa-spinner')
    return (
      <Button
        type={this.props.type}
        className={this.props.className}
        raised
        primary
        disabled={loading ? loading : this.props.disabled}
        onClick={this.props.onClick}
      >
        {this.props.children} <i className={buttonClass} />
      </Button>
    )
  }
}

export default LoadingButton
