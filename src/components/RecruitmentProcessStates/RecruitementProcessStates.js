import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Popover, PopoverBody, Row, Col } from 'reactstrap'
import moment from 'moment'

class RecruitementProcessStates extends React.Component {
  render() {
    let { states, totalProcessNum } = this.props
    let mClass = classNames('processStates d-flex', this.props.className)

    return (
      <div className={mClass}>
        {states &&
          states.map((state, index) => {
            return (
              <State
                key={state.id}
                state={state}
                id={state.id}
                editFn={this.props.editFn}
                noFunctionality={this.props.noFunctionality}
              />
            )
          })}
        {!this.props.noFunctionality && (
          <div className="stateItem addNew" onClick={this.props.onClick}>
            <span className="stateState bg-green">Lägg till</span>
            <div className="dot stateItem bg-green d-flex justify-content-center align-items-center">
              <i className="fa fa-plus fg-white" />
            </div>
            <div className="line" />
          </div>
        )}
      </div>
    )
  }
}

RecruitementProcessStates.propTypes = {
  states: PropTypes.array.isRequired
}

export default RecruitementProcessStates

class State extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      popoverOpen: false
    }
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    })
  }

  render() {
    let { state, noFunctionality } = this.props

    return (
      <div
        className="stateItem"
        onClick={
          state.comment
            ? this.toggle
            : noFunctionality ? this.return : () => this.props.editFn(state)
        }
      >
        <span className="stateState">{state.value.name}</span>
        <div className="dot" id={'Popover-' + this.props.id}>
          {state.comment && <i className="fa fa-comment comment-icon" />}
        </div>
        {state.scheduled_at &&
          !state.completed_at && (
            <div className="scheduled_at">
              <i className="fa fa-clock-o" />
              {moment(state.scheduled_at).format('YYYY-MM-DD')}
            </div>
          )}

        {state.completed_at && (
          <div className="completed_at">
            <i className="fa fa-check" />
            {moment(state.completed_at).format('YYYY-MM-DD')}
          </div>
        )}
        <div className="line" />
        <Popover
          placement="bottom"
          isOpen={this.state.popoverOpen}
          target={'Popover-' + this.props.id}
          toggle={this.toggle}
        >
          <PopoverBody>
            {state.comment}
            {!noFunctionality && (
              <Row>
                <Col>
                  <i
                    className="fa fa-pencil-square edit-btn"
                    onClick={() => this.props.editFn(state)}
                  />
                </Col>
              </Row>
            )}
          </PopoverBody>
        </Popover>
      </div>
    )
  }
}
