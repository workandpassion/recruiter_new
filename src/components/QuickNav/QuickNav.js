import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

class QuickNav extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      show: false
    }

    this.showHideMenu = this.showHideMenu.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  showHideMenu() {
    this.setState({
      show: !this.state.show
    })
  }

  handleClick(e, item) {
    this.showHideMenu()
    if (item.fn) {
      item.fn()
    }
  }

  render() {
    let { items } = this.props
    let quicknavClass = classNames(
      'quick-nav d-md-none d-lg-none d-xl-none',
      this.state.show && 'nav-is-visible'
    )

    return (
      <nav className={quicknavClass}>
        <div className="quick-nav-trigger" onClick={this.showHideMenu}>
          <span aria-hidden="true" />
        </div>
        <ul>
          {items &&
            typeof items.map === 'function' &&
            items.map(item => {
              return (
                <li key={item.title} onClick={e => this.handleClick(e, item)}>
                  <div>
                    <span>{item.title}</span>
                    <i className={classNames('fa', item.icon)} />
                  </div>
                </li>
              )
            })}
        </ul>
        <span aria-hidden="true" className="quick-nav-bg" />
      </nav>
    )
  }
}

QuickNav.propTypes = {
  items: PropTypes.array
}

export default QuickNav

// class QuickNavItem extends React.Component {
//   render() {
//     let { item } = this.props
//     let iconClass = classNames('fa', item.icon)
//     return (
//       <li>
//         <div>
//           <span>{item.title}</span>
//           <i className={iconClass} />
//         </div>
//       </li>
//     )
//   }
// }
//
// QuickNavItem.propTypes = {
//   item: PropTypes.object.isRequired
// }
