const defaultRanges = {
  Idag: {
    startDate: function startDate(now) {
      return now
    },
    endDate: function endDate(now) {
      return now
    }
  },

  Igår: {
    startDate: function startDate(now) {
      return now.add(-1, 'days')
    },
    endDate: function endDate(now) {
      return now.add(-1, 'days')
    }
  },

  '7 dagar': {
    startDate: function startDate(now) {
      return now.add(-7, 'days')
    },
    endDate: function endDate(now) {
      return now
    }
  },

  '14 dagar': {
    startDate: function startDate(now) {
      return now.add(-14, 'days')
    },
    endDate: function endDate(now) {
      return now
    }
  },

  '30 dagar': {
    startDate: function startDate(now) {
      return now.add(-30, 'days')
    },
    endDate: function endDate(now) {
      return now
    }
  }
}
export default defaultRanges
