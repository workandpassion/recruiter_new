import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Popover, PopoverHeader, PopoverBody, Row, Col } from 'reactstrap'

class HorizontalAssignmentStates extends React.Component {
  render() {
    let { states, totalProcessNum } = this.props
    let mClass = classNames(
      'horizontalAssignmentStates d-flex',
      this.props.className
    )

    return (
      <div className={mClass}>
        {states &&
          states.map((state, index) => {
            return <State key={state.id} state={state} id={state.id} />
          })}
        <div className="stateItem addNew" onClick={this.props.onClick}>
          <span className="stateState bg-green">Lägg till</span>
          <div className="dot stateItem bg-green d-flex justify-content-center align-items-center">
            <i className="fa fa-plus fg-white" />
          </div>
          <div className="line" />
        </div>
      </div>
    )
  }
}

HorizontalAssignmentStates.propTypes = {
  states: PropTypes.array.isRequired
}

export default HorizontalAssignmentStates

class State extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      popoverOpen: false
    }
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    })
  }

  render() {
    let { state } = this.props

    return (
      <div className="stateItem" onClick={this.toggle}>
        <span className="stateState">{state.value.name}</span>
        <div className="dot" id={'Popover-' + this.props.id}>
          {state.comment && <i className="fa fa-comment comment-icon" />}
        </div>
        <div className="line" />
        <Popover
          placement="bottom"
          isOpen={this.state.popoverOpen}
          target={'Popover-' + this.props.id}
          toggle={this.toggle}
        >
          <PopoverBody>
            {state.notes}
            <Row>
              <Col>
                <i className="fa fa-pencil-square edit-btn" />
              </Col>
            </Row>
          </PopoverBody>
        </Popover>
      </div>
    )
  }
}
