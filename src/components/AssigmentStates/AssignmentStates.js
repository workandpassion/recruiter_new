import React from 'react'
import { Col, Row, Card, CardHeader, CardTitle, CardBody } from 'reactstrap'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import $ from 'jquery'
import { ActionButtons } from '../../layouts/Header/SubHeader'

class AssignmentStates extends React.Component {
  setColorFromSibling() {
    let $addNewState = $('#addNewState')
    let $siblingPrev = $addNewState.prev()
    let siblingId = $siblingPrev.attr('id')
    $addNewState.addClass(siblingId)
  }

  componentDidMount() {
    this.setColorFromSibling()
  }

  componentDidUpdate(prevProps, prevState) {
    this.setColorFromSibling()
  }

  render() {
    let { states } = this.props
    let mClass = classNames(
      'assignmentStates d-flex flex-column-reverse',
      this.props.vertical && 'vertical',
      this.props.className
    )

    return (
      <Row className={mClass}>
        {states &&
          states.map(state => {
            return (
              <State
                key={state.id}
                state={state}
                onClick={() => this.props.onClick(state)}
              />
            )
          })}
        {this.props.addable && (
          <Col
            xs={12}
            className="stateItem"
            id="addNewState"
            onClick={this.props.onClick}
          >
            <div className="d-flex flex-column">
              <div className="dot bg-green">
                <i className="fa fa-plus" />
              </div>
              <div className="line" />
            </div>
            <Card className="comments">
              <CardHeader>
                <CardTitle>Lägg till</CardTitle>
              </CardHeader>
            </Card>
          </Col>
        )}
      </Row>
    )
  }
}

AssignmentStates.propTypes = {
  states: PropTypes.array.isRequired
}

export default AssignmentStates

class State extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      popoverOpen: true
    }

    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    })
  }

  render() {
    let { state } = this.props
    let actions = [
      {
        title: 'Redigera',
        icon: 'fa-pencil',
        id: 'id_' + state.id,
        fn: this.props.onClick
      }
    ]
    return (
      <Col xs={12} className="stateItem" id={state.value.name.toLowerCase()}>
        <div className="d-flex flex-column">
          <div className="dot" />
          <div className="line" />
        </div>
        <Card className="comments">
          <CardHeader>
            <CardTitle>{state.value.name}</CardTitle>
            <ActionButtons showOnHover actions={actions} />
          </CardHeader>
          <CardBody>{state.notes}</CardBody>
        </Card>
      </Col>
    )
  }
}
