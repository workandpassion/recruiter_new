import React from 'react'
import $ from 'jquery'
import { Progress, Button } from 'reactstrap'
import { connect } from 'react-redux'

class Video extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      downloaded: false,
      downloading: false
    }

    this.downloadVideoFromServer = this.downloadVideoFromServer.bind(this)
  }

  downloadVideoFromServer() {
    let that = this

    let url =
      'https://api.wapcard.se/api/v1/download/videos/' + this.props.videoid

    let xhr = new XMLHttpRequest()

    xhr.open('GET', url, true)
    xhr.setRequestHeader('Authorization', 'Token ' + this.props.user.token)
    xhr.responseType = 'arraybuffer'

    xhr.onload = function(oEvent) {
      let blob = new Blob([oEvent.target.response], {
        type: that.props.content_type
      })

      // video.src = URL.createObjectURL(blob);

      that.setState({
        videoSrc: URL.createObjectURL(blob),
        downloaded: true,
        downloading: false
      })

      that.props.videoIsDownloaded()
      // video.play()  if you want it to play on load
    }

    xhr.onprogress = function(oEvent) {
      that.setState({
        downloading: true
      })

      if (oEvent.lengthComputable) {
        let percentComplete = oEvent.loaded / oEvent.total
        console.log(percentComplete)
        $('#progress .progress-bar').width(percentComplete * 100 + '%')
      }
    }

    xhr.send()
  }

  render() {
    return (
      <div id="videoWrapper">
        {!this.state.downloaded &&
          !this.state.downloading && (
            <Button onClick={() => this.downloadVideoFromServer()}>
              Ladda ner film
            </Button>
          )}
        {this.state.downloading && (
          <Progress id="progress" value={0} color="info" min={0} max={1} />
        )}
        {this.state.downloaded &&
          this.state.videoSrc && (
            <video id="video" width="100%" height="auto" controls preload>
              <source src={this.state.videoSrc} />
              Your browser does not support HTML5 video.
            </video>
          )}
      </div>
    )
  }
}

export default connect(state => state)(Video)
